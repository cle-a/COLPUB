This repository contains the experiment code, the data, and the analysis code to produce the main results described in _Competing cognitive pressures on human exploration in the absence of trade-off with exploitation_ - Alméras, Chambon, Wyart.


1. Experiment: contains the code used for generating and running the task for the three datasets, in separated subfolders.  
   .COLEXP_experiment = discovery dataset  
   .COLREP_experiment = replication dataset  
   .COLMID_experiment  = third dataset  
The display code is based on Pschychtoolbox-3 (we ran version 3.0.14 on MATLAB 2017b): [http://psychtoolbox.org/](http://psychtoolbox.org/)


2. Data: contains the data recorded for each dataset.  
   .COLEXP_data = discovery dataset  
   .COLREP_data = replication dataset  
   .COLMID_data  = third dataset  


3. Analysis: contains code to reproduce the main results of the manuscript.
The model fitting code uses the fmincon function, from MATLAB's Optimization Toolbox: [https://www.mathworks.com/products/optimization.html](https://www.mathworks.com/products/optimization.html)  
The model fitting code uses the bads package (Acerbi, L. & Ji, W. Practical Bayesian Optimization for Model Fitting with Bayesian Adaptive Direct Search. 11 (2017) [https://github.com/acerbilab/bads](https://github.com/acerbilab/bads)
The analysis code uses the function simple_mixed_anova from Laurent Caplette [https://www.researchgate.net/profile/Laurent_Caplette](https://www.researchgate.net/profile/Laurent_Caplette)
The analysis code uses MATLAB's Image Processing Toolbox [https://www.mathworks.com/products/image-processing.html](https://www.mathworks.com/products/image-processing.html)  

**Note**: the analysis code was submitted before collecting the replication dataset (available at https://gitlab.com/cle-a/colrep). The model fitting code was updated for the analysis of the third dataset and during the review process. 

The software is licenced under Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)