%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% does the stats on the behavioural indices
% for each sample in the paper
% fetches data in Dat/dat_choicestbl_xxx.mat
%                 where xxx is the samplename
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clearvars

addpath './Toolboxes'
addpath './Toolboxes/draw'

samplename = 'COLMID';

figsdir  = sprintf('./Figs/%s/', samplename);

load(sprintf('Dat/dat_choicestbl_%s.mat', samplename), 'choicestbl');
choicestbl = choicestbl(choicestbl.excludepart == 0,:);
subjl = unique(choicestbl.subj);
nsubj = numel(unique(choicestbl.subj));
ncond = max(unique(choicestbl.condition));
nfact = 4;

% diary
clc
diaryfile = sprintf('%s%s_statsprintout_behaviour.txt', figsdir,samplename);
if (exist(diaryfile))
    delete(diaryfile);
end
diary(diaryfile);
diary on
fprintf('------------------------\n%s: BEHAVIOUR STATS\n------------------------\n\n\n', samplename);


%% PERFORMANCE learning curve
lcurve = nan(nsubj, 20, ncond);
for icond = 1:ncond
    for isubj = 1:nsubj
        temp = choicestbl(choicestbl.subj == subjl(isubj) & ...
            choicestbl.condition == icond & choicestbl.symmetry == 3,:); % only in symm == 3 (goodbad)
        temp = cell2mat(temp.choices);
        correct = double(temp == 1);
        correct(isnan(temp)) = nan;
        lcurve(isubj,:, icond) = nanmean(correct,1);
    end
end

if strcmp(samplename, 'COLMID')
    lcurve = lcurve(:,:,[1 3]);
else
    lcurve = lcurve(:,:,1);
end

% test against chance: for COLREP DRAW and COLMID DRAW and FIND
for whichcondition = 1:size(lcurve,3)
    [~, pvals, ~, tvals] = ttest(lcurve(:,:,whichcondition), 0.5);
    signif = pvals < 0.05;
    clustr = bwconncomp(signif);

    og_peakt = max(tvals.tstat);
    og_clust = clustr.PixelIdxList;

    % run nit simulations of random agents
    rng(1);
    nit      = 1e2;
    peakt_dis = nan(nit, 1);
    clust_dis = cell(nit,1);

    for it = 1:nit
        rnd_lcurve = nan(nsubj, 20, ncond, nfact);
        for isubj = 1:nsubj
            for icond = 1:ncond
                for ifact = 4

                    if ifact < 4
                        ifilt = choicestbl.subj == subjl(isubj) & choicestbl.condition == icond & choicestbl.symmetry == ifact;
                    elseif ifact ==4
                        ifilt = choicestbl.subj == subjl(isubj) & choicestbl.condition == icond;
                    end

                    truchoices = choicestbl.choices(ifilt);
                    truchoices = cellfun(@(x) x(~isnan(x)), truchoices, 'UniformOutput', false);            % remove trailing nans
                    rndchoices = cellfun(@(x) randi(2, size(x)), truchoices, 'UniformOutput', false);       % REPLACE WITH RANDOM CHOICES
                    rndchoices = cellfun(@(x) x == 1, rndchoices, 'UniformOutput', false);                  % get correctness
                    rndchoices = cellfun(@(x) [x nan(1, 20-numel(x))], rndchoices, 'UniformOutput', false); % get correctness
                    rndchoices = cell2mat(rndchoices);
                    rnd_lcurve(isubj, :, icond, ifact) = nanmean(rndchoices, 1);

                end%fact
            end%cond
        end%subj

        % compare random choice direction to chance and store cluster values
        temp = squeeze(rnd_lcurve(:,:,whichcondition,4));
        [~, pvals, ~, tvals] = ttest(temp, 0.5);
        signif = pvals < 0.05;
        clustr = bwconncomp(signif);

        if all(signif == 0) % no time point is significant
            peakt_dis(it) = 0;
        elseif clustr.NumObjects == 1 % single cluster
            peakt_dis(it) = max(abs(tvals.tstat));
            clust_dis(it) = clustr.PixelIdxList;
        elseif clustr.NumObjects > 1 % multiple clusters, keep that with highest tvalue
            [tpeak, idx] = max(abs(tvals.tstat));
            temp = cell2mat(cellfun(@(x) sum(x == idx), clustr.PixelIdxList, 'UniformOutput', false));
            temp = find(temp == 1);
            peakt_dis(it) = tpeak;
            clust_dis(it) = clustr.PixelIdxList(temp);
        end
    end

    % get final pvalue for DRAW vs. chance, against random agents vs. chance
    fprintf('cluster against chance:\n');
    fprintf('peak t value = %0.2f\n', og_peakt);
    fprintf('     p value = %0.3f\n', sum(peakt_dis >= og_peakt) / nit);
    fprintf('cluster choices:');
    cell2mat(og_clust)'
    fprintf('\n');
    pval = sum(peakt_dis >= og_peakt) / nit;

end

% for COLMID, also compare conditions

% get og clusters in the original data
if strcmp(samplename, 'COLMID')
    [~, pvals, ~, tvals] = ttest(lcurve(:,:,1), lcurve(:,:,2));
    signif = pvals < 0.05;
    clustr = bwconncomp(signif);
    for ic = 1:clustr.NumObjects
        og_peakt(ic) = max(abs(tvals.tstat(clustr.PixelIdxList{ic}')));
    end
    og_clust = clustr.PixelIdxList;

    rng(1);
    nit = 1e2;
    for ic = 1:numel(og_peakt) % for each cluster
        peakt_dis = nan(nit, 1);
        clust_dis = cell(nit,1);
        for it = 1:nit
            permuted = lcurve;
            selected_subji = randi(size(lcurve,1), [1, floor(size(lcurve,1)/2)]);
            permuted(selected_subji, : , 1) = lcurve(selected_subji, : , 2);
            permuted(selected_subji, : , 2) = lcurve(selected_subji, : , 1);
            [~, pvals, ~, tvals] = ttest(permuted(:,:,1), permuted(:,:,2));
            signif = pvals < 0.05;
            clustr = bwconncomp(signif);

            if all(signif == 0) % no time point is significant
                peakt_dis(it) = 0;
            elseif clustr.NumObjects == 1 % single cluster
                peakt_dis(it) = max(abs(tvals.tstat));
                clust_dis(it) = clustr.PixelIdxList;
            elseif clustr.NumObjects > 1 % multiple clusters, keep that with highest tvalue
                [tpeak, idx] = max(abs(tvals.tstat));
                temp = cell2mat(cellfun(@(x) sum(x == idx), clustr.PixelIdxList, 'UniformOutput', false));
                peakt_dis(it) = tpeak;
                clust_dis(it) = clustr.PixelIdxList(temp == 1);
            end

        end

        fprintf('for original cluster no %d\n', ic);
        og_peakt(ic)
        cell2mat(og_clust(ic))'
        pval = sum(peakt_dis >= og_peakt(ic)) / nit
    end
end


%% PERFORMANCE: accuracy at last sample

ncond = numel(unique(choicestbl.condition));
fchoice = nan(nsubj, ncond);
for icond = 1:ncond
    for isubj = 1:nsubj
        temp = choicestbl(choicestbl.subj == subjl(isubj) & ...
                          choicestbl.condition == icond & choicestbl.symmetry == 3,:);
        for it = 1:size(temp,1)
            targ(it) = temp.choices{it}(temp.nsamples(it)) == 1;
        end
        fchoice(isubj,icond) = nanmean(targ);         
    end
end

if strcmp(samplename, 'COLMID')
    fchoice = fchoice(:,[1 3]).*100;
else 
    fchoice = fchoice(:,1).*100;
end

for icond = 1:size(fchoice,2)
    X = fchoice(:,icond);
    chance = 50;
    [~, tpval, ~, tstats] = ttest(X, chance);
    d                     = mean(X - chance) / std(X);
    ci                    = [mean(X) - 1.96*(std(X)/sqrt(size(X,1))) mean(X) + 1.96*(std(X)/sqrt(size(X,1)))];
    fprintf('Last sample condition vs. chance\n-----------------\n');
    fprintf('Accuracy at last sample vs. chance (50%%): mean: %.2f -- CI [%.2f : %.2f] -- std: %.2f\n', mean(X), ci, std(X));
    fprintf('Accuracy at last sample vs. chance (50%%): T(df %d) = %.1f; p = %.3f; d = %.2f\n\n', tstats.df, tstats.tstat, tpval, d);
    fprintf('\n');
    
    
    
    [wpval, ~, wstats] = signrank(X, chance); % is 0 always chance?
    if ~isfield(wstats, 'zval')
        Z              = nan;
        r              = nan;
    elseif isfield(wstats, 'zval')
        Z              = wstats.zval;
        r              = Z/sqrt(size(X,1));
    end
    fprintf('Accuracy at last sample vs. chance (50%%): median: %.2f -- IQR: (%.2f) [%.2f - %.2f]\n', median(X), iqr(X), prctile(X,[25 75]));
    fprintf('Accuracy at last sample vs. chance (50%%): Wilcoxon: Z = %.3f; p = %.3f; r = %.2f\n\n', Z, wpval, r);
    fprintf('\n\n');
end


if strcmp(samplename, 'COLMID')
    
    X = fchoice(:,1)-fchoice(:,2);
    chance = 50;
    [~, tpval, ~, tstats] = ttest(X, chance);
    d                     = mean(X - chance) / std(X);
    ci                    = [mean(X) - 1.96*(std(X)/sqrt(size(X,1))) mean(X) + 1.96*(std(X)/sqrt(size(X,1)))];
    fprintf('Last sample comparison\n-----------------\n');
    fprintf('Accuracy at last sample DRAW vs. FIND: mean: %.2f -- CI [%.2f : %.2f] -- std: %.2f\n', mean(X), ci, std(X));
    fprintf('Accuracy at last sample DRAW vs. FIND: T(df %d) = %.1f; p = %.3f; d = %.2f\n\n', tstats.df, tstats.tstat, tpval, d);
    fprintf('\n');
    
    [wpval, ~, wstats] = signrank(X, chance); % is 0 always chance?
    if ~isfield(wstats, 'zval')
        Z              = nan;
        r              = nan;
    elseif isfield(wstats, 'zval')
        Z              = wstats.zval;
        r              = Z/sqrt(size(X,1));
    end
    fprintf('Accuracy at last sample DRAW vs. FIND: median: %.2f -- IQR: (%.2f) [%.2f - %.2f]\n', median(X), iqr(X), prctile(X,[25 75]));
    fprintf('Accuracy at last sample DRAW vs. FIND: Wilcoxon: Z = %.3f; p = %.3f; r = %.2f\n\n', Z, wpval, r);
    fprintf('\n\n');
    
end

%% PEFORMANCE: final question

fquest = nan(nsubj,ncond);
for icond = 1:ncond
    for isubj = 1:nsubj
        temp = choicestbl(choicestbl.subj == subjl(isubj) & ...
                          choicestbl.condition == icond & choicestbl.symmetry == 3,:);
        fquest(isubj, icond) = nanmean(temp.finalchoicecorr);         
    end
end

if strcmp(samplename, 'COLMID')
    fquest = fquest(:,[2 3]).*100;
    cnames = {'FIND', 'GUESS'};
else 
    fquest = fquest(:,2).*100;
    cnames = {'GUESS'};
end

for icond = 1:size(fquest,2)
    X      = fquest(:,icond);
    chance = 50;
    [~, tpval, ~, tstats] = ttest(X, chance);
    d                     = mean(X - chance) / std(X);
    ci                    = [mean(X) - 1.96*(std(X)/sqrt(size(X,1))) mean(X) + 1.96*(std(X)/sqrt(size(X,1)))];
    fprintf('Final question condition vs chance\n-----------------\n');
    fprintf('Accuracy at final question vs. chance (50%%): mean: %.2f -- CI [%.2f : %.2f] -- std: %.2f\n', mean(X), ci, std(X));
    fprintf('Accuracy at final question vs. chance (50%%): T(df %d) = %.1f; p = %.3f; d = %.2f\n\n', tstats.df, tstats.tstat, tpval, d);
    fprintf('\n');
    
    
    [wpval, ~, wstats] = signrank(X, chance); % is 0 always chance?
    if ~isfield(wstats, 'zval')
        Z              = nan;
        r              = nan;
    elseif isfield(wstats, 'zval')
        Z              = wstats.zval;
        r              = Z/sqrt(size(X,1));
    end
    fprintf('Accuracy at final question vs. chance (50%%): median: %.2f -- IQR: (%.2f) [%.2f - %.2f]\n', median(X), iqr(X), prctile(X,[25 75]));
    fprintf('Accuracy at final question vs. chance (50%%): Wilcoxon: Z = %.3f; p = %.3f; r = %.2f\n\n', Z, wpval, r);
end

if strcmp(samplename, 'COLMID')
    
    X = fquest(:,1)-fquest(:,2);
    chance = 50;
    [~, tpval, ~, tstats] = ttest(X, chance);
    d                     = mean(X - chance) / std(X);
    ci                    = [mean(X) - 1.96*(std(X)/sqrt(size(X,1))) mean(X) + 1.96*(std(X)/sqrt(size(X,1)))];
    fprintf('Final question GUESS vs. FIND\n-----------------\n');
    fprintf('Accuracy at final question GUESS vs. FIND: mean: %.2f -- CI [%.2f : %.2f] -- std: %.2f\n', mean(X), ci, std(X));
    fprintf('Accuracy at final question GUESS vs. FIND: T(df %d) = %.1f; p = %.3f; d = %.2f\n\n', tstats.df, tstats.tstat, tpval, d);
    fprintf('\n');
    
    [wpval, ~, wstats] = signrank(X, chance); % is 0 always chance?
    if ~isfield(wstats, 'zval')
        Z              = nan;
        r              = nan;
    elseif isfield(wstats, 'zval')
        Z              = wstats.zval;
        r              = Z/sqrt(size(X,1));
    end
    fprintf('Accuracy at final question GUESS vs. FIND : median: %.2f -- IQR: (%.2f) [%.2f - %.2f]\n', median(X), iqr(X), prctile(X,[25 75]));
    fprintf('Accuracy at final question GUESS vs. FIND : Wilcoxon: Z = %.3f; p = %.3f; r = %.2f\n\n', Z, wpval, r);
    fprintf('\n\n');
    
end

fprintf('\n\n');

%% prepeat overall
temp            = cell2mat(choicestbl.choices);
choicestbl.frep = (sum(~isnan(temp),2) - nansum(abs(diff(temp,1,2)),2)) ./ sum(~isnan(temp),2);

nfact = 3;
subjl = unique(choicestbl.subj);
nsubj = numel(subjl);
frep  = nan(nsubj, ncond, nfact+1);
for icond = 1:ncond
    for ifact = 1:nfact
        for isubj = 1:nsubj
            subj = subjl(isubj);
            temp = choicestbl.frep(choicestbl.subj == subj & choicestbl.condition == icond & choicestbl.symmetry == ifact,:);
            frep(isubj,icond,ifact) = mean(temp);
        end
    end
end

% adding the 4th fact = average 
frep(:,:,4) = mean(frep(:,:,1:3),3);

% for COLMID reorder conditions
if strcmp(samplename, 'COLMID')
    frep = frep(:,[1 3 2],:); % SWAPPING CONDITIONS IN COLMID
    condnames = {'DRAW', 'FIND', 'GUESS'};
else condnames = {'DRAW', 'GUESS'};
end

temp = frep(:,:,4); % keeping only the average

% TESTING PER CONDITION
for icond = 1:ncond 
    fprintf('prepeat overall condition %s\n-----------------\n', condnames{icond});
    X      = temp(:,icond);
    chance = 0.5;
    [wpval, ~, wstats] = signrank(X, chance); % is 0 always chance?
    if ~isfield(wstats, 'zval')
        Z              = nan;
        r              = nan;
    elseif isfield(wstats, 'zval')
        Z              = wstats.zval;
        r              = Z/sqrt(size(X,1));
    end
    fprintf('p repeat overall against chance (0.5%%): median: %.2f -- IQR: (%.2f) [%.2f - %.2f]\n', median(X), iqr(X), prctile(X,[25 75]));
    fprintf('p repeat overall against chance (0.5%%): Wilcoxon: Z = %.3f; p = %.3f; r = %.2f\n', Z, wpval, r);
    fprintf('mean(sd) = %.3f(%.3f)\n\n', mean(X), std(X))
end

%TESTING COMPARISONS FOR COLMID
% fprintf('prepeat condition comparisons\n-----------------\n');
% X                  = temp(:,1)-temp(:,2);
% [wpval, ~, wstats] = signrank(X, 0); % is 0 always chance?
% if ~isfield(wstats, 'zval')
%     Z              = nan;
%     r              = nan;
% elseif isfield(wstats, 'zval')
%     Z              = wstats.zval;
%     r              = Z/sqrt(size(X,1));
% end
% fprintf('p repeat overall DRAW - FIND: median: %.2f -- IQR: (%.2f) [%.2f - %.2f]\n', median(X), iqr(X), prctile(X,[25 75]));
% fprintf('p repeat overall DRAW - FIND: Wilcoxon: Z = %.3f; p = %.3f; r = %.2f\n\n', Z, wpval, r);
% 
% X                  = temp(:,2)-temp(:,3);
% [wpval, ~, wstats] = signrank(X, 0); % is 0 always chance?
% if ~isfield(wstats, 'zval')
%     Z              = nan;
%     r              = nan;
% elseif isfield(wstats, 'zval')
%     Z              = wstats.zval;
%     r              = Z/sqrt(size(X,1));
% end
% fprintf('p repeat overall FIND - GUESS: median: %.2f -- IQR: (%.2f) [%.2f - %.2f]\n', median(X), iqr(X), prctile(X,[25 75]));
% fprintf('p repeat overall FIND - GUESS: Wilcoxon: Z = %.3f; p = %.3f; r = %.2f\n\n', Z, wpval, r);
% 
% X                  = temp(:,1)-temp(:,3);
% [wpval, ~, wstats] = signrank(X, 0); % is 0 always chance?
% if ~isfield(wstats, 'zval')
%     Z              = nan;
%     r              = nan;
% elseif isfield(wstats, 'zval')
%     Z              = wstats.zval;
%     r              = Z/sqrt(size(X,1));
% end
% fprintf('p repeat overall DRAW - GUESS: median: %.2f -- IQR: (%.2f) [%.2f - %.2f]\n', median(X), iqr(X), prctile(X,[25 75]));
% fprintf('p repeat overall DRAW - GUESS: Wilcoxon: Z = %.3f; p = %.3f; r = %.2f\n\n', Z, wpval, r);

% TESTING COMPARISONS
fprintf('prepeat condition comparisons\n-----------------\n');
X                  = temp(:,1)-temp(:,2);
[wpval, ~, wstats] = signrank(X, 0); % is 0 always chance?
if ~isfield(wstats, 'zval')
    Z              = nan;
    r              = nan;
elseif isfield(wstats, 'zval')
    Z              = wstats.zval;
    r              = Z/sqrt(size(X,1));
end
fprintf('p repeat overall DRAW - GUESS: median: %.2f -- IQR: (%.2f) [%.2f - %.2f]\n', median(X), iqr(X), prctile(X,[25 75]));
fprintf('p repeat overall DRAW - GUESS: Wilcoxon: Z = %.3f; p = %.3f; r = %.2f\n\n', Z, wpval, r);

%% prepeat x familiarity anova
subjl  = unique(choicestbl.subj);
nsubj  = numel(subjl);
spstay = nan(nsubj, 20, max(choicestbl.condition));
for isubj = 1:nsubj
    
    for icond = 1:max(choicestbl.condition)
        ifilt    = choicestbl.subj == subjl(isubj) & choicestbl.condition == icond;
        schoices = cellfun(@(x) x(~isnan(x)), choicestbl.choices(ifilt), 'UniformOutput', false); % cle
        
        % then for each choice, count nb of of times this option was seen + whether they repeat on the next choice
        nseq     = size(schoices, 1);
        sprevch  = nan(nseq,20);
        srepeat  = nan(nseq,20);
        
        for iseq = 1:nseq
            nsmp = size(schoices{iseq},2);
            
            % subj: for each sequence, count nprev and nrepeats
            % mod : for each sequence, count in all the simulations
            for ismp  = 1:nsmp-1
                sprevch(iseq, ismp) = sum( schoices{iseq}(1:ismp) == schoices{iseq}(ismp) ); % count nb of times seen, including this one
                srepeat(iseq, ismp) = schoices{iseq}(ismp) == schoices{iseq}(ismp+1);        % ~abs(diff([schoices{iseq}(ismp) schoices{iseq}(ismp+1)]' ));  % do they repeat the next choice
            end
        end
        
        % subj: for each nb of times seen up to here, get average p(stay)
        for idx = 1:max(sprevch(:))
            ich = find(sprevch == idx);
            spstay(isubj, idx, icond) = nanmean(srepeat(ich));
        end        
    end
end

liim = 10;
xdat = spstay(:,1:liim,:); % 27subj x 10presampled x 2conditions;

[tbl,rm]  = simple_mixed_anova(xdat, [], {'presmp','condition'}, {});
fprintf('\n***\n');
fprintf('ANOVA\n')
fprintf('p(repeat) x condition: F(Df: %d, %d) = %.2f, p = %.3f, eta2 = %.3f, eta_p^2 = %.3f, eta_G^2 = %.3f\n', tbl.DF(5), tbl.DF(6), tbl.F(5), tbl.pValue(5),...
                                                                                    tbl.SumSq(5) / (sum(tbl.SumSq(3:end))),...
                                                                                    tbl.SumSq(5) / (tbl.SumSq(5) + tbl.SumSq(6)),...
                                                                                    tbl.SumSq(5) / (tbl.SumSq(5) + tbl.SumSq(2) + tbl.SumSq(4) + tbl.SumSq(6) + tbl.SumSq(8)));
                                                                                
fprintf('p(repeat) x nb pre-samples: F(Df: %d, %d) = %.2f, p = %.3f, eta2 = %.3f, eta_p^2 = %.3f, eta_G^2 = %.3f\n', tbl.DF(3), tbl.DF(4), tbl.F(3), tbl.pValue(3),...
                                                                                    tbl.SumSq(3) / (sum(tbl.SumSq(3:end))),...
                                                                                    tbl.SumSq(3) / (tbl.SumSq(3) + tbl.SumSq(4)),...
                                                                                    tbl.SumSq(3) / (tbl.SumSq(3) + tbl.SumSq(2) + tbl.SumSq(4) + tbl.SumSq(6) + tbl.SumSq(8)));
                                                                                
fprintf('p(repeat) x condition x pre-samples: F(Df: %d, %d) = %.2f, p = %.3f, eta2 = %.3f, eta_p^2 = %.3f, eta_G^2 = %.3f\n', tbl.DF(7), tbl.DF(8), tbl.F(7), tbl.pValue(7),...
                                                                                    tbl.SumSq(7) / (sum(tbl.SumSq(3:end))),...
                                                                                    tbl.SumSq(7) / (tbl.SumSq(7) + tbl.SumSq(8)),...
                                                                                    tbl.SumSq(7) / (tbl.SumSq(7) + tbl.SumSq(2) + tbl.SumSq(4) + tbl.SumSq(6) + tbl.SumSq(8)));   

                                                                                
%% posthoc test of prepeat curve? ttest, c-corrected by permutations??





                                                                                
%% diary off

diary off