%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% computes cluster corrected pvalues for the clusters of significant choice directions:
%
% NB which condition and variable we're testing is set manually below
% either comparing single variable to chance agents
% or comparing conditions by swapping labels 
% in COLEXP and COLREP:
%       target direction against chance in DRAW
%       uncertainty direction compared between DRAW and FIND
% in COLMID:
%       target direction compared between DRAW and FIND
%       uncertainty direction compared between FIND and GUESS
% this needs to be toggled manually to processing COLREP or COLMID
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all
clearvars;
clc

addpath './Toolboxes'
addpath './Toolboxes/draw'

samplename = 'COLREP';

fprintf('\n\n*** %s ***\n*** %s *** Testing choice directions...\n', samplename, samplename)


%% GET TRUE CHOICE DIRECTIONS

load(sprintf('./Dat/dat_choicestbl_%s.mat', samplename))
subjl = unique(choicestbl.subj);
nsubj = numel(subjl);
ncond = numel(unique(choicestbl.condition));
nfact = numel(unique(choicestbl.symmetry));

sub_vopt = nan(nsubj, 20, ncond, nfact);
sub_copt = nan(nsubj, 20, ncond, nfact);

for isubj = 1:nsubj
    for icond = 1:ncond
        for ifact = 4

            if ifact < 4
                ifilt = choicestbl.subj == subjl(isubj) & choicestbl.condition == icond & choicestbl.symmetry == ifact;
            elseif ifact ==4
                ifilt = choicestbl.subj == subjl(isubj) & choicestbl.condition == icond;
            end
            
            choices = choicestbl.choices(ifilt);
            choices = cellfun(@(x) x(~isnan(x)), choices, 'UniformOutput', false); % remove trailing nans
            targetc = num2cell(2*choicestbl.targetcolor(ifilt)-3); % -1 | +1 target colours
            nsample = num2cell(choicestbl.nsamples(ifilt));

            x       = -1:0.001:+1;
            perr    = 1/3;
            cfac    = fzero(@(b)trapz(x(x > 0),1./(1+exp(x(x > 0)*b))/trapz(x,1./(1+exp(x*b))))-perr,[0,10]);

            cs   = choicestbl.colorseen(ifilt);
            cs   = cellfun(@(x,b) x(:,1:size(b,2))*cfac, cs, choices, 'UniformOutput', false); % remove trailing nans & multiply by cfac
            cs   = cellfun(@(x) cumsum(x, 2, 'omitnan'), cs, 'UniformOutput', false);          % accumulate the colour values

            [~, opt] = cellfun(@(x,t) max(x.*t, [], 1), cs, targetc, 'UniformOutput', false); % get index of option closest to target
            temp = cellfun(@(a,b) a == b, choices, opt, 'UniformOutput', false); % compare choices to copt choices
            temp = cellfun(@(x) [x nan(1, 20-size(x,2))], temp, 'UniformOutput', false);
            temp = cell2mat(temp);
            sub_vopt(isubj, :, icond, ifact) = nanmean(temp, 1); % average across subj's sequences

            [~, opt] = cellfun(@(x) min(abs(x), [], 1), cs, 'UniformOutput', false); % get index of the option with smallest acc value
            temp = cellfun(@(a,b) a == b, choices, opt, 'UniformOutput', false); % compare choices to copt choices
            temp = cellfun(@(x) [x nan(1, 20-size(x,2))], temp, 'UniformOutput', false);
            temp = cell2mat(temp);
            sub_copt(isubj, :, icond, ifact) = nanmean(temp, 1); % average across subj's sequences

        end%fact
    end%cond
end%subj


%% COMPARING CHOICES TO A RANDOM AGENT
%%%%%%%%%%%%%%%%%%%%%
% this needs to be toggled manually
% either testing vopt or copt
% it's always against random agents
% so manually set the variable whichcondition (to 1, 2 or 3)
% and whether we're looking at target directed choices (vopt)
% or uncertainty directed choices (copt)
%%%%%%%%%%%%%%%%%%%%%

clearvars og_peakt og_clust peakt_dis clust_dis


whichcondition = 1;

temp = squeeze(sub_vopt(:,:,whichcondition,4));
[~, pvals, ~, tvals] = ttest(temp, 0.5);
signif = pvals < 0.05;
clustr = bwconncomp(signif);
for ic = 1:clustr.NumObjects
    og_peakt(ic) = max(abs(tvals.tstat(clustr.PixelIdxList{ic}')));
end
og_clust = clustr.PixelIdxList;

% run nit simulations of random agents
nit      = 1e2;
peakt_dis = nan(nit, 1);
clust_dis = cell(nit,1);

rng(1);

for ic = 1:numel(og_peakt)
    for it = 1:nit
        if mod(it,20) == 0; fprintf('it #%d/%d\n', it, nit); end
        rnd_vopt = nan(nsubj, 20, ncond, nfact);
        rnd_copt = nan(nsubj, 20, ncond, nfact);
        for isubj = 1:nsubj
            for icond = 1:ncond
                for ifact = 4

                    if ifact < 4
                        ifilt = choicestbl.subj == subjl(isubj) & choicestbl.condition == icond & choicestbl.symmetry == ifact;
                    elseif ifact ==4
                        ifilt = choicestbl.subj == subjl(isubj) & choicestbl.condition == icond;
                    end

                    choices = choicestbl.choices(ifilt); % will be replaced with random choices later down
                    choices = cellfun(@(x) x(~isnan(x)), choices, 'UniformOutput', false);      % remove trailing nans
                    choices = cellfun(@(x) randi(2, size(x)), choices, 'UniformOutput', false); % REPLACE WITH RANDOM CHOICES

                    targetc = num2cell(2*choicestbl.targetcolor(ifilt)-3); % -1 | +1 target colours
                    nsample = num2cell(choicestbl.nsamples(ifilt));

                    x       = -1:0.001:+1;
                    perr    = 1/3;
                    cfac    = fzero(@(b)trapz(x(x > 0),1./(1+exp(x(x > 0)*b))/trapz(x,1./(1+exp(x*b))))-perr,[0,10]);

                    cs      = choicestbl.colorseen(ifilt);
                    cs      = cellfun(@(x,b) x(:,1:size(b,2))*cfac, cs, choices, 'UniformOutput', false); % remove trailing nans & multiply by cfac
                    cs      = cellfun(@(x) cumsum(x, 2, 'omitnan'), cs, 'UniformOutput', false);          % accumulate the colour values

                    % compute vopt & copt
                    [~, opt] = cellfun(@(x,t) max(x.*t, [], 1), cs, targetc, 'UniformOutput', false); % get index of option closest to target
                    temp     = cellfun(@(a,b) a == b, choices, opt, 'UniformOutput', false);          % compare choices to copt choices
                    temp     = cellfun(@(x) [x nan(1, 20-size(x,2))], temp, 'UniformOutput', false);
                    temp     = cell2mat(temp);
                    rnd_vopt(isubj, :, icond, ifact) = nanmean(temp, 1); % average across subj's sequences

                    [~, opt] = cellfun(@(x) min(abs(x), [], 1), cs, 'UniformOutput', false); % get index of the option with smallest acc value
                    temp = cellfun(@(a,b) a == b, choices, opt, 'UniformOutput', false);     % compare choices to copt choices
                    temp = cellfun(@(x) [x nan(1, 20-size(x,2))], temp, 'UniformOutput', false);
                    temp = cell2mat(temp);
                    rnd_copt(isubj, :, icond, ifact) = nanmean(temp, 1); % average across subj's sequences

                end%fact
            end%cond
        end%subj

        % compare random choice direction to chance and store cluster values
        temp = squeeze(rnd_vopt(:,:,whichcondition,4));
        [~, pvals, ~, tvals] = ttest(temp, 0.5);
        signif = pvals < 0.05;
        clustr = bwconncomp(signif);

        if all(signif == 0) % no time point is significant
            peakt_dis(it) = 0;
        elseif clustr.NumObjects == 1 % single cluster
            peakt_dis(it) = max(abs(tvals.tstat));
            clust_dis(it) = clustr.PixelIdxList;
        elseif clustr.NumObjects > 1 % multiple clusters, keep that with highest tvalue
            [tpeak, idx] = max(abs(tvals.tstat));
            temp = cell2mat(cellfun(@(x) sum(x == idx), clustr.PixelIdxList, 'UniformOutput', false));
            temp = find(temp == 1);
            peakt_dis(it) = tpeak;
            clust_dis(it) = clustr.PixelIdxList(temp);
        end
    end

    % get final pvalue against random agents vs. chance
    fprintf('for original cluster no %d\n', ic);
    fprintf('peak t value = %0.2f\n', og_peakt(ic));
    fprintf('     p value = %0.3f\n', sum(peakt_dis >= og_peakt(ic)) / nit);
    fprintf('cluster choices:');
    cell2mat(og_clust(ic))'
    fprintf('\n');

end % for each original cluster


%% DIRECT COMPARISON BETWEEN CONDITIONS BY SWAPPING CONDITION LABELS :  
%%%%%%%%%%%%%%%%%%%%%
% this needs to be toggled manually
% either testing vopt or copt, comparing different conditions
% it's always against random agents
% so manually set the variable whichcondition (to pairs of 1 2 3)
% and whether we're looking at target directed choices (vopt)
% or uncertainty directed choices (copt)
%%%%%%%%%%%%%%%%%%%%%


clearvars og_peakt og_clust peakt_dis clust_dis

whichconditions = [2 3];

og_choices = squeeze(sub_copt(:,:,whichconditions,4));
[~, pvals, ~, tvals] = ttest(og_choices(:,:,1), og_choices(:,:,2));
signif = pvals < 0.05;
clustr = bwconncomp(signif);
for ic = 1:clustr.NumObjects
    og_peakt(ic) = max(abs(tvals.tstat(clustr.PixelIdxList{ic}')));
end
og_clust = clustr.PixelIdxList;


% permute nit time, reversing conditions for half of the participants:
rng(1);
nit = 1e3;

for ic = 1:numel(og_peakt) % for each cluster
    peakt_dis = nan(nit, 1);
    clust_dis = cell(nit,1);
    for it = 1:nit
        permuted = og_choices;
        selected_subji = randi(size(og_choices,1), [1, floor(size(og_choices,1)/2)]);
        permuted(selected_subji, : , 1) = og_choices(selected_subji, : , 2);
        permuted(selected_subji, : , 2) = og_choices(selected_subji, : , 1);
        [~, pvals, ~, tvals] = ttest(permuted(:,:,1), permuted(:,:,2));
        signif = pvals < 0.05;
        clustr = bwconncomp(signif);
        
        if all(signif == 0) % no time point is significant
            peakt_dis(it) = 0;
        elseif clustr.NumObjects == 1 % single cluster
            peakt_dis(it) = max(abs(tvals.tstat));
            clust_dis(it) = clustr.PixelIdxList;
        elseif clustr.NumObjects > 1 % multiple clusters, keep that with highest tvalue
            [tpeak, idx] = max(abs(tvals.tstat));
            temp = cell2mat(cellfun(@(x) sum(x == idx), clustr.PixelIdxList, 'UniformOutput', false));
            temp = find(temp == 1);
            peakt_dis(it) = tpeak;
            clust_dis(it) = clustr.PixelIdxList(temp);
        end
        
    end

    fprintf('for original cluster no %d\n', ic);
    fprintf('peak t value = %0.2f\n', og_peakt(ic));
    fprintf('     p value = %0.3f\n', sum(peakt_dis >= og_peakt(ic)) / nit);
    fprintf('cluster choices:');
    cell2mat(og_clust(ic))'
    fprintf('\n');
end




















