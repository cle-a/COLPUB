function modelfit_fun_fit_modelbound_evi(expename,subj,icond,binit,ifact,loo)
%  FUN_FIT_MODELBOUND_EVI  Function for fitting model with initial sampling
%                          phase constrained by bound on accumulated evidence
%
%  Usage: FUN_FIT_MODELBOUND_EVI(expename,isubj,icond,binit)
%
%  Valentin Wyart <valentin.wyart@inserm.fr>
%
% Clemence fit_fun_fit_modelbound_evi(expename,subj,icond,binit,ifact,loo)
% where expename is the experiment name (COLEXP, COLREP, COLMID)
%        subj  = subject *NUMBER*, not the isubj
%        icond = condition number (1:DRAW, 2:GUESS, 3:FIND)
%        binit = bound type (true:initial, false:permanent)
%        ifact = counterfactual subcondition (1:GG, 2:BB, 3:GB, 4:all together)
%        loo   = crossvalidation switch ('doloo' = do crossvalidated fits, 'noloo' = don't)
%
%  needs access to ./Dat/dat_choicestbl_xxx to get all sequences data
%  needs bads toolbox on the path

% check input arguments
if nargin < 5
    error('Missing input arguments!');
end

% set output filename and folder: NB: saving by subj number rather than index of subject in the matrix
foldname = sprintf('./Fits/%s', expename);
if ~exist(foldname)
    mkdir(foldname)
end
if strcmp(loo, 'doloo')
    filename = sprintf('fit_modelbound_evi_%s_subj%03d_icond%d_binit%d_ifact%d_crossval.mat', ...
        expename,subj,icond,binit,ifact);
elseif strcmp(loo, 'noloo')
    filename = sprintf('fit_modelbound_evi_%s_subj%03d_icond%d_binit%d_ifact%d.mat', ...
        expename,subj,icond,binit,ifact);
end

% add bads toolbox to path
addpath ./Toolboxes/
addpath ./Toolboxes/bads

% set analysis parameters
theta = 0.05:0.05:5.00; % tested bound levels (logLR)
gamma = 0.1;            % fixed bound standard deviation (logLR)
nv = numel(theta);


% extract relevant information from data 
load(sprintf('Dat/dat_choicestbl_%s.mat', expename), 'choicestbl');
if ifact == 4
    thisfilt = choicestbl.subj == subj & choicestbl.condition == icond;
else
    thisfilt = choicestbl.subj == subj & choicestbl.condition == icond & choicestbl.symmetry == ifact;
end

% create configuration structure
cfg     = [];

cfg.choices = cellfun(@(x) x(~isnan(x)), choicestbl.choices(thisfilt), 'UniformOutput', false); % remove trailing NaNs
cfg.conditn = choicestbl.condition(thisfilt);
cfg.targetc = ( 2* choicestbl.targetcolor(thisfilt) -3 ) .* (cfg.conditn ~= 2) ;
cfg.colvals = cellfun(@(x) x(:,1:(sum(~isnan(x(1,:))))), choicestbl.coloroptions(thisfilt), 'UniformOutput', false); % remove trailing NaNs
cfg.ctfactn = choicestbl.symmetry(thisfilt);
cfg.binit   = binit; % set bound type
cfg.gamma   = gamma; % set bound standard deviation

% use fmincon for fitting
cfg.fitalgo = 'fmincon';

% fit model without bound
cfg.theta = 0;
cfg.nrun  = 10; % multiple starting points
out0_fmin = modelfit_fit_modelbound_evi(cfg);

% set initial values
cfg.pini.betav = out0_fmin.betav;
cfg.pini.betac = out0_fmin.betac;
cfg.pini.betar = out0_fmin.betar;
cfg.nrun       = 1; % single starting point

% fit model with bound
out1_fmin = cell(1,nv);
for iv = 1:nv
    cfg.theta = theta(iv); % set bound level
    out1_fmin{iv} = modelfit_fit_modelbound_evi(cfg);
end

% use BADS for fitting
cfg.fitalgo = 'bads';

% fit model without bound
fprintf('without bound S_%02d\n', subj)
cfg.theta     = 0;
out0_bads     = modelfit_fit_modelbound_evi(cfg);

if strcmp(loo, 'doloo')
    out0_bads_loo = modelfit_fit_modelbound_evi_loo(cfg);
end

% set initial values
[~,iv] = max(cellfun(@(s)getfield(s,'llh'),out1_fmin));
cfg.pini.betav = out1_fmin{iv}.betav;
cfg.pini.betac = out1_fmin{iv}.betac;
cfg.pini.betar = out1_fmin{iv}.betar;
cfg.pini.theta = out1_fmin{iv}.theta;
cfg.pini.omega = out1_fmin{iv}.omega;

% fit model with bound
fprintf('with bound S_%02d\n', subj)
cfg           = rmfield(cfg,'theta'); % rm theta = free, fitted parm
out1_bads     = modelfit_fit_modelbound_evi(cfg);
if strcmp(loo, 'doloo')
    out1_bads_loo = modelfit_fit_modelbound_evi_loo(cfg);
end


% save fitting output to file
if strcmp(loo, 'doloo')
    save(fullfile(foldname,filename), ...
        'expename', 'subj',...
        'theta','gamma','nv', ...
        'out0_fmin','out1_fmin',...
        'out0_bads','out1_bads',...
        'out0_bads_loo','out1_bads_loo', 'cfg');
    
elseif strcmp(loo, 'noloo')
    save(fullfile(foldname,filename), ...
        'expename', 'subj',...
        'theta','gamma','nv', ...
        'out0_fmin','out1_fmin',...
        'out0_bads','out1_bads', 'cfg');
end


end