%%%%%%%%%%%
%  LAUNCHES THE MODEL FITTING FUNCTION PER SUBCONDITION
%   - modelfit_fun_fit_modelbound_evi(
%       expename,
%       isubj_fit(jobID),
%       icond_fit(jobID),
%       binit_fit(jobID),
%       ifact_fit(jobID),
%       yesnoloo);
%   - needs access to choicestbl_xxx to get all participants ID
%%%%%%%%%%%

% function modelfit_run_fit_modelbound_evi(slurm_array_task_id)
% jobID = slurm_array_task_id;


%%% which dataset
expename = 'COLEXP';
ncond    = 2;

% get subjlist from choicestbl, excluding participants from behavioural indices
load(sprintf('Dat/dat_choicestbl_%s.mat', expename), 'choicestbl');
subjlist = unique(choicestbl.subj(choicestbl.excludepart == 0));

% binit = 1 = bound only initial
binit = 1;

% fit all conditions by default
icond = 1:ncond;

% fit all facts by default
ifact = 1:4; % ifact = 4;

% do cross-validation by default
yesnoloo = 'doloo';

% define list of fits
[icond_fit,binit_fit,isubj_fit,ifact_fit] = ndgrid(icond,binit,subjlist,ifact);
isubj_fit = isubj_fit(:);
icond_fit = icond_fit(:); 
binit_fit = binit_fit(:);
ifact_fit = ifact_fit(:);

% run the number of fits
nfit = numel(isubj_fit);
for jobID = 1:nfit
    modelfit_fun_fit_modelbound_evi(expename,isubj_fit(jobID),icond_fit(jobID),binit_fit(jobID), ifact_fit(jobID), yesnoloo);
end

fprintf('\ndone!\n');





