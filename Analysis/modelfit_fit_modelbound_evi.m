function [out] = modelfit_fit_modelbound_evi(cfg)

% check input arguments
if ~isfield(cfg,'same1st')
    cfg.same1st = false;
end
if ~isfield(cfg,'fitalgo')
    cfg.fitalgo = 'fmincon';
end
if ~isfield(cfg,'nrun')
    cfg.nrun = 1;
end
if ~isfield(cfg,'nsim')
    cfg.nsim = 1;
end
if ~isfield(cfg,'verbose')
    cfg.verbose = 0;
end

% set fitting parameters
peps = 1e-6; % infinitesimal choice probability
perr = 1/3;  % generative fraction of missorted colors

% define logit and sigmoid functions
logit = @(x)log(x./(1-x));
sigmd = @(x)1./(1+exp(-x));

% get color-to-evidence factor
cfac = get_cfac(perr);

% get number of sequences
nseq = numel(cfg.choices);

if cfg.conditn == 2
    % do not fit sensitivity to value
    cfg.betav = 0;
end

if isfield(cfg,'theta') && cfg.theta == 0
    % do not fit other bound parameters
    cfg.omega = 0;
    cfg.gamma = 0;
end

% define model parameters
pnam = cell(6,1); % name
pini = nan(6,1);  % initial value
pmin = nan(6,1);  % lower bound
pmax = nan(6,1);  % upper bound
pplb = nan(6,1);  % plausible lower bound
ppub = nan(6,1);  % plausible upper bound
pfun = cell(6,1); % prior function

% betav = sensitivity to value ~ normal(0,1)
pnam{1} = 'betav';
pini(1) = 0;
pmin(1) = -10;
pmax(1) = +10;
pplb(1) = -2;
ppub(1) = +2;
pfun{1} = @(x)normpdf(x,0,1);

% betac = sensitivity to certainty ~ normal(0,1)
pnam{2} = 'betac';
pini(2) = 0;
pmin(2) = -10;
pmax(2) = +10;
pplb(2) = -2;
ppub(2) = +2;
pfun{2} = @(x)normpdf(x,0,1);

% betar = repetition bias ~ normal(0,2)
pnam{3} = 'betar';
pini(3) = 0;
pmin(3) = -10;
pmax(3) = +10;
pplb(3) = -4;
ppub(3) = +4;
pfun{3} = @(x)normpdf(x,0,2);

% theta = bound level ~ logit(uniform(0.5,1))
pnam{4} = 'theta';
pini(4) = logit(0.8);
pmin(4) = 0;
pmax(4) = 10;
pplb(4) = 0.1;
ppub(4) = 5;
pfun{4} = @(x)unifpdf(sigmd(x),0.5,1).*sigmd(x).*(1-sigmd(x));

% omega = bound hardness ~ beta(4,1)
pnam{5} = 'omega';
pini(5) = 0.8;
pmin(5) = 0;
pmax(5) = 1;
pplb(5) = 0.1;
ppub(5) = 0.9;
pfun{5} = @(x)betapdf(x,4,1);

% gamma = bound standard deviation ~ exp(0.1)
pnam{6} = 'gamma';
pini(6) = 0.1;
pmin(6) = 0.001;
pmax(6) = 1;
pplb(6) = 0.01;
ppub(6) = 0.5;
pfun{6} = @(x)exppdf(x,0.1);

npar = numel(pnam);

% set initial parameter values
if isfield(cfg,'pini')
    for ipar = 1:npar
        if isfield(cfg.pini,pnam{ipar})
            pini(ipar) = cfg.pini.(pnam{ipar});
        end
    end
end

% define fixed parameters
pfix = nan(npar,1);
for i = 1:npar
    if isfield(cfg,pnam{i})
        pfix(i) = cfg.(pnam{i});
    end
end

% define fitted parameters
ifit = nan(npar,1);
pfit_ini = [];
pfit_min = [];
pfit_max = [];
pfit_plb = [];
pfit_pub = [];
n = 1;
for i = 1:npar
    if isnan(pfix(i))
        ifit(i) = n;
        pfit_ini = cat(2,pfit_ini,pini(i));  
        pfit_min = cat(2,pfit_min,pmin(i));
        pfit_max = cat(2,pfit_max,pmax(i));
        pfit_plb = cat(2,pfit_plb,pplb(i));
        pfit_pub = cat(2,pfit_pub,ppub(i));
        n = n+1;
    end
end
nfit = numel(pfit_ini);
nobs = sum(cellfun(@length,cfg.choices));

if nfit > 0
    % set display level
    switch cfg.verbose
        case 0, opt_disp = 'none';
        case 1, opt_disp = 'final';
        case 2, opt_disp = 'iter';
    end
    nrun = cfg.nrun;

    if ~exist(lower(cfg.fitalgo),'file')
        error('%s not found in path!',lower(cfg.fitalgo));
    end
    switch lower(cfg.fitalgo)
        
        case 'fmincon'
            
            % configure fmincon
            options = optimoptions('fmincon');
            options.Algorithm = 'interior-point'; % fitting algorithm
            options.FunValCheck = 'on'; % check objective function
            options.MaxFunEvals = 1e6; % maximum number of function evaluations
            options.TolX = 1e-20; % tolerance on objective function
            options.Display = opt_disp; % display level
            
            % fit model
            fval   = nan(1,nrun);
            xhat   = cell(1,nrun);
            output = cell(1,nrun);
            for irun = 1:nrun
                if nrun > 1
                    % set random starting point
                    for i = 1:nfit
                        pfit_ini(i) = unifrnd(pfit_plb(i),pfit_pub(i));
                    end
                end
                % fit model
                [xhat{irun},fval(irun),~,output{irun}] = ...
                    fmincon(@fmin, ...
                    pfit_ini,[],[],[],[],pfit_min,pfit_max,[],options);
            end
            
            % find best fit among random starting points
            [fval,irun] = min(fval);
            xhat        = xhat{irun};
            output      = output{irun};
            
            % get best-fitting parameters
            [~,phat] = fmin(xhat);
            
            % store best-fitting parameters
            out = cell2struct(phat,pnam);
            
            % account for fixed bound level as implicit parameter
            if isfield(cfg,'theta') && cfg.theta > 0
                nfit = nfit+1;
            end
            out.nfit = nfit; % number of fitted parameters
            out.nobs = nobs; % number of observations
            
            % get maximum log-likelihood
            out.llh = sum(get_lseq(phat{:}));
            
            % get AICc and BIC
            out.aic = -2*out.llh+2*nfit+2*nfit*(nfit+1)/(nobs-nfit+1);
            out.bic = -2*out.llh+nfit*log(nobs);
            
        case 'bads'
            
            % configure BADS
            options = bads('defaults');
            options.UncertaintyHandling = false; % noise-free objective function
            options.NoiseFinalSamples = 100; % number of final samples
            options.Display = opt_disp; % display level
            
            % fit model
            fval   = nan(1,nrun);
            xhat   = cell(1,nrun);
            output = cell(1,nrun);
            for irun = 1:nrun
                if nrun > 1
                    % set random starting point
                    for i = 1:nfit
                        pfit_ini(i) = unifrnd(pfit_plb(i),pfit_pub(i));
                    end
                end
                % fit model
                [xhat{irun},fval(irun),~,output{irun}] = ...
                    bads(@fmin, ...
                    pfit_ini,pfit_min,pfit_max,pfit_plb,pfit_pub,[],options);
            end
            % find best fit among random starting points
            [fval,irun] = min(fval);
            xhat        = xhat{irun};
            output      = output{irun};
            
            % get best-fitting parameters
            [~,phat] = fmin(xhat);
            
            % store best-fitting parameters
            out = cell2struct(phat,pnam);
            
            % account for fixed bound level as implicit parameter
            if isfield(cfg,'theta') && cfg.theta > 0
                nfit = nfit+1;
            end
            out.nfit = nfit; % number of fitted parameters
            out.nobs = nobs; % number of observations
            
            % get maximum log-likelihood
            out.llh = sum(get_lseq(phat{:}));
            
            % get AICc and BIC
            out.aic = -2*out.llh+2*nfit+2*nfit*(nfit+1)/(nobs-nfit+1);
            out.bic = -2*out.llh+nfit*log(nobs);
            
        case 'vbmc'

            % configure VBMC
            options = vbmc('defaults');
            options.MaxIter = 300; % maximum number of iterations
            options.MaxFunEvals = 500; % maximum number of function evaluations
            options.SpecifyTargetNoise = false; % noise-free log-posterior function
            options.Display = opt_disp; % display level
            
            % fit model
            [vp,elbo,~,~,output] = vbmc(@(x)getlp(x), ...
                pfit_ini,pfit_min,pfit_max,pfit_plb,pfit_pub,options);
            
            % generate 10^6 samples from the variational posterior
            xsmp = vbmc_rnd(vp,1e6);
            
            % get sample statistics
            xmap = vbmc_mode(vp); % posterior mode
            xavg = mean(xsmp,1); % posterior mean
            xstd = std(xsmp,[],1); % posterior s.d.
            xcov = cov(xsmp); % posterior covariance matrix
            xmed = median(xsmp,1); % posterior medians
            xqrt = quantile(xsmp,[0.25,0.75],1); % posterior 1st and 3rd quartiles
            
            % get best-fitting parameters
            [~,phat] = getlp(xavg);
            
            % store best-fitting parameters
            out = cell2struct(phat,pnam);

            % store variational posterior solution
            out.vp = vp;
            
            % get ELBO (expected lower bound on log-marginal likelihood)
            out.elbo = elbo; % estimate
            out.elbo_sd = output.elbo_sd; % standard deviation
            
            % get parameter values
            out.xnam = pnam(isnan(pfix)); % fitted parameters
            out.xmap = xmap; % posterior mode
            out.xavg = xavg; % posterior mean
            out.xstd = xstd; % posterior s.d.
            out.xcov = xcov; % posterior covariance matrix
            out.xmed = xmed; % posterior median
            out.xqrt = xqrt; % posterior 1st and 3rd quartiles
            
        otherwise
            error('Undefined fitting algorithm!');
    
    end
    
    % store extra information
    out.out = output; % output structure
    out.cfg = cfg; % configuration structure
    
else
    
    % get provided parameters
    [~,phat] = fmin([]);
    
    % store provided parameters
    out = cell2struct(phat,pnam);
    
    % get sequences
    [out.lseq,out.xseq,out.xcol,out.vopt, out.copt] = get_lseq(phat{:});
    
    if cfg.nsim > 0
        % simulate model choices
        nsim = cfg.nsim;
        out.mch = cell(1,nsim);
        out.mfq = cell(1,nsim);
        for isim = 1:nsim
            [out.mch{isim},out.mfc{isim},out.mfq{isim},...
                out.mvopt{isim},out.mcopt{isim}, out.mxc{isim}, out.mxe{isim}] = sim_mch(phat{:});
            out.mchnan{isim} = cellfun(@(x) [x nan(1, 20-size(x,2))], out.mch{isim}, 'UniformOutput', false);
        end
    end
        
    % store extra information
    out.cfg = cfg; % configuration structure
    
end

    function [f,pfit] = fmin(p)
        % get parameter set
        pfit = cell(npar,1);
        for i = 1:npar
            if isnan(pfix(i)) % free parameter
                pfit{i} = p(ifit(i));
            else % fixed parameter
                pfit{i} = pfix(i);
            end
        end
        % get negative log-likelihood
        f = -sum(get_lseq(pfit{:}));
    end

    function [lp,pfit] = getlp(p) % get log posterior
        % get parameter set
        pfit = cell(npar,1);
        for i = 1:npar
            if isnan(pfix(i)) % free parameter
                pfit{i} = p(ifit(i));
            else % fixed parameter
                pfit{i} = pfix(i);
            end
        end
        % get log-prior
        l0 = 0;
        for k = 1:npar
            if isnan(pfix(k)) % free parameter
                l0 = l0+log(pfun{k}(pfit{k}));
            end
        end
        % get log-likelihood
        ll = sum(get_lseq(pfit{:}));
        % get log-posterior
        lp = ll+l0;
    end

    function [lseq,xseq,xcol,vopt,copt] = get_lseq(betav,betac,betar,theta,omega,gamma)
        % get log-likelihood
        lseq = nan(nseq,1); % log posterior of the sequence = sum of the lp of the samples?
        xseq = nan(nseq,2); % final accumulated col value after the last outcome, for each option
        xcol = cell(nseq,1);% accumulated col values of each option at each choice 
        for iseq = 1:nseq
            nsmp = numel(cfg.choices{iseq});
            lsmp = nan(nsmp,1);
            xc = nan(nsmp,2);
            pc = nan(nsmp,2);
            bc = true(1,2);
            if theta == 0
                bc(:) = false;
            end
            xc(1,:) = 0;
            pc(1,:) = 0.5;
            lsmp(1) = log(0.5);
            for ismp = 2:nsmp
                ch = cfg.choices{iseq}(ismp-1);
                un = 3-ch;
                xc(ismp,ch) = xc(ismp-1,ch)+cfg.colvals{iseq}(ch,ismp-1)*cfac;
                xc(ismp,un) = xc(ismp-1,un);
                dv =    betav*(xc(ismp,1)-xc(ismp,2))*cfg.targetc(iseq);
                dv = dv+betac*(abs(xc(ismp,1))-abs(xc(ismp,2)));
                dv = dv+betar*(3-2*ch);
                if bc(ch) == true
                    if gamma == 0
                        pinb = abs(xc(ismp,ch)) < theta;
                    else
                        pinb = 1-gamcdf(abs(xc(ismp,ch)),theta^2/gamma^2,gamma^2/theta);
                    end
                    pc_inb = nan(1,2);
                    pc_inb(ch) = omega;
                    pc_inb(un) = 1-omega;
                    pc_out = nan(1,2);
                    pc_out(1) = 1/(1+exp(-dv));
                    pc_out(2) = 1-pc_out(1);
                    pc(ismp,:) = pc_inb*pinb+pc_out*(1-pinb);
                else
                    pc(ismp,1) = 1/(1+exp(-dv));
                    pc(ismp,2) = 1-pc(ismp,1);
                end
                ch_prv = ch;
                ch_cur = cfg.choices{iseq}(ismp);
                if ch_cur ~= ch_prv
                    bc(ch_prv) = false;
                end
                ch = cfg.choices{iseq}(ismp);
                un = 3-ch;
                lsmp(ismp) = log(peps+(1-peps*2)*pc(ismp,ch));
            end
            
            % compute vopt and copt choices for participants:
            [~, tempopt] = max(xc*cfg.targetc(iseq), [], 2);
            vopt{iseq}  = [cfg.choices{iseq} == tempopt' nan(1, 20-size(cfg.choices{iseq},2))];
            
            [~, tempopt] = min(abs(xc), [], 2);
            copt{iseq}  = [cfg.choices{iseq} == tempopt' nan(1, 20-size(cfg.choices{iseq},2))];
            
            xc(ismp+1,ch) = xc(ismp,ch)+cfg.colvals{iseq}(ch,ismp)*cfac;
            xc(ismp+1,un) = xc(ismp,un);
            xseq(iseq,:)  = xc(ismp+1,:);
            lseq(iseq)    = sum(lsmp);
            xcol{iseq}    = xc;
            
        end
    end

    function [mch,mfc,mfq,mvopt,mcopt,mxc,mxe] = sim_mch(betav,betac,betar,theta,omega,gamma)
        % simulate model choices
        mch = cell(nseq,1);
        mfc = nan(nseq,1);
        mfq = nan(nseq,1);
        mxc = cell(nseq,1);
        mvopt = cell(nseq,1);
        mcopt = cell(nseq,1);
        for iseq = 1:nseq
            nsmp = numel(cfg.choices{iseq});
            mch{iseq} = nan(1,nsmp);
            xc = nan(nsmp,2);
            bc = true(1,2);
            xc(1,:) = 0;
            if theta == 0
                bc(:) = false;
            end
            if cfg.same1st
                mch{iseq}(1) = cfg.choices{iseq}(1);
            else
                mch{iseq}(1) = ceil(2*rand);
            end
            for ismp = 2:nsmp
                % update beliefs
                ch = mch{iseq}(ismp-1);
                un = 3-ch;
                xc(ismp,ch) = xc(ismp-1,ch)+cfg.colvals{iseq}(ch,ismp-1)*cfac;
                xc(ismp,un) = xc(ismp-1,un);
                % update bound
                if bc(ch) == true
                    if gamma == 0 % fixed bound
                        xt = theta;
                    else % variable bound
                        xt = gamrnd(theta^2/gamma^2,gamma^2/theta);
                    end
                    bc(ch) = abs(xc(ismp,ch)) < xt;
                end
                % compute decision variable
                dv =    betav*(xc(ismp,1)-xc(ismp,2))*cfg.targetc(iseq);
                dv = dv+betac*(abs(xc(ismp,1))-abs(xc(ismp,2)));
                dv = dv+betar*(3-2*ch);
                % compute choice probability
                pc = nan(1,2);
                if bc(ch) == true
                    pc(ch) = omega;
                    pc(un) = 1-omega;
                else
                    pc(1) = 1/(1+exp(-dv));
                    pc(2) = 1-pc(1);
                end
                % choose
                mch{iseq}(ismp) = 1+(pc(1) < rand);
                % update bound
                ch_prv = ch;
                ch_cur = mch{iseq}(ismp);
                if ch_cur ~= ch_prv
                    bc(ch_prv) = false;
                end
            end
            mfc(iseq) = (xc(nsmp,1)-xc(nsmp,2))*cfg.targetc(iseq);
            mxc{iseq} = xc(end,:); % evidence available for last decision = acquired at sample end-1

            
            % update beliefs for final question // for final Q = which is the colour of THIS bag? \\
            ch = mch{iseq}(nsmp);
            un = 3-ch;
            xe = nan(1,2);
            xe(ch) = xc(nsmp,ch)+cfg.colvals{iseq}(ch,nsmp)*cfac;
            xe(un) = xc(nsmp,un);
            mfq(iseq) = xe(1);
            mxe{iseq} = xe; % evidence available for final question = acquired at sample end
            
            % compute vopt and copt choices for simulations:
            [~, tempopt] = max(xc*cfg.targetc(iseq), [], 2);
            mvopt{iseq}  = [mch{iseq} == tempopt' nan(1, 20-size(mch{iseq},2))];
            mvopt{iseq}(xc(:,1) == xc(:,2)) = nan; % if options are equal, nan
            
            %sum(mvopt{iseq}(2:size(mch{iseq},2))) ~= size(mch{iseq},2)-1
            
            [~, tempopt] = min(abs(xc), [], 2);
            mcopt{iseq}  = [mch{iseq} == tempopt' nan(1, 20-size(mch{iseq},2))];
            mcopt{iseq}(abs(xc(:,1)) == abs(xc(:,2))) = nan; % if options are equal, nan
            
        end
    end
end

function [cfac] = get_cfac(perr)
% get color-to-evidence factor
x = -1:0.001:+1;
cfac = fzero(@(b)trapz(x(x > 0),1./(1+exp(x(x > 0)*b))/trapz(x,1./(1+exp(x*b))))-perr,[0,10]);
end


