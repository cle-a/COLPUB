%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% aggregates the individual files from modelfitting
% saves it to ./Dat/dat_modelbound_evi_bads_xxx_crossval.mat
% or ./Dat/dat_modelbound_evi_bads_xxx.mat for no crossvalidation
% where xxx is sample name
%
% uses Valentin's GetConsecutiveValues
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% clear workspace
clear all
close all
clc

addpath ./Toolboxes

% set analysis parameters
expename = 'COLEXP'; % experiment
nsim     = [];       % number of simulations
yesnoloo = 'doloo';
nbinit   = 2;
facts    = 1:4;
nfact    = numel(facts);

% identify folder name
foldname = sprintf('./Fits/%s/',expename);

% get number of conditions
switch expename
    case 'COLEXP' % experiment 1
        subjlist = setdiff(01:30,[10,18,23]);
        ncond = 2;
    case 'COLREP' % experiment 2
        subjlist = setdiff(01:30,[09,10,24]);
        ncond = 2;
    case 'COLMID' % experiment 3
        subjlist = setdiff(01:38,[01,03,04,05,24,25,35]);
        ncond = 3;
    otherwise
        error('Undefined experiment name!');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% get number of subjects
nsubj = numel(dir(fullfile(foldname,['fit_modelbound_evi_' expename '*_icond1_binit1_ifact4*_crossval.mat'])));
fprintf('Found %d crossvalidated subjects.\n',nsubj);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% best-fitting parameters for model with initial bound, BADS fits, not loo
betav_fit = nan(nsubj,ncond,nfact);
betac_fit = nan(nsubj,ncond,nfact);
betar_fit = nan(nsubj,ncond,nfact);
theta_fit = nan(nsubj,ncond,nfact);
omega_fit = nan(nsubj,ncond,nfact);
gamma     = nan(nsubj,ncond,nfact);

% AICc from fmincon, BADS, and llh from BADS loo:
aic0_fmin = nan(nsubj,  1,ncond,nfact); % fmincon, without bound
aic1_fmin = nan(nsubj,100,ncond,nfact); % fmincon, with fixed bound levels
aic0_bads = nan(nsubj,ncond,nfact); % BADS, without bound
aic1_bads = nan(nsubj,ncond,nfact); % BADS, with bound
llh0_bads = nan(nsubj,ncond,nfact); % llh from loo procedure (bads fits)
llh1_bads = nan(nsubj,ncond,nfact); % llh from loo procedure (bads fits)

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for isubj = 1:nsubj
    fprintf('isubj %03d\n', isubj);
    subj = subjlist(isubj);
    for icond = 1:ncond
        for ifact = 1:nfact
            thisfact = facts(ifact);
            for binit = 1 % is whether it's inital or permanent over the sequence
                
                % load fitting output
                filename = sprintf('fit_modelbound_evi_%s_subj%03d_icond%d_binit%d_ifact%d_crossval.mat', ...
                    expename,subj,icond,binit == 1,thisfact); % so 1=b initial, 0=b permanent
                load(fullfile(foldname,filename));
                
                % get AICc curves from fmincon fits
                aic0(isubj,1,icond,thisfact) = out0_fmin.aic;
                aic1(isubj,:,icond,thisfact) = cellfun(@(s)getfield(s,'aic'),out1_fmin);
                
                % get AICc from BADS fits
                aic0_bads(isubj,icond,thisfact) = out0_bads.aic;
                aic1_bads(isubj,icond,thisfact) = out1_bads.aic;

                if binit == 1 % initial bound
                    % get best-fitting parameters
                    betav_fit(isubj,icond,thisfact) = out1_bads.betav;
                    betac_fit(isubj,icond,thisfact) = out1_bads.betac;
                    betar_fit(isubj,icond,thisfact) = out1_bads.betar;
                    theta_fit(isubj,icond,thisfact) = out1_bads.theta;
                    omega_fit(isubj,icond,thisfact) = out1_bads.omega;
                    gamma(isubj,icond,thisfact)     = gamma;

                    
                    cfg = out1_bads.cfg; % we're taking results of BADS
                    cfg.betav = out1_bads.betav;
                    cfg.betac = out1_bads.betac;
                    cfg.betar = out1_bads.betar;
                    cfg.theta = out1_bads.theta;
                    cfg.omega = out1_bads.omega;
                    cfg.gamma = out1_bads.gamma;
                    
                    % saving simulation choices
                    if ~(isempty(nsim))
                        cfg.nsim = nsim;
                        out_sim  = fit_modelbound_evi(cfg); %%% running simulations from the fitting script
                        ch_sim(isubj,:,icond,thisfact)   = out_sim.mch;
                        vopt_sim{isubj,:,icond,thisfact} = out_sim.mvopt;
                        copt_sim{isubj,:,icond,thisfact} = out_sim.mcopt;
                    end
                    
                    % get llh from bads if we've done cross validation
                    if exist('out0_bads_loo', 'var')
                        llh0_bads(isubj,icond,thisfact) = out0_bads_loo.llh_tst;
                        llh1_bads(isubj,icond,thisfact) = out1_bads_loo.llh_tst;
                    end
                    
                    
                end
                
            end
        end
    end
end


if strcmp(yesnoloo, 'doloo') && ~isempty(nsim)
    save(sprintf('./Dat/dat_modelbound_evi_bads_%s_crossval.mat', expename), ...
        'betav_fit',...
        'betac_fit',...
        'betar_fit',...
        'omega_fit',...
        'theta_fit',...
        'gamma',    ...
        'aic0_bads', 'aic1_bads', 'aic0_fmin', 'aic1_fmin',...
        'llh0_bads', 'llh1_bads',...
        'ch_sim', 'vopt_sim', 'copt_sim', 'nsim');

elseif strcmp(yesnoloo, 'doloo') && isempty(nsim)
    save(sprintf('./Dat/dat_modelbound_evi_bads_%s_crossval.mat', expename), ...
        'betav_fit',...
        'betac_fit',...
        'betar_fit',...
        'omega_fit',...
        'theta_fit',...
        'gamma',    ...
        'aic0_bads', 'aic1_bads', 'aic0_fmin', 'aic1_fmin',...
        'llh0_bads', 'llh1_bads');    

elseif strcmp(yesnoloo, 'noloo') && ~isempty(nsim)
    save(sprintf('./Dat/dat_modelbound_evi_bads_%s.mat', expename), ...
        'betav_fit',...
        'betac_fit',...
        'betar_fit',...
        'omega_fit',...
        'theta_fit',...
        'gamma',    ...
        'aic0_bads', 'aic1_bads', 'aic0_fmin', 'aic1_fmin',...
        'ch_sim', 'vopt_sim', 'copt_sim', 'nsim');
    
elseif strcmp(yesnoloo, 'noloo') && isempty(nsim)
    save(sprintf('./Dat/dat_modelbound_evi_bads_%s.mat', expename), ...
        'betav_fit',...
        'betac_fit',...
        'betar_fit',...
        'omega_fit',...
        'theta_fit',...
        'gamma',    ...
        'aic0_bads', 'aic1_bads', 'aic0_fmin', 'aic1_fmin');
    
end

fprintf('\nsaved!\n');
