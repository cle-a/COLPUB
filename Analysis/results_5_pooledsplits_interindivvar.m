%%% PLOTTING DIFFERENCES BTW PARTICIPANTS WITH/WITHOUT INITIAL SAMPLING
%%% pools all datasets
% % % %%% splits participants based on best fitting model _in GUESS sequences_ (with/without initial sampling phase)
% % % %%% plots: performance      in both conditions, for ini./no ini. participants
% % % %%%        choice direction ""
% % % %%%        fraction repeat  ""
% % % %%%        performance      ""
% % % %%%        betav in draw    ""
% % % %%%        betar in both    "" + correlation of betars in GUESS/DRAW conditions
% % % %%%        betac in both    ""

% TTEST2 UNEQUAL VARIANCES: from MATLAB:
% 'unequal', to perform the test
% assuming that the two samples come from normal
% distributions with unknown and unequal variances.
% ttest2 uses Satterthwaite's approximation for the
% effective degrees of freedom.

%% POOL DATA & SPLIT PARTICIPANTS
close all
clearvars
clc

%%%%%% DIARY ON
diaryfile = sprintf('./Figs/splits/statsprintout_thresholdsplits.txt');
if ~exist('./Figs/splits', 'dir')
    mkdir('./Figs/splits')
elseif exist(diaryfile, 'file')
    delete(diaryfile);
end
diary(diaryfile);
diary on
fprintf('------------------------\n------------------------\n THRESHOLD SPLITS \n------------------------\n------------------------\n');
fprintf('Participants split based on the cross-validated (loo) likelihood estimates of the two models\n');
%%%%%%

addpath ./Toolboxes
addpath ./Toolboxes/draw
addpath ./Toolboxes/spm12 % for spm_BMS

% PLOTTING PARAMETERS
lcol = lines;
col(1,:) = lcol(4,:);
col(2,:) = lcol(5,:);
col(3,:) = lcol(3,:);
gray     = [0.5 0.5 0.5];
fsz      = 8;
psz      = 2;
figf     = '-dsvg';

figsdir = './Figs/Splits/';
if ~exist(figsdir, 'dir')
    mkdir(figsdir)
end

% POOLING CHOICESTBL
expe  = 'POOLED';
expes = {'COLEXP', 'COLREP', 'COLMID'};
ctb   = table();
for ixp = 1:3
    xname = expes{ixp};
    load(sprintf('./Dat/dat_choicestbl_%s.mat', xname), 'choicestbl');
    switch ixp
        case 1
            choicestbl = choicestbl(:, [1 2 3 6 7 9 10 11 13 14 16 19 26]);
        case 2
            choicestbl = choicestbl(:, [1 2 3 6 7 9 10 11 13 14 17 20 26]);
        case 3
            choicestbl = choicestbl(:, [1 2 3 6 7 9 10 11 13 14 17 20 26]);
    end
    choicestbl = choicestbl(choicestbl.excludepart == 0,:);
    choicestbl.expe = repelem(ixp, size(choicestbl,1))';
    ctb = [ctb; choicestbl];
end
clearvars choicestbl
choicestbl = ctb;
totalnsubj = 85;
ncond      = 2;
condnames = {'DRAW', 'GUESS'};
factnames = {'both target', 'no target', 'single target'};

% sCOLEXP = setdiff(01:30,[10,18,23]);
% sCOLREP = setdiff(01:30,[09,10,24]);
% sCOLMID = setdiff(01:38,[01,03,04,05,24,25,35]);
% nsubj   = size([sCOLEXP sCOLREP sCOLMID],2);
 
% pooling datasets:
betav   = [];
betac   = [];
betar   = [];
theta   = [];
aicini  = [];
aicnone = [];
for iset = 1:size(expes,2)
    ixp = expes{iset};
    load(sprintf('./Dat/dat_modelbound_evi_bads_%s_crossval.mat', ixp),...
        'betav_fit', 'betac_fit', 'betar_fit', 'theta_fit',...
        'aic0_bads', 'aic1_bads', ...
        'llh0_bads', 'llh1_bads');
    betav    = [betav; betav_fit(:,1:2,4)];
    betac    = [betac; betac_fit(:,1:2,4)];
    betar    = [betar; betar_fit(:,1:2,4)];
    theta    = [theta; theta_fit(:,1:2,4)];
    aicini   = [aicini;  llh1_bads(:,2,4)];
    aicnone  = [aicnone; llh0_bads(:,2,4)];

end

% SPLITTING PARTICIPANTS INTO INI+/INI- 
bpos = aicini > aicnone; % larger crossvalidated llh = more evidence
bneg = aicini < aicnone;
fprintf('***\n***\nsplitting participant based on model in the GUESS games:\nn initial phase = %d\nn no initial phase = %d\n\n',...
        sum(bpos), sum(bneg));
inistatus(:,1) = bpos;
inistatus(:,2) = bneg;


%% PERFORMANCE (last choice and final question accuracy) in good-bad sequences only
%%% last choice
fchoice = nan(totalnsubj,1);
nn      = 0;
for iexp = 1:3
    subjl = unique(choicestbl.subj(choicestbl.expe == iexp))';
    for isubj = subjl
        nn = nn +1;
        temp = choicestbl(choicestbl.expe == iexp & choicestbl.subj == isubj & ...
                          choicestbl.condition == 1 & choicestbl.symmetry == 3,:);
        for it = 1:size(temp,1)
            targ(it) = temp.choices{it}(temp.nsamples(it)) == 1;
        end
        fchoice(nn) = nanmean(targ);         
    end
end
fchoice = fchoice*100;

%%% final question accuracy
fquest = nan(totalnsubj,1);
nn      = 0;
for iexp = 1:3
    subjl = unique(choicestbl.subj(choicestbl.expe == iexp))';
    for isubj = subjl
        nn = nn +1;
        temp = choicestbl(choicestbl.expe == iexp & choicestbl.subj == isubj & ...
                          choicestbl.condition == 2 & choicestbl.symmetry == 3,:);
        fquest(nn) = nanmean(temp.finalchoicecorr);         
    end
end

fquest = fquest*100;

%%% plot
perf   = [fchoice fquest];
for icond = 1:2
    pr{1} = perf(bpos, icond);
    pr{2} = perf(bneg, icond);
    
    plot_violins(pr,[],col(icond,:),1,9,'med_iqr', false);
    hold on
    plot(0:3, repelem(50, 4), ':', 'Color', gray, 'LineWidth', 0.5); % plot mean line
    
    set(gca, 'YLim', [0 100], 'YTick', (0:20:100), 'XTickLabel', {'ini+', 'ini-'});
    ylabel('performance');
    
    
    t = title(sprintf('%s', condnames{icond}));
    f = gcf;
    f = fx_sqbeautify(f, psz, fsz, 1, 0.5);
    
    print([figsdir 'splits_perf_' condnames{icond}],'-vector',figf);
    
end

fprintf('\n**\n');
for icond = 1:2
    thr = perf(bpos, icond);
    nth = perf(bneg, icond);

    [p, h, stats] = ranksum(thr,nth);
    fprintf('performance %s: difference thr-nth: mean = %.2f, median = %.2f\n', condnames{icond}, mean(thr) - mean(nth), median(thr) - median(nth));
    fprintf('performance %s: difference thr-nth: Mann-Whitney U/rank sum test for two samples: Zval = %.2f, ES = %.2f p = %.3f\n', condnames{icond}, stats.zval, stats.zval/sqrt(size(perf,1)),  p);
    
end

%% CHOICE DIRECTION: target-dir (in all cond x facts):

% for each dataset recompute vopt et copt des participants:
vopt_subj = [];
copt_subj = [];

for iset = 1:size(expes,2)
    
    subjlist = unique(choicestbl.subj(choicestbl.expe == iset));
    nsubj    = numel(subjlist); 
    facts    = 1:4;
    nfact    = numel(facts);
    ncond    = 2;
    
    vopt = nan(nsubj, 20, ncond, nfact);
    copt = nan(nsubj, 20, ncond, nfact);
    
    for isubj = 1:nsubj
        for icond = 1:ncond
            for ifact = 1:nfact
                                
                if ifact < 4
                    ifilt = choicestbl.expe == iset & choicestbl.subj == subjlist(isubj) & choicestbl.condition == icond & choicestbl.symmetry == ifact;
                elseif ifact == 4
                    ifilt = choicestbl.expe == iset & choicestbl.subj == subjlist(isubj) & choicestbl.condition == icond;
                end
                
                choices = choicestbl.choices(ifilt);
                choices = cellfun(@(x) x(~isnan(x)), choices, 'UniformOutput', false); % remove trailing nans
                
                targetc = 2*choicestbl.targetcolor(ifilt)-3;
                targetc = mat2cell(targetc, repelem(1, size(targetc,1)), 1);
                
                x       = -1:0.001:+1;
                perr    = 1/3;
                cfac    = fzero(@(b)trapz(x(x > 0),1./(1+exp(x(x > 0)*b))/trapz(x,1./(1+exp(x*b))))-perr,[0,10]);
                
                cs = choicestbl.colorseen(ifilt);
                cs = cellfun(@(x,b) x(:,1:size(b,2))*cfac, cs, choices, 'UniformOutput', false);  % multiply by cfac only the relevant values
                cs = cellfun(@(x) cumsum(x, 2, 'omitnan'), cs, 'UniformOutput', false);           % accumulate the colour values
                
                [~, opt] = cellfun(@(x,t) max(x.*t, [], 1), cs, targetc, 'UniformOutput', false); % get index of option closest to target
                temp = cellfun(@(a,b)   a == b, choices, opt, 'UniformOutput', false);            % compare choices to copt choices
                temp = cellfun(@(x) [x nan(1, 20-size(x,2))], temp, 'UniformOutput', false);      % add trailing nans
                temp = cell2mat(temp);
                vopt(isubj, :, icond, ifact) = nanmean(temp, 1); % average across subj's sequences
                
                
                [~, opt] = cellfun(@(x) min(abs(x), [], 1), cs, 'UniformOutput', false); % get index of the option with smallest acc value
                temp = cellfun(@(a,b) a == b, choices, opt, 'UniformOutput', false);     % compare choices to copt choices
                temp = cellfun(@(x) [x nan(1, 20-size(x,2))], temp, 'UniformOutput', false);
                temp = cell2mat(temp);
                copt(isubj, :, icond, ifact) = nanmean(temp, 1); % average across subj's sequences
                
            end%fact
        end%cond
    end%subj
    
    vopt_subj(end+1:end+nsubj,:,:,:) = vopt;
    copt_subj(end+1:end+nsubj,:,:,:) = copt;
    
end

vopt_subj(:,1,:,:) = nan;
copt_subj(:,1,:,:) = nan;

% plot target directed choices
f = figure('Name', 'vopt');
fx_drawcurve(vopt_subj(bpos,:,1,4), col(1,:), 'ci');
fx_drawcurve(vopt_subj(bneg,:,1,4), 0.5*(col(1,:)+1), 'ci');

set(gca, 'YLim', [0 1], 'YTick', [0:0.2:1], 'XLim', [0 20], 'XTick', [1 8:4:20]);
xlabel('choice position in game');
ylabel('fraction of choices');

f = fx_sqbeautify(f, psz, fsz);
t = title(sprintf('target-congruent\nin DRAW'));
t.HorizontalAlignment = 'left';
yl = ylim;
xl = xlim;
t.Position = [xl(1) yl(2) 0];

print(sprintf('%ssplits_choicedir_target', figsdir),'-vector',figf);

%%% plot information directed choices
f = figure('Name', 'copt');
fx_drawcurve(copt_subj(bpos,:,2,4), col(icond,:), 'ci');
fx_drawcurve(copt_subj(bneg,:,2,4), 0.5*(col(icond,:)+1), 'ci');
set(gca, 'YLim', [0 1], 'YTick', [0:0.2:1], 'XLim', [0 20], 'XTick', [1 8:4:20]);
xlabel('choice position in game');
ylabel('fraction of choices');

f = fx_sqbeautify(f, psz, fsz);
t = title(sprintf('uncertainty-congruent\nin GUESS'));
t.HorizontalAlignment = 'left';
yl = ylim;
xl = xlim;
t.Position = [xl(1) yl(2) 0];

print(sprintf('%ssplits_choicedir_info', figsdir),'-vector',figf);


%% fraction resample (average over all types of sequences)
spstay = nan(totalnsubj, 20, ncond);
nn     = 0;
liim   = 10;
itit   = {'initial sampling phase', 'no initial sampling phase'};
inams  = {'ini+', 'ini-'};
% compute frac rep
for iexp  = 1:3
    subjl = unique(choicestbl.subj(choicestbl.expe == iexp))';  
    for isubj = 1:numel(subjl)
        nn = nn+1;
        
        for icond = 1:ncond
            
            % filter mod and subj choices for this participant and this condition:
            ifilt    = choicestbl.expe == iexp & choicestbl.subj == subjl(isubj) & choicestbl.condition == icond;
            schoices = cellfun(@(x) x(~isnan(x)), choicestbl.choices(ifilt), 'UniformOutput', false); % cle
            nseq     = size(schoices, 1);
            sprevch  = nan(nseq,20);
            srepeat  = nan(nseq,20);

            % subj: for each sequence, for each choice, count nprev (n option was seen before) and nrepeats (n repeats on next choice)
            for iseq = 1:nseq
                nsmp = size(schoices{iseq},2);
                for ismp  = 1:nsmp-1
                    sprevch(iseq, ismp) = sum( schoices{iseq}(1:ismp) == schoices{iseq}(ismp) ); % count nb of times seen, including this one
                    srepeat(iseq, ismp) = schoices{iseq}(ismp) == schoices{iseq}(ismp+1);        
                end
            end
            
            % subj: for each nb of times seen up to here, get average p(stay)
            for idx = 1:max(max(sprevch))
                ich = find(sprevch == idx);
                spstay(nn, idx, icond) = nanmean(srepeat(ich));
            end
            
        end% for isubj
    end
end

% plot
for ini = 1:2
    hdl = figure('Color','white');
    hold on
    
    for icond = 1:2
        
        x_patch = [1:liim, fliplr(1:liim)];
        x       = squeeze(spstay(inistatus(:,ini),:,icond));
        xphat   = bsxfun(@minus,x,nanmean(x,2)); % remove each participants' average in each cond (nrepeats)
        avg     = squeeze(nanmean(x(:,1:liim),1));
        err     = (squeeze(nanstd( xphat(:,1:liim))) / sqrt(size(x,1)) ) * 1.96;
        y_patch = [(avg + err), flip((avg - err))];        
        switch ini
            case 1
                fill(x_patch, y_patch , 1, 'facecolor', col(icond,:), 'edgecolor','none', 'facealpha', 0.3); % patch ErrBar
                plot(1:liim, squeeze(nanmean(x(:,1:liim))), '-', 'Color', col(icond,:), 'LineWidth', 1);   % plot mean line
            case 2
                fill(x_patch, y_patch , 1, 'facecolor', 0.5*(col(icond,:)+1), 'edgecolor','none', 'facealpha', 0.3); % patch ErrBar
                plot(1:liim, squeeze(nanmean(x(:,1:liim))), ':', 'Color', 0.5*(col(icond,:)+1), 'LineWidth', 0.75);   % plot mean line
        end
    end
     
    set(gca,'XLim', [0.5 liim], 'XTick', [1 2:2:liim], 'YLim', [0.5 1], 'YTick', [0.5:0.1:1], 'FontSize', 7.2);
    xlabel('times seen before');
    ylabel(sprintf('fraction repeat\non the next choice'));
    
    f = gcf;
    f = fx_sqbeautify(f, psz, fsz);
    switch ini
        case 1
            t=title(sprintf('without initial\nsampling phase'));
        case 2
            t=title(sprintf('with initial\nsampling phase'));
    end
    t.HorizontalAlignment = 'left';
    yl = ylim;
    xl = xlim;
    t.Position = [xl(1) yl(2) 0];
    print(sprintf('%ssplits_fracresample_%s', figsdir, inams{ini}),'-vector','-dsvg');

end



% plot
for icond = 1:2
    hdl = figure('Color','white');
    hold on
    
    for ini = 1:2
        
        x_patch = [1:liim, fliplr(1:liim)];
        x       = squeeze(spstay(inistatus(:,ini),:,icond));
        xphat   = bsxfun(@minus,x,nanmean(x,2)); % remove each participants' average in each cond (nrepeats)
        avg     = squeeze(nanmean(x(:,1:liim),1));
        err     = (squeeze(nanstd( xphat(:,1:liim))) / sqrt(size(x,1)) ) * 1.96;
        y_patch = [(avg + err), flip((avg - err))];        
        switch ini
            case 1 % ini+
                fill(x_patch, y_patch , 1, 'facecolor', col(icond,:), 'edgecolor','none', 'facealpha', 0.3); % patch ErrBar
                plot(1:liim, squeeze(nanmean(x(:,1:liim))), '-', 'Color', col(icond,:), 'LineWidth', 1);   % plot mean line
            case 2 %ini-
                fill(x_patch, y_patch , 1, 'facecolor', 0.5*(col(icond,:)+1), 'edgecolor','none', 'facealpha', 0.3); % patch ErrBar
                plot(1:liim, squeeze(nanmean(x(:,1:liim))), ':', 'Color', 0.5*(col(icond,:)+1), 'LineWidth', 0.75);   % plot mean line
        end
    end
     
    set(gca,'XLim', [0.5 liim], 'XTick', [1 2:2:liim], 'YLim', [0.5 1], 'YTick', [0.5:0.1:1], 'FontSize', 7.2);
    xlabel('times seen before');
    ylabel(sprintf('fraction repeat\non the next choice'));
    
    f = gcf;
    f = fx_sqbeautify(f, psz, fsz);
    t = title(sprintf('%s', condnames{icond}));
    t.HorizontalAlignment = 'left';
    yl = ylim;
    xl = xlim;
    t.Position = [xl(1) yl(2) 0];
    print(sprintf('%ssplits_fracresamplepercond_%s', figsdir, condnames{icond}),'-vector','-dsvg');

end


%% choice similarity matrices:
nsmp    = max(unique(choicestbl.nsamples));        % max length of sequences = 20
csm     = nan(nsmp, nsmp, ncond, 3 , totalnsubj);  % 20 x 20 x 2|3 x 1ifact x 30ish
nn      = 0;

for iexp = 1:3
    subjl = unique(choicestbl.subj(choicestbl.expe == iexp))';
    for isubj = 1:numel(subjl)
        nn    = nn+1;
        subj  = subjl(isubj);
        
        % for each condition: (1)subset, (2)compute similarity of each choice within condition, (3) average per condition:
        for icond = 1:ncond
            for ifact = 1:3
                
                % (1) subset
                condsqc           = choicestbl(choicestbl.expe == iexp & choicestbl.subj == subj & choicestbl.condition == icond & choicestbl.symmetry == ifact, :);
                nsqcpercond       = height(condsqc);                                                  % nb of sequence per syncondition varies between syncond
                fidx              = find(strcmp(condsqc.Properties.VariableNames, 'choices') == 1);   
                condchoices       = cell2mat(table2cell(condsqc(:, fidx)));                           % get all the responses for all the sequences = nsqcpersyncond x 20
                condsimilaritymat = nan(nsmp, nsmp, nsqcpercond); % 20 x 20 similarity matrix for each sequence of the condition => 20 x 20 x nsqcpercond
                
                % (2) for each sequence, compute the 20 x 20 similarity matrix to all other choices:
                for iseq = 1:nsqcpercond
                    
                    % for each choice, see how similar it is to other choices *in its sequence*:
                    for ismp = 1:condsqc.nsamples(iseq)
                        sim = condchoices(iseq,ismp) - condchoices(iseq,:);
                        
                        corr_similaritymat = nan(size(sim)); % manually check for presence of nans:
                        for isim = 1:numel(sim)
                            if isnan(sim(isim)); corr_similaritymat(isim) = NaN; % if nan stays nan
                            elseif sim(isim) == 0; corr_similaritymat(isim) = 1; % if 0 = they were the equal
                            elseif sim(isim) == 1 || sim(isim) == -1; corr_similaritymat(isim) = 0; % if not zero (1 or -1), were not equal
                            end % if nan, 0, 1
                        end%for each isim value check for nans
                        
                        % for each smp, returns 20 values of its simil to all 20smp, so it's 20x20 for each sequence of this condition => 20 x 20 x nsqcpercond
                        condsimilaritymat(ismp, :, iseq) = corr_similaritymat;
                    end% for each sample, get a line of 20 similarity values
                end% for each sq in condition get 20x20 similarity matrix
                
                % (3) compute average similarity per synthetic condition:
                csm(:,:,icond,ifact,nn) = nanmean(condsimilaritymat,3); % (b) average similarity at each smp position *across sequences in condition* = 20x20 x ncond x nsubj
                
            end% for each symmetry condition (fact)
        end% for each synthetic condition
    end% for each subj
end% for each expe

csm   = squeeze(nanmean(csm,4)); % averaging over symmetries

% plots
lim   = [0.2 0.8];
inams = {'ini+', 'ini-'};
matsz = 1.5;
for ini = 1:2
    for icond = 1:ncond
        f = figure('Name', num2str(icond));
        
        x = squeeze(csm(:,:,icond,inistatus(:,ini))); % subset this condition, and ini/no ini participants
        x = squeeze(nanmean(x,3)); % average over participants
        imagesc(tril(x,0));
        
        set(gca, 'YTick', [1 8:4:20], 'XTick', [1 8:4:20], 'FontSize', 7.2);
        
        cols = colormap('parula');
        cols = cat(1,[1,1,1],cols);
        colormap(cols);
        c              = colorbar;
        c.Position     = [0.8 0.25 0.04 0.5];
        c.Color        = [0 0 0 0];
        c.FontSize     = 7.2;
        c.Label.String = 'mean similarity';
        c.Ticks        = [0:.2:lim(2)];
        c.TickLabels   = {'0', '0.2', '0.4', '0.6', '0.8'};
        c.TickLength   = 0.02/max(1,1);
        set(gca,'CLim',lim);
        
        xlabel('choice position');
        ylabel('choice position');
        
        f = fx_sqbeautify(f, matsz, fsz);
        
        t = title(sprintf('%s %s',condnames{icond}, inams{ini}));
        t.HorizontalAlignment = 'left';
        yl = ylim;
        xl = xlim;
        t.Position = [xl(1) yl(1) 0];
        
        print(sprintf('%ssplits_csm_%s_%s', figsdir, condnames{icond}, inams{ini}),'-vector','-dsvg');
        
    end%for each cond
end% for each ini status

%% betav: 

bv{1} = betav(bpos,1);
bv{2} = betav(bneg,1);
bv{3} = betav(bpos,1)*1000;
bv{4} = betav(bneg,1)*1000;

plot_violins(bv,[],[col(1,:); col(1,:); col(1,:); col(1,:)], 1,9,'avg_ci', false);
hold on
plot(0:6, repelem(0, 7), ':', 'Color', gray, 'LineWidth', 0.5); % plot mean line

set(gca, 'YLim', [-0.6 1.6], 'YTick', -0.5:0.5:1.5, 'XTickLabel', {'ini+', 'ini-'});
ylabel('parameter estimates');

f = gcf;
f = fx_sqbeautify(f, psz, fsz);
t = title(sprintf('sensitivity\nto the target'));
t.HorizontalAlignment = 'left';
yl = ylim;
xl = xlim;
t.Position = [xl(1) yl(2) 0];

print(sprintf('%ssplits_parms_betav', figsdir),'-vector',figf);


%% betar:

br{1} = betar(bpos,1);
br{2} = betar(bneg,1);
br{3} = betar(bpos,2);
br{4} = betar(bneg,2);

plot_violins(br,[],[col(1,:); col(1,:); col(2,:); col(2,:)], 1,9,'avg_ci', false);
hold on
plot(0:6, repelem(0, 7), ':', 'Color', gray, 'LineWidth', 0.5); % plot mean line

set(gca, 'YLim', [-2.2 2.7], 'YTick', -2 :1:2.5, 'XTickLabel', {'ini+', 'ini-', 'ini+', 'ini-'});
ylabel('parameter estimates');

f = gcf;
f = fx_sqbeautify(f, psz, fsz);
t = title(sprintf('repetition\nbias'));
t.HorizontalAlignment = 'left';
yl = ylim;
xl = xlim;
t.Position = [xl(1) yl(2) 0];

print(sprintf('%ssplits_parms_betar', figsdir),'-vector',figf);


%% betar correlation:
figure
for ini = 1:2
    switch ini
        case 1
            % darker = ini
            x = betar(bpos,2); % X = guess
            y = betar(bpos,1); % Y = draw
            scatter(x, y, 10, 'MarkerFaceColor', [0.3 0.3 0.3], 'MarkerFaceAlpha',0.3, 'MarkerEdgeAlpha',0);
            hold on
        case 2
            x = betar(bneg,2); % X = guess
            y = betar(bneg,1); % Y = draw
            scatter(x, y, 10, 'MarkerFaceColor', [0.7 0.7 0.7], 'MarkerFaceAlpha',0.3, 'MarkerEdgeAlpha',0);
    end

    [rh, pv, low, high] = corrcoef(x, y);
    r(ini) = rh(1,2);
    p(ini) = pv(1,2);
    lo(ini) = low(1,2);
    hi(ini) = high(1,2);   
end
fprintf('betar correlation in ini: rh = %.2f [%.2f:%.2f]\n', r(1), lo(1), hi(1));
fprintf('betar correlation in no ini: rh = %.2f [%.2f:%.2f]\n', r(2), lo(2), hi(2));


llim = [-4 +4];
set(gca, 'YLim', llim, 'XLim', llim);
plot(llim, llim, ':', 'Color', gray, 'LineWidth', 0.5, 'HandleVisibility', 'off')
plot([0 0], llim, '-', 'Color', gray, 'LineWidth', 0.5, 'HandleVisibility', 'off')
plot(llim, [0 0], '-', 'Color', gray, 'LineWidth', 0.5, 'HandleVisibility', 'off')


xlabel('repetition bias GUESS');
ylabel('repetition bias DRAW');

xl = xlim;
xr = range(xl);
yl = ylim;
yr = range(yl);
text(xl(1) + 5/100*xr , yl(1) + 15/100*yr , sprintf('ini+ rh = %.2f [%.2f:%.2f]', r(1), lo(1), hi(1)), 'FontSize', 7.2);
text(xl(1) + 5/100*xr , yl(1) + 5/100*yr , sprintf('ini- rh = %.2f [%.2f:%.2f]', r(2), lo(2), hi(2)), 'FontSize', 7.2);

f = gcf;
f = fx_sqbeautify(f, psz, fsz);
t = title(sprintf('repetition bias\ncorrelation'));
t.HorizontalAlignment = 'left';
yl = ylim;
xl = xlim;
t.Position = [xl(1) yl(2) 0];

print(sprintf('%ssplits_parms_betar_corr', figsdir),'-vector',figf);




%% betac:
bc{1} = betac(bpos,1).*-1;
bc{2} = betac(bneg,1).*-1;
bc{3} = betac(bpos,2).*-1;
bc{4} = betac(bneg,2).*-1;

plot_violins(bc,[],[col(1,:); col(1,:); col(2,:); col(2,:)], 1,9,'avg_ci', false);
hold on
plot(0:6, repelem(0, 7), ':', 'Color', gray, 'LineWidth', 0.5); % plot mean line

set(gca, 'YLim', [-0.6 1.6], 'YTick', -0.5:0.5:1.5, 'XTickLabel', {'ini+', 'ini-', 'ini+', 'ini-'});

ylabel('parameter estimates');

f = gcf;
f = fx_sqbeautify(f, psz, fsz);
t = title(sprintf('sensitivity\nto information'));
t.HorizontalAlignment = 'left';
yl = ylim;
xl = xlim;
t.Position = [xl(1) yl(2) 0];

print(sprintf('%ssplits_parms_betac', figsdir),'-vector',figf);



diary off 

