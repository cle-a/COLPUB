%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plots distributions of parameter estimates in third dataset (COLMID)
% and computes single tail statistics
% requires spm12 for Bayesian model comparison
% 
% requires a file of the agregated parameter estimates in ./Dat
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all
clearvars
clc

addpath ./Toolboxes/
addpath ./Toolboxes/draw
addpath ./Toolboxes/spm12

samplename = 'COLMID'; % in COLMID will do single tailed tests


% define drawing parameters
figsdir    = ['Figs/' samplename '/'];
if ~exist(figsdir)
    mkdir(figsdir)
end
figf = '-dsvg';
col      = lines;
col(1,:) = col(4,:); % THIS IS SWAPPED FOR COLMID TO
col(2,:) = col(3,:); % HAVE FIND (YELLOW)
col(3,:) = col(5,:); % IN THE MIDDLE
gray     = [0.6 0.6 0.6];
fsz      = 8;
psz      = 3; 
condnames  = {'DRAW', 'FIND', 'GUESS'};


% set up  diary file
statprt = true;
clc
diaryfile = sprintf('%s%s_statsprintout.txt', figsdir,samplename);
if (exist(diaryfile))
    delete(diaryfile);
end
diary(diaryfile);
diary on
fprintf('------------------------\n%s: STATS ON PARM ESTIMATES\n------------------------\n\n\n', samplename);

% retrieve best-fitting parameters
file = sprintf('./Dat/dat_modelbound_evi_bads_%s_crossval.mat', samplename); % this has the crossval-ed llh and regular parms estimates
load(file);
parmv = {squeeze(betav_fit(:,[1 3 2],4)),... % only keeping ifact = 4
         squeeze(betac_fit(:,[1 3 2],4)),...
         squeeze(betar_fit(:,[1 3 2],4)),...
         squeeze(theta_fit(:,[1 3 2],4)),...
         squeeze(omega_fit(:,[1 3 2],4))};


%% BETAV
pnam = 'betav';
var  = parmv{1};
fprintf('------------------------\n-------- %s ---------\n', pnam);
fprintf('------------------------\n');


% draw
fprintf('DRAW betav mean(sd) = %.2f (%.2f), median(iqr) = %.2f (%.2f)\n', mean(var(:,1)), std(var(:,1)), median(var(:,1)), iqr(var(:,1)));
vardif = var(:,1);
[~, tpval, ~, tstats] = ttest(vardif, 0, 'tail', 'right'); % DRAW > chance = pos = right tail
d                     = mean(vardif) / std(vardif);
ci                    = [mean(vardif) - 1.96*(std(vardif)/sqrt(size(vardif,1))) mean(vardif) + 1.96*(std(vardif)/sqrt(size(vardif,1)))];
fprintf('single tail DRAW > 0: mean: %.2f -- CI [%.2f : %.2f] -- std: %.2f\n', mean(vardif),ci, std(vardif));
fprintf('single tail DRAW > 0: Tpaired(df %d) = %.1f; p = %.3f; d = %.2f\n\n', tstats.df, tstats.tstat, tpval, d);


% find > 0
fprintf('FIND betav mean(sd) = %.2f (%.2f), median(iqr) = %.2f (%.2f)\n', mean(var(:,2)), std(var(:,2)), median(var(:,2)), iqr(var(:,2)));
vardif = var(:,2);
[~, tpval, ~, tstats] = ttest(vardif, 0, 'tail', 'right'); % FIND > chance = pos = right tail
d                     = mean(vardif) / std(vardif);
ci                    = [mean(vardif) - 1.96*(std(vardif)/sqrt(size(vardif,1))) mean(vardif) + 1.96*(std(vardif)/sqrt(size(vardif,1)))];
fprintf('single tail FIND > 0: mean: %.2f -- CI [%.2f : %.2f] -- std: %.2f\n', mean(vardif),ci, std(vardif));
fprintf('single tail FIND > 0: Tpaired(df %d) = %.1f; p = %.3f; d = %.2f\n', tstats.df, tstats.tstat, tpval, d);
fprintf('....................\n\n');


% comparison draw > find
vardif = var(:,1) - var(:,2);
[~, tpval, ~, tstats] = ttest(vardif, 0, 'tail', 'right'); % DRAW - FIND = postitive = right tail
d                     = mean(vardif) / std(vardif);
ci                    = [mean(vardif) - 1.96*(std(vardif)/sqrt(size(vardif,1))) mean(vardif) + 1.96*(std(vardif)/sqrt(size(vardif,1)))];
fprintf('single tail DRAW > FIND: mean: %.2f -- CI [%.2f : %.2f] -- std: %.2f\n', mean(vardif),ci, std(vardif));
fprintf('single tail DRAW > FIND: Tpaired(df %d) = %.1f; p = %.3f; d = %.2f\n\n', tstats.df, tstats.tstat, tpval, d);


%% BETAC
pnam = 'betac';
var  = parmv{2};
var  = var*-1;   % reverse betac into positive for sensitivity to uncertainty
fprintf('------------------------\n-------- %s ---------\n', pnam);
fprintf('------------------------\n');

% draw > 0
fprintf('DRAW betac mean(sd) = %.2f (%.2f), median(iqr) = %.2f (%.2f)\n', mean(var(:,1)), std(var(:,1)), median(var(:,1)), iqr(var(:,1)));
vardif = var(:,1);
[~, tpval, ~, tstats] = ttest(vardif, 0, 'tail', 'right'); % DRAW > chance = pos = right tail
d                     = mean(vardif) / std(vardif);
ci                    = [mean(vardif) - 1.96*(std(vardif)/sqrt(size(vardif,1))) mean(vardif) + 1.96*(std(vardif)/sqrt(size(vardif,1)))];
fprintf('single tail DRAW > 0: mean: %.2f -- CI [%.2f : %.2f] -- std: %.2f\n', mean(vardif),ci, std(vardif));
fprintf('single tail DRAW > 0: Tpaired(df %d) = %.1f; p = %.3f; d = %.2f\n\n', tstats.df, tstats.tstat, tpval, d);

% find > 0
fprintf('FIND betac mean(sd) = %.2f (%.2f), median(iqr) = %.2f (%.2f)\n', mean(var(:,2)), std(var(:,2)), median(var(:,2)), iqr(var(:,2)));
vardif = var(:,2);
[~, tpval, ~, tstats] = ttest(vardif, 0, 'tail', 'right'); % FIND > chance = pos = right tail
d                     = mean(vardif) / std(vardif);
ci                    = [mean(vardif) - 1.96*(std(vardif)/sqrt(size(vardif,1))) mean(vardif) + 1.96*(std(vardif)/sqrt(size(vardif,1)))];
fprintf('single tail FIND > 0: mean: %.2f -- CI [%.2f : %.2f] -- std: %.2f\n', mean(vardif),ci, std(vardif));
fprintf('single tail FIND > 0: Tpaired(df %d) = %.1f; p = %.3f; d = %.2f\n\n', tstats.df, tstats.tstat, tpval, d);

% guess > 0
fprintf('GUESS betac mean(sd) = %.2f (%.2f), median(iqr) = %.2f (%.2f)\n', mean(var(:,end)), std(var(:,end)), median(var(:,end)), iqr(var(:,end)));
vardif = var(:,end);
[~, tpval, ~, tstats] = ttest(vardif, 0, 'tail', 'right'); % FIND > chance = pos = right tail
d                     = mean(vardif) / std(vardif);
ci                    = [mean(vardif) - 1.96*(std(vardif)/sqrt(size(vardif,1))) mean(vardif) + 1.96*(std(vardif)/sqrt(size(vardif,1)))];
fprintf('single tail FIND > 0: mean: %.2f -- CI [%.2f : %.2f] -- std: %.2f\n', mean(vardif),ci, std(vardif));
fprintf('single tail FIND > 0: Tpaired(df %d) = %.1f; p = %.3f; d = %.2f\n', tstats.df, tstats.tstat, tpval, d);
fprintf('....................\n\n');

% comparison draw < guess
vardif = var(:,1) - var(:,end);
[~, tpval, ~, tstats] = ttest(vardif, 0, 'tail', 'left'); % draw - guess = neg = left tail
d                     = mean(vardif) / std(vardif);
ci                    = [mean(vardif) - 1.96*(std(vardif)/sqrt(size(vardif,1))) mean(vardif) + 1.96*(std(vardif)/sqrt(size(vardif,1)))];
fprintf('single tail DRAW < GUESS: mean: %.2f -- CI [%.2f : %.2f] -- std: %.2f\n', mean(vardif),ci, std(vardif));
fprintf('single tail DRAW < GUESS: Tpaired(df %d) = %.1f; p = %.3f; d = %.2f\n\n', tstats.df, tstats.tstat, tpval, d);

% comparison draw < find
vardif = var(:,1) - var(:,2);
[~, tpval, ~, tstats] = ttest(vardif, 0, 'tail', 'left'); % DRAW - FIND = neg = left tail
d                     = mean(vardif) / std(vardif);
ci                    = [mean(vardif) - 1.96*(std(vardif)/sqrt(size(vardif,1))) mean(vardif) + 1.96*(std(vardif)/sqrt(size(vardif,1)))];
fprintf('single tail DRAW < FIND: mean: %.2f -- CI [%.2f : %.2f] -- std: %.2f\n', mean(vardif),ci, std(vardif));
fprintf('single tail DRAW < FIND: Tpaired(df %d) = %.1f; p = %.3f; d = %.2f\n\n', tstats.df, tstats.tstat, tpval, d);

% comparison find < guess
vardif = var(:,2) - var(:,3);
[~, tpval, ~, tstats] = ttest(vardif, 0, 'tail', 'left'); % find - guess = neg = left tail
d                     = mean(vardif) / std(vardif);
ci                    = [mean(vardif) - 1.96*(std(vardif)/sqrt(size(vardif,1))) mean(vardif) + 1.96*(std(vardif)/sqrt(size(vardif,1)))];
fprintf('single tail FIND < GUESS: mean: %.2f -- CI [%.2f : %.2f] -- std: %.2f\n', mean(vardif),ci, std(vardif));
fprintf('single tail FIND < GUESS: Tpaired(df %d) = %.1f; p = %.3f; d = %.2f\n\n', tstats.df, tstats.tstat, tpval, d);

%% BETAR 
pnam = 'betar';
var  = parmv{3};
fprintf('------------------------\n-------- %s COMPARISONS ARE TWO-TAILED ---------\n', pnam);
fprintf('------------------------\n');

% draw > 0 single tail
fprintf('DRAW betar mean(sd) = %.2f (%.2f), median(iqr) = %.2f (%.2f)\n', mean(var(:,1)), std(var(:,1)), median(var(:,1)), iqr(var(:,1)));
vardif = var(:,1);
[~, tpval, ~, tstats] = ttest(vardif, 0, 'tail', 'right'); % DRAW > chance = pos = right tail
d                     = mean(vardif) / std(vardif);
ci                    = [mean(vardif) - 1.96*(std(vardif)/sqrt(size(vardif,1))) mean(vardif) + 1.96*(std(vardif)/sqrt(size(vardif,1)))];
fprintf('single tail DRAW > 0: mean: %.2f -- CI [%.2f : %.2f] -- std: %.2f\n', mean(vardif),ci, std(vardif));
fprintf('single tail DRAW > 0: Tpaired(df %d) = %.1f; p = %.3f; d = %.2f\n\n', tstats.df, tstats.tstat, tpval, d);
 
% find > 0 single tail
fprintf('FIND betar mean(sd) = %.2f (%.2f), median(iqr) = %.2f (%.2f)\n', mean(var(:,2)), std(var(:,2)), median(var(:,2)), iqr(var(:,2)));
vardif = var(:,2);
[~, tpval, ~, tstats] = ttest(vardif, 0, 'tail', 'right'); % FIND > chance = pos = right tail
d                     = mean(vardif) / std(vardif);
ci                    = [mean(vardif) - 1.96*(std(vardif)/sqrt(size(vardif,1))) mean(vardif) + 1.96*(std(vardif)/sqrt(size(vardif,1)))];
fprintf('single tail FIND > 0: mean: %.2f -- CI [%.2f : %.2f] -- std: %.2f\n', mean(vardif),ci, std(vardif));
fprintf('single tail FIND > 0: Tpaired(df %d) = %.1f; p = %.3f; d = %.2f\n\n', tstats.df, tstats.tstat, tpval, d);

% guess > 0 single tail
fprintf('GUESS betar mean(sd) = %.2f (%.2f), median(iqr) = %.2f (%.2f)\n', mean(var(:,end)), std(var(:,end)), median(var(:,end)), iqr(var(:,end)));
vardif = var(:,end);
[~, tpval, ~, tstats] = ttest(vardif, 0, 'tail', 'right'); % FIND > chance = pos = right tail
d                     = mean(vardif) / std(vardif);
ci                    = [mean(vardif) - 1.96*(std(vardif)/sqrt(size(vardif,1))) mean(vardif) + 1.96*(std(vardif)/sqrt(size(vardif,1)))];
fprintf('single tail FIND > 0: mean: %.2f -- CI [%.2f : %.2f] -- std: %.2f\n', mean(vardif),ci, std(vardif));
fprintf('single tail FIND > 0: Tpaired(df %d) = %.1f; p = %.3f; d = %.2f\n', tstats.df, tstats.tstat, tpval, d);
fprintf('....................\n\n');


% comparison draw < guess two-tailed (was not different in prev datasets)
vardif = var(:,1) - var(:,end);
[~, tpval, ~, tstats] = ttest(vardif, 0); % draw - guess = neg = left tail
d                     = mean(vardif) / std(vardif);
ci                    = [mean(vardif) - 1.96*(std(vardif)/sqrt(size(vardif,1))) mean(vardif) + 1.96*(std(vardif)/sqrt(size(vardif,1)))];
fprintf('two-tailed DRAW <> GUESS: mean: %.2f -- CI [%.2f : %.2f] -- std: %.2f\n', mean(vardif),ci, std(vardif));
fprintf('two-tailed DRAW <> GUESS: Tpaired(df %d) = %.1f; p = %.3f; d = %.2f\n\n', tstats.df, tstats.tstat, tpval, d);

% comparison draw < find two-tailed
vardif = var(:,1) - var(:,2);
[~, tpval, ~, tstats] = ttest(vardif, 0); % DRAW - FIND = neg = left tail
d                     = mean(vardif) / std(vardif);
ci                    = [mean(vardif) - 1.96*(std(vardif)/sqrt(size(vardif,1))) mean(vardif) + 1.96*(std(vardif)/sqrt(size(vardif,1)))];
fprintf('two-tailed DRAW <> FIND: mean: %.2f -- CI [%.2f : %.2f] -- std: %.2f\n', mean(vardif),ci, std(vardif));
fprintf('two-tailed DRAW <> FIND: Tpaired(df %d) = %.1f; p = %.3f; d = %.2f\n\n', tstats.df, tstats.tstat, tpval, d);

% comparison find < guess two-tailed
vardif = var(:,2) - var(:,3);
[~, tpval, ~, tstats] = ttest(vardif, 0); % find - guess = neg = left tail
d                     = mean(vardif) / std(vardif);
ci                    = [mean(vardif) - 1.96*(std(vardif)/sqrt(size(vardif,1))) mean(vardif) + 1.96*(std(vardif)/sqrt(size(vardif,1)))];
fprintf('two-tailed FIND <> GUESS: mean: %.2f -- CI [%.2f : %.2f] -- std: %.2f\n', mean(vardif),ci, std(vardif));
fprintf('two-tailed FIND <> GUESS: Tpaired(df %d) = %.1f; p = %.3f; d = %.2f\n\n', tstats.df, tstats.tstat, tpval, d);

%% THETA
pnam = 'theta';
var  = parmv{4};         
var  = 1./(1+exp(-var)); % transforming theta into 0.5-1
fprintf('------------------------\n-------- %s ---------\n', pnam);
fprintf('------------------------\n');

% draw < guess
difvar = var(:,1) - var(:,end);
[wpval, ~, wstats] = signrank(difvar, 0, 'tail', 'left'); % SINGLE TAIL LEFT
if ~isfield(wstats, 'zval')
Z              = nan;
r              = nan;
elseif isfield(wstats, 'zval')
Z              = wstats.zval;
r              = Z/sqrt(size(difvar,1));
end
fprintf('single tail DRAW < GUESS: Wilcoxon: Z = %.3f; p = %.3f; r = %.2f\n\n', Z, wpval, r);

% draw < find
difvar = var(:,1) - var(:,2);
[wpval, ~, wstats] = signrank(difvar, 0, 'tail', 'left'); % SINGLE TAIL LEFT
if ~isfield(wstats, 'zval')
    Z              = nan;
    r              = nan;
elseif isfield(wstats, 'zval')
    Z              = wstats.zval;
    r              = Z/sqrt(size(difvar,1));
end
fprintf('single tail DRAW < FIND: Wilcoxon: Z = %.3f; p = %.3f; r = %.2f\n\n', Z, wpval, r);

% find < guess
difvar = var(:,2) - var(:,3);
[wpval, ~, wstats] = signrank(difvar, 0, 'tail', 'left'); % SINGLE TAIL LEFT
if ~isfield(wstats, 'zval')
    Z              = nan;
    r              = nan;
elseif isfield(wstats, 'zval')
    Z              = wstats.zval;
    r              = Z/sqrt(size(difvar,1));
end
fprintf('single tail FIND < GUESS: Wilcoxon: Z = %.3f; p = %.3f; r = %.2f\n\n', Z, wpval, r);

%% OMEGA
pnam = 'omega';
var  = parmv{5};
fprintf('------------------------\n-------- %s COMPARISONS ARE TWO-TAILED ---------\n', pnam);
fprintf('------------------------\n');

% draw < guess
difvar = var(:,1) - var(:,end);
[wpval, ~, wstats] = signrank(difvar, 0);
if ~isfield(wstats, 'zval')
Z              = nan;
r              = nan;
elseif isfield(wstats, 'zval')
Z              = wstats.zval;
r              = Z/sqrt(size(difvar,1));
end
fprintf('two-tailed DRAW <> GUESS: Wilcoxon: Z = %.3f; p = %.3f; r = %.2f\n\n', Z, wpval, r);

% draw < find
difvar = var(:,1) - var(:,2);
[wpval, ~, wstats] = signrank(difvar, 0);
if ~isfield(wstats, 'zval')
    Z              = nan;
    r              = nan;
elseif isfield(wstats, 'zval')
    Z              = wstats.zval;
    r              = Z/sqrt(size(difvar,1));
end
fprintf('two-tailed DRAW <> FIND: Wilcoxon: Z = %.3f; p = %.3f; r = %.2f\n\n', Z, wpval, r);

% find < guess
difvar = var(:,2) - var(:,3);
[wpval, ~, wstats] = signrank(difvar, 0);
if ~isfield(wstats, 'zval')
    Z              = nan;
    r              = nan;
elseif isfield(wstats, 'zval')
    Z              = wstats.zval;
    r              = Z/sqrt(size(difvar,1));
end
fprintf('two-tailed FIND <> GUESS: Wilcoxon: Z = %.3f; p = %.3f; r = %.2f\n\n', Z, wpval, r);

%% BMS stats

% with = true;
nit  = 1e6;
binit = 1;
ifact = 4;

inipos = squeeze(llh1_bads_loo(:,:,ifact));
inineg = squeeze(llh0_bads_loo(:,:,ifact));

% swapping order to have FIND in the middle
inipos = inipos(:,[1 3 2]);
inineg = inineg(:,[1 3 2]);

% run Bayesian model selection in each condition
ncond    = size(inineg,2);
pavg_mod = nan(ncond, 2);  % prevalence
pstd_mod = nan(ncond, 2);  % prevalence s.d.
pexc_mod = nan(ncond, 2);  % exceedance probabilities
alphas   = nan(ncond, 2);  % alphas of the Drch distributions

for icond = 1:ncond
    
    lme = [inipos(:,icond) inineg(:,icond)]; % 1 = POS, 2 = NEG
    
    % run BMS
    [alpha,~,pexc]    = spm_BMS(lme);
    [pavg,pvar]       = drchstat(alpha);
    pavg_mod(icond,:) = pavg;
    pstd_mod(icond,:) = sqrt(pvar(1,:));
    pexc_mod(icond,:) = pexc;
    alphas(icond,:)   = alpha;

    % drawing from dirichlet by hand
    pdraws            = drchrnd(alpha,nit);
    pexc_check(icond) = mean(pdraws(:,1) > pdraws(:,2)); % this should be ==  pexc_mod(:,1)
    
end


fprintf('\n\n\nmodel comparison\n----------\n');

for icond = 1:ncond
    fprintf('pexc of ini positive in %s  = %.4f (mean prev. = %.2f, sd = %.2f)\n',...
        condnames{icond}, pexc_mod(icond,1), pavg_mod(icond,1)*100, pstd_mod(icond,1)*100);
    fprintf('pexc of ini negative in %s  = %.4f (mean prev. = %.2f, sd = %.2f)\n',...
        condnames{icond}, pexc_mod(icond,2), pavg_mod(icond,2)*100, pstd_mod(icond,2)*100);
end

for icond = 1:ncond
    oconds = setdiff(icond:ncond, icond);
    for ocond = oconds
        thesedraws = drchrnd(alphas([icond ocond],1)', nit);   % take only model 1, ini+, compare across conds
        pexc_comp   = mean(thesedraws(:,2) > thesedraws(:,1)); % ocond > icond
        fprintf('drch draws: pexc of INI POSITIVE model in %s > %s: %.03f\n',condnames{ocond}, condnames{icond}, pexc_comp);
    end
end


%% diary off

diary off
