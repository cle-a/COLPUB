%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plots the behavioural indices for each sample of the paper
% fetches data in Dat/dat_choicestbl_xxx.mat
%                 where xxx is the samplename
% saves the plots in the Figs directory
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clearvars;
addpath './Toolboxes'
addpath './Toolboxes/draw'

samplename = 'COLMID';
figsdir    = sprintf('./Figs/%s/', samplename);
if ~exist(figsdir)
    mkdir(figsdir)
end

file = sprintf('./Dat/dat_choicestbl_%s.mat', samplename);
load(file);
choicestbl.expe = repelem(4, size(choicestbl,1), 1);
choicestbl      = choicestbl(choicestbl.excludepart == 0,:);
subjlist        = (unique(choicestbl.subj));
nsubj           = numel(unique(choicestbl.subj));
ncond           = numel(unique(choicestbl.condition));

condnames  = {'DRAW', 'GUESS'};
exlist     = unique(choicestbl.expe);
expes      = {samplename};
expename   = samplename;



figf = '-dsvg';
col      = lines;
col(1,:) = col(4,:);
col(2,:) = col(5,:);
col(3,:) = col(3,:);
fsz      = 8;
psz      = 2.2;

facts = 4; % ifact = 4 is fitted over all symmetries


if strcmp(expename, 'COLMID')
    condnames  = {'DR', 'FN', 'GS'};
    col = col([1 3 2 4 5], :);
end

%% learning curve: DRAW, single target
lcurve = nan(nsubj, 20, ncond);
for icond = 1:ncond
    for isubj = 1:nsubj
        temp = choicestbl(choicestbl.subj == subjlist(isubj) & ...
            choicestbl.condition == icond & choicestbl.symmetry == 3,:); % only in symm == 3 (goodbad)
        temp = cell2mat(temp.choices);
        correct = double(temp == 1);
        correct(isnan(temp)) = nan;
        lcurve(isubj,:, icond) = nanmean(correct,1);
    end
end

if strcmp(expename, 'COLMID')
    lcurve = lcurve(:,:,[1 3]);
else 
    lcurve = lcurve(:,:,1);
end

f = figure('Color','white','Name', 'learning curves');
for icond = 1:size(lcurve,3)
    fx_drawcurve(lcurve(:,:,icond)*100, col(icond,:), 'ci'); % average over lengths
    hold on
end
plot([0 20], [50 50], 'LineStyle', ':', 'Color', [0.5 0.5 0.5], 'LineWidth', 0.5);

set(gca, 'YLim', [0 100], 'YTick', [0:20:100], 'XTick', [1 8:4:20], 'FontName', 'Helvetica', 'FontSize', 7.2);
xlabel('choice position in game');
ylabel('target option choices (%)');
t = title(sprintf('learning curve'));
t.HorizontalAlignment = 'left';

f = gcf;
f = fx_sqbeautify(f, psz, fsz);
yl = ylim;
xl = xlim;
t.Position = [xl(1) yl(2) 0];

print([figsdir expename '_behav_learningcurve'],'-painters', figf);
clearvars temp corr f 

%% last sample: DRAW, single target
fchoice = nan(nsubj, ncond);
for icond = 1:ncond
    for isubj = 1:nsubj
        temp = choicestbl(choicestbl.subj == subjlist(isubj) & ...
                          choicestbl.condition == icond & choicestbl.symmetry == 3,:);
        for it = 1:size(temp,1)
            targ(it) = temp.choices{it}(temp.nsamples(it)) == 1;
        end
        fchoice(isubj,icond) = nanmean(targ);         
    end
end

if strcmp(expename, 'COLMID')
    fchoice = fchoice(:,[1 3]).*100;
else 
    fchoice = fchoice(:,1).*100;
end

fchoice = mat2cell(fchoice, size(fchoice,1), repelem(1,size(fchoice,2)));
f = plot_violins(fchoice,[],col,1,9,'med_iqr');
hold on
plot([0 2.5], [50 50], 'LineStyle', ':', 'Color', [0.5 0.5 0.5], 'LineWidth', 0.5);
set(gca, 'YLim', [0 100], 'YTick', [0:20:100], 'XTickLabels', {'DRAW', 'FIND'});

xlabel('last sample');
ylabel('target option choices (%)');

f = gcf;
f = fx_sqbeautify(f, psz, fsz, 1, 0.6);
print([figsdir  '/' expename '_behav_lastsample'],'-painters',figf);
clearvars temp targ it f

%% final question: GUESS, single target
fquest = nan(nsubj,ncond);
for icond = 1:ncond
    for isubj = 1:nsubj
        temp = choicestbl(choicestbl.subj == subjlist(isubj) & ...
                          choicestbl.condition == icond & choicestbl.symmetry == 3,:);
        fquest(isubj, icond) = nanmean(temp.finalchoicecorr);         
    end
end

if strcmp(expename, 'COLMID')
    fquest = fquest(:,[2 3]).*100;
    cnames = {'FIND', 'GUESS'};
else 
    fquest = fquest(:,2).*100;
    cnames = {'GUESS'};
end

fquest = mat2cell(fquest, size(fquest,1), repelem(1,size(fquest,2)));
f = plot_violins(fquest,[],col(2:3,:),1,9,'med_iqr');
hold on
plot([0 2], [50 50], 'LineStyle', ':', 'Color', [0.5 0.5 0.5], 'LineWidth', 0.5);
set(gca, 'YLim', [0 100], 'YTick', [0:20:100], 'XTickLabels', 'final', 'FontName', 'Helvetica', 'FontSize', 7.2);
xlabel('question');
ylabel('final question accuracy (%)')

f = gcf;
f = fx_sqbeautify(f, psz, fsz, 1, 0.6);
    
print([figsdir  '/' expename '_behav_finalquestion'],'-painters',figf);

clearvars temp f 

%% overall fraction repeat

temp            = cell2mat(choicestbl.choices);
choicestbl.frep = (sum(~isnan(temp),2) - nansum(abs(diff(temp,1,2)),2)) ./ sum(~isnan(temp),2);

nfact = 3;
subjl = unique(choicestbl.subj);
nsubj = numel(subjl);
frep  = nan(nsubj, ncond, nfact+1);
for icond = 1:ncond
    for ifact = 1:nfact
        for isubj = 1:nsubj
            subj = subjl(isubj);
            temp = choicestbl.frep(choicestbl.subj == subj & choicestbl.condition == icond & choicestbl.symmetry == ifact,:);
            frep(isubj,icond,ifact) = mean(temp);
        end
    end
end

% adding the 4th fact = average 
frep(:,:,4) = mean(frep(:,:,1:3),3);

% for COLMID reorder conditions
if strcmp(expename, 'COLMID')
    frep = frep(:,[1 3 2],:); % SWAPPING CONDITIONS IN COLMID
    condnames = {'DRAW', 'FIND', 'GUESS'};
end

% reshape into cells
thifact = 4;
toplot  = squeeze(frep(:,:,thifact));
toplot  = mat2cell(toplot, size(toplot,1), repelem(1,size(toplot,2)));
    
% plot
f = plot_violins(toplot,[],col,1,9,'med_iqr');
hold on
plot([0 2.5], [0.5 0.5], 'LineStyle', ':', 'Color', [0.7 0.7 0.7], 'LineWidth', 0.5);
set(gca, 'YLim', [0 1], 'YTick', [0:0.2:1], 'XTickLabels', condnames, 'FontName', 'Helvetica', 'FontSize', 7.2);
xlabel('condition');
ylabel(sprintf('overall\nfraction repeat'));

f = gcf;
f = fx_sqbeautify(f, psz, fsz, 1, 0.6);
    
print([figsdir  '/' expename '_behav_fracrepeat_average'],'-painters',figf);

clearvars temp f 

%% fraction resample

% pstay x number of times chosen before, even if switches
spstay = nan(nsubj, 20, ncond);
method = 'new';
ifact  = 4;

for isubj = 1:nsubj
    
    for icond = 1:ncond
        
        % filter mod and subj choices for this participant and this condition:
        ifilt    = choicestbl.subj == subjlist(isubj) & choicestbl.condition == icond;
        schoices = cellfun(@(x) x(~isnan(x)), choicestbl.choices(ifilt), 'UniformOutput', false); % cle
        %             mchoices = mch_sim{isubj,icond,ifact};
        
        % then for each choice, count nb of of times this option was seen + whether they repeat on the next choice
        nseq     = size(schoices, 1);
        sprevch  = nan(nseq,20);
        srepeat  = nan(nseq,20);
        
        for iseq = 1:nseq
            
            nsmp = size(schoices{iseq},2);
            
            for ismp  = 1:nsmp-1
                sprevch(iseq, ismp) = sum( schoices{iseq}(1:ismp) == schoices{iseq}(ismp) ); % count nb of times seen, including this one
                srepeat(iseq, ismp) = schoices{iseq}(ismp) == schoices{iseq}(ismp+1);        % do they repeat the next choice
            end
        end
        
        % subj: for each nb of times seen up to here, get average p(stay)
        for idx = 1:max(max(sprevch))
            ich = find(sprevch == idx);
            spstay(isubj, idx, icond) = nanmean(srepeat(ich));
        end
        
    end% each cond
end% each subj
    
f = figure();
liim = 10;
for icond = 1:ncond
    % plot participants' curve by condition
    hold on
    sphat    = bsxfun(@minus,spstay,nanmean(spstay(:,1:liim,:),2));
    x_ax     = 1:liim;
    X_patch  = [x_ax, fliplr(x_ax)];
    yup      = nanmean(spstay(:,1:liim,icond)) + ( nanstd( sphat(:,1:liim,icond)) / sqrt(nsubj)*1.96 ) ;
    ylo      = nanmean(spstay(:,1:liim,icond)) - ( nanstd( sphat(:,1:liim,icond)) / sqrt(nsubj)*1.96 ) ;
    Y_patch  = [yup,flip(ylo)];
    fill(X_patch, Y_patch , 1, 'facecolor', 0.5*(col(icond,:)+1), 'edgecolor','none', 'facealpha', 0.7); % patch ErrBar
    plot(1:liim, nanmean(spstay(:,1:liim,icond)), '-', 'Color', col(icond,:), 'LineWidth', 1);           % plot mean line
end

set(gca,'XLim', [0.5 liim], 'XTick', [1 2:2:liim], 'YLim', [0.5 1], 'YTick', [0.5:0.1:1],'FontName','Helvetica','FontSize',7.2);
xlabel('times seen before (n)')
ylabel(sprintf('fraction repeat\non the next choice'));

f = gcf;
f = fx_sqbeautify(f, psz, fsz);
print([figsdir  '/' expename '_behav_fracresample'],'-painters',figf);

clearvars f ich icond idx iexp ifact ifilt iseq ismp isubj liim method schoices x_ax X_patch Y_patch ylo yup

%% choice matrices

nsmp    = max(unique(choicestbl.nsamples));    % max length of sequences = 20
csm     = nan(nsmp, nsmp, ncond, 3 , nsubj);    % 20 x 20 x 2|3 x 1ifact x 30ish
nn      = 0;

for iexp = 1:numel(unique(choicestbl.expe))
    subjl = unique(choicestbl.subj(choicestbl.expe == exlist(iexp)))';
    for isubj = 1:numel(subjl)
        %     clearvars -except choicestbl fieldtoplot expe plotformat subjs nsubj ncond nfact nsmp csm isubj psz
        nn    = nn+1;
        subj  = subjl(isubj);
        
        % for each condition: (1)subset, (2)compute similarity of each choice within condition, (3) average per condition:
        for icond = 1:ncond
            for ifact = 1:3
                
                % (1) subset
                condsqc           = choicestbl(choicestbl.expe == exlist(iexp) & choicestbl.subj == subj & choicestbl.condition == icond & choicestbl.symmetry == ifact, :);
                nsqcpercond       = height(condsqc);                                                  % nb of sequence per syncondition varies between syncond
                fidx              = find(strcmp(condsqc.Properties.VariableNames, 'choices') == 1); % find the variable to plot
                condchoices       = cell2mat(table2cell(condsqc(:, fidx)));                           % get all the responses for all the sequences = nsqcpersyncond x 20
                condsimilaritymat = nan(nsmp, nsmp, nsqcpercond); % 20 x 20 similarity matrix for each sequence of the condition => 20 x 20 x nsqcpercond
                
                % (2) for each sequence, compute the 20 x 20 similarity matrix to all other choices:
                for iseq = 1:nsqcpercond
                    
                    % for each choice, see how similar it is to other choices *in its sequence*:
                    for ismp = 1:condsqc.nsamples(iseq)
                        sim = condchoices(iseq,ismp) - condchoices(iseq,:);
                        
                        corr_similaritymat = nan(size(sim)); % manually check for presence of nans:
                        for isim = 1:numel(sim)
                            if isnan(sim(isim)); corr_similaritymat(isim) = NaN; % if nan stays nan
                            elseif sim(isim) == 0; corr_similaritymat(isim) = 1; % if 0 = they were the equal
                            elseif sim(isim) == 1 || sim(isim) == -1; corr_similaritymat(isim) = 0; % if not zero (1 or -1), were not equal
                            end % if nan, 0, 1
                        end%for each isim value check for nans
                        
                        % for each smp, returns 20 values of its sim to all 20smp, so it's 20x20 for each sequence of this condition => 20 x 20 x nsqcpercond (16)
                        condsimilaritymat(ismp, :, iseq) = corr_similaritymat;
                    end% for each sample, get a line of 20 similarity values
                end% for each sq in condition get 20x20 similarity matrix
                
                % (3) compute average similarity per synthetic condition:
                %             choicematrix{icond, isubj}        = (condchoices); % (a) matrix of the choices = [nsq x nsmp] x ncond x nsubj
                csm(:,:,icond,ifact,nn) = nanmean(condsimilaritymat,3); % (b) average similarity at each smp position *across sequences in condition* = 20x20 x ncond x nsubj
                
            end% for each symmetry condition (fact)
        end% for each synthetic condition
    end% for each subj
end% for each expe

% averaging over symmetries & participants
csm = squeeze(nanmean(csm,4)); % symmetries
csm = squeeze(nanmean(csm,4)); % participants

% for COLMID swapping the conditions:
if strcmp(expename, 'COLMID')
    csm = csm(:,:,[1 3 2]);
    condnames = {'DRAW', 'FIND', 'GUESS'};
end

% plots
lim = [0.2 0.8];
for icond = 1:ncond 
    f = figure('Name', num2str(icond));
    
    x  = csm(:,:,icond); % subset this condition and symmetry
    im = imagesc(tril(x,0));
    
    set(gca, 'YTick', [5:5:20], 'XTick', [5:5:20], 'FontSize', 7.2);
    
    cols = colormap('parula');
%     cols = cat(1,[1,1,1],cols);
    colormap(cols);
    c              = colorbar;
    c.Position     = [0.64 0.5 0.03 0.35];
    c.Color        = [0 0 0 0];
    c.FontSize     = fsz*0.87;
    c.Label.String = 'mean similarity';
    c.Ticks        = [0:0.2:lim(2)];
    c.TickLength   = 0.03;
    set(gca,'CLim',lim);
    
    xlabel('choice position');
    ylabel('choice position');

    dumb = tril(ones(20,20));
    set(im,'AlphaData', dumb);
    
    t = title(sprintf('%s',condnames{icond}));
    f = fx_sqbeautify(f, psz, fsz);
    t.HorizontalAlignment = 'left';
    yl = ylim;
    xl = xlim;
    t.Position = [xl(1) yl(1) 0];
    print(sprintf('%s/%s_behav_csm_averaged_%s',figsdir, expename, condnames{icond}),'-painters',figf);

end%for each cond

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% choice directions
moddir = 'Dat';

facts = 1:4;
nfact = numel(facts);
sub_vopt = nan(nsubj, 20, ncond, nfact);
sub_copt = nan(nsubj, 20, ncond, nfact);

for isubj = 1:nsubj
    for icond = 1:ncond
        for ifact = 1:nfact

            if ifact < 4
                ifilt = choicestbl.subj == subjlist(isubj) & choicestbl.condition == icond & choicestbl.symmetry == ifact;
            elseif ifact ==4
                ifilt = choicestbl.subj == subjlist(isubj) & choicestbl.condition == icond;
            end
            
            choices = choicestbl.choices(ifilt);
            choices = cellfun(@(x) x(~isnan(x)), choices, 'UniformOutput', false); % remove trailing nans
            targetc = num2cell(2*choicestbl.targetcolor(ifilt)-3); % -1 | +1 target colours
            nsample = num2cell(choicestbl.nsamples(ifilt));

            x       = -1:0.001:+1;
            perr    = 1/3;
            cfac    = fzero(@(b)trapz(x(x > 0),1./(1+exp(x(x > 0)*b))/trapz(x,1./(1+exp(x*b))))-perr,[0,10]);

            cs   = choicestbl.colorseen(ifilt);
            cs   = cellfun(@(x,b) x(:,1:size(b,2))*cfac, cs, choices, 'UniformOutput', false); % remove trailing nans & multiply by cfac
            cs   = cellfun(@(x) cumsum(x, 2, 'omitnan'), cs, 'UniformOutput', false);          % accumulate the colour values

            [~, opt] = cellfun(@(x,t) max(x.*t, [], 1), cs, targetc, 'UniformOutput', false); % get index of option closest to target
            temp = cellfun(@(a,b) a == b, choices, opt, 'UniformOutput', false);              % compare choices to copt choices
            temp = cellfun(@(x) [x nan(1, 20-size(x,2))], temp, 'UniformOutput', false);
            temp = cell2mat(temp);
            sub_vopt(isubj, :, icond, ifact) = nanmean(temp, 1); % average across subj's sequences

            [~, opt] = cellfun(@(x) min(abs(x), [], 1), cs, 'UniformOutput', false); % get index of the option with smallest acc value
            temp = cellfun(@(a,b) a == b, choices, opt, 'UniformOutput', false); % compare choices to copt choices
            temp = cellfun(@(x) [x nan(1, 20-size(x,2))], temp, 'UniformOutput', false);
            temp = cell2mat(temp);
            sub_copt(isubj, :, icond, ifact) = nanmean(temp, 1); % average across subj's sequences

        end%fact
    end%cond
end%subj


tfact = 4;
if strcmp(expename, 'COLMID')
    vopt = squeeze(sub_vopt(:,:,[1 3],tfact));
    copt = squeeze(sub_copt(:,:,[1 3 2],tfact));
else
    vopt = squeeze(sub_vopt(:,:,[1],tfact));
    copt = squeeze(sub_copt(:,:,[1 2],tfact));
end 
vopt(:,1,:)   = nan;
copt(:,1,:) = nan;


f = figure('Name', 'vopt');
for icond = 1:size(vopt,3)
    fx_drawcurve(vopt(:,:,icond), col(icond,:), 'ci'); % average over lengths
end
set(gca, 'YLim', [0 1], 'YTick', [0:0.2:1], 'XTick', [1 8:4:20], 'FontName', 'Helvetica', 'FontSize', 7.2);
xlabel('choice position');
ylabel('fraction choices');

t = title(sprintf('direction: target-colour'));
f = gcf;
f = fx_sqbeautify(f, psz, fsz);
print([figsdir expename '_behav_choicedir_target'],'-painters',figf);

f = figure('Name', 'copt');
for icond = 1:size(copt,3)
    fx_drawcurve(copt(:,:,icond), col(icond,:), 'ci'); % average over lengths
end
set(gca, 'YLim', [0 1], 'YTick', [0:0.2:1], 'XTick', [1 8:4:20], 'FontName', 'Helvetica', 'FontSize', 7.2);
xlabel('choice position');
ylabel('fraction choices');

t = title(sprintf('direction: uncertainty'));
f = gcf;
f = fx_sqbeautify(f, psz, fsz);
print([figsdir '/' expename '_behav_choicedir_information'],'-painters',figf);





