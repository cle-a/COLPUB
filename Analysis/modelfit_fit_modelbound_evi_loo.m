function [out] = modelfit_fit_modelbound_evi_loo(cfg)

% check input arguments
if ~isfield(cfg,'fitalgo')
    cfg.fitalgo = 'fmincon';
end
if ~isfield(cfg,'nrun')
    cfg.nrun = 1;
end
if ~isfield(cfg,'nsim')
    cfg.nsim = 1;
end
if ~isfield(cfg,'verbose')
    cfg.verbose = 0;
end

% set fitting parameters
peps = 1e-6; % infinitesimal choice probability
perr = 1/3;  % generative fraction of missorted colors

% define logit and sigmoid functions
logit = @(x)log(x./(1-x));
sigmd = @(x)1./(1+exp(-x));

% get color-to-evidence factor
cfac = get_cfac(perr);

% get number of sequences
nseq = numel(cfg.choices);

% get number of observations per sequence
nobsperseq = cellfun(@numel,cfg.choices);

if cfg.conditn == 2
    % do not fit sensitivity to value
    cfg.betav = 0;
end

if isfield(cfg,'theta') && cfg.theta == 0
    % do not fit other bound parameters
    cfg.omega = 0;
    cfg.gamma = 0;
end

% define model parameters
pnam = cell(6,1); % name
pini = nan(6,1);  % initial value
pmin = nan(6,1);  % lower bound
pmax = nan(6,1);  % upper bound
pplb = nan(6,1);  % plausible lower bound
ppub = nan(6,1);  % plausible upper bound
pfun = cell(6,1); % prior function

% betav = sensitivity to value ~ normal(0,2)
pnam{1} = 'betav';
pini(1) = 0;
pmin(1) = -10;
pmax(1) = +10;
pplb(1) = -1;
ppub(1) = +1;
pfun{1} = @(x)normpdf(x,0,2);

% betac = sensitivity to certainty ~ normal(0,0.5)
pnam{2} = 'betac';
pini(2) = 0;
pmin(2) = -10;
pmax(2) = +10;
pplb(2) = -5;
ppub(2) = +5;
pfun{2} = @(x)normpdf(x,0,0.5);

% betar = repetition bias ~ normal(0,2)
pnam{3} = 'betar';
pini(3) = 0;
pmin(3) = -10;
pmax(3) = +10;
pplb(3) = -5;
ppub(3) = +5;
pfun{3} = @(x)normpdf(x,0,2);

% theta = bound level ~ logit(uniform(0.5,1))
pnam{4} = 'theta';
pini(4) = logit(0.8);
pmin(4) = 0;
pmax(4) = 10;
pplb(4) = 0.1;
ppub(4) = 5;
pfun{4} = @(x)unifpdf(sigmd(x),0.5,1).*sigmd(x).*(1-sigmd(x));

% omega = bound hardness ~ beta(4,1)
pnam{5} = 'omega';
pini(5) = 0.8;
pmin(5) = 0;
pmax(5) = 1;
pplb(5) = 0.1;
ppub(5) = 0.9;
pfun{5} = @(x)betapdf(x,4,1);

% gamma = bound standard deviation ~ exp(0.1)
pnam{6} = 'gamma';
pini(6) = 0.1;
pmin(6) = 0.001;
pmax(6) = 1;
pplb(6) = 0.01;
ppub(6) = 0.5;
pfun{6} = @(x)exppdf(x,0.1);

npar = numel(pnam);

% set initial parameter values
if isfield(cfg,'pini')
    for ipar = 1:npar
        if isfield(cfg.pini,pnam{ipar})
            pini(ipar) = cfg.pini.(pnam{ipar});
        end
    end
end

% define fixed parameters
pfix = nan(npar,1);
for i = 1:npar
    if isfield(cfg,pnam{i})
        pfix(i) = cfg.(pnam{i});
    end
end

% define fitted parameters
ifit = nan(npar,1);
pfit_ini = [];
pfit_min = [];
pfit_max = [];
pfit_plb = [];
pfit_pub = [];
n = 1;
for i = 1:npar
    if isnan(pfix(i))
        ifit(i) = n;
        pfit_ini = cat(2,pfit_ini,pini(i));  
        pfit_min = cat(2,pfit_min,pmin(i));
        pfit_max = cat(2,pfit_max,pmax(i));
        pfit_plb = cat(2,pfit_plb,pplb(i));
        pfit_pub = cat(2,pfit_pub,ppub(i));
        n = n+1;
    end
end
nfit = numel(pfit_ini);
nobs = sum(cellfun(@length,cfg.choices));

if nfit > 0
    % set display level
    switch cfg.verbose
        case 0, opt_disp = 'none';
        case 1, opt_disp = 'final';
        case 2, opt_disp = 'iter';
    end
    nrun = cfg.nrun;

    if ~exist(lower(cfg.fitalgo),'file')
        error('%s not found in path!',lower(cfg.fitalgo));
    end
    switch lower(cfg.fitalgo)
        
        case 'fmincon'
            
            % configure fmincon
            options = optimoptions('fmincon');
            options.Algorithm = 'interior-point'; % fitting algorithm
            options.FunValCheck = 'on'; % check objective function
            options.MaxFunEvals = 1e6; % maximum number of function evaluations
            options.TolX = 1e-20; % tolerance on objective function
            options.Display = opt_disp; % display level
            
            % fit model
            llh_trn = 0; nobs_trn = 0;
            llh_tst = 0; nobs_tst = 0;
            for iloo = 1:nseq
                fval_trn = zeros(1,nrun);
                fval_tst = zeros(1,nrun);
                for irun = 1:nrun
                    if nrun > 1
                        % set random starting point
                        for i = 1:nfit
                            pfit_ini(i) = unifrnd(pfit_plb(i),pfit_pub(i));
                        end
                    end
                    % fit model
                    pval = fmincon(@fmin_loo, ...
                        pfit_ini,[],[],[],[],pfit_min,pfit_max,[],options);
                    % get output
                    [fval_trn(irun),fval_tst(irun)] = fmin_loo(pval);
                end
                % find best fit among random starting points
                [~,imin] = min(fval_trn);
                llh_trn = llh_trn-fval_trn(imin);
                llh_tst = llh_tst-fval_tst(imin);
                % get number of observations
                nobs_trn = nobs_trn+sum(nobsperseq(setdiff(1:nseq,iloo)));
                nobs_tst = nobs_tst+nobsperseq(iloo);
            end
            
            % create fitting output structure
            out = [];

            % account for fixed bound level as implicit parameter
            if isfield(cfg,'theta') && cfg.theta > 0
                nfit = nfit+1;
            end
            out.nfit = nfit; % number of fitted parameters
            out.nobs = nobs; % number of observations
            
            % get number of observations
            out.nobs_trn = nobs_trn; % training set
            out.nobs_tst = nobs_tst; % test set
            
            % get log-likelihood
            out.llh_trn = llh_trn; % training set
            out.llh_tst = llh_tst; % test set
            
        case 'bads'
            
            % configure BADS
            options = bads('defaults');
            options.UncertaintyHandling = false; % noise-free objective function
            options.NoiseFinalSamples = 100; % number of final samples
            options.Display = opt_disp; % display level
            
            % fit model
            llh_trn = 0; nobs_trn = 0;
            llh_tst = 0; nobs_tst = 0;
            for iloo = 1:nseq
                fval_trn = zeros(1,nrun);
                fval_tst = zeros(1,nrun);
                for irun = 1:nrun
                    if nrun > 1
                        % set random starting point
                        for i = 1:nfit
                            pfit_ini(i) = unifrnd(pfit_plb(i),pfit_pub(i));
                        end
                    end
                    % fit model
                    pval = bads(@fmin_loo, ...
                        pfit_ini,pfit_min,pfit_max,pfit_plb,pfit_pub,[],options);
                    % get output
                    [fval_trn(irun),fval_tst(irun)] = fmin_loo(pval);
                end
                % find best fit among random starting points
                [~,imin] = min(fval_trn);
                llh_trn = llh_trn-fval_trn(imin);
                llh_tst = llh_tst-fval_tst(imin);
                % get number of observations
                nobs_trn = nobs_trn+sum(nobsperseq(setdiff(1:nseq,iloo)));
                nobs_tst = nobs_tst+nobsperseq(iloo);
            end

            % create fitting output structure
            out = [];

            % account for fixed bound level as implicit parameter
            if isfield(cfg,'theta') && cfg.theta > 0
                nfit = nfit+1;
            end
            out.nfit = nfit; % number of fitted parameters
            out.nobs = nobs; % number of observations
            
            % get number of observations
            out.nobs_trn = nobs_trn; % training set
            out.nobs_tst = nobs_tst; % test set
            
            % get log-likelihood
            out.llh_trn = llh_trn; % training set
            out.llh_tst = llh_tst; % test set
            
        otherwise
            error('Undefined fitting algorithm!');
    
    end
    
else
    error('This function is not fit for simulations!');
    
end

    function [ftrn,ftst] = fmin_loo(p)
        % get parameter set
        pfit = cell(npar,1);
        for i = 1:npar
            if isnan(pfix(i)) % free parameter
                pfit{i} = p(ifit(i));
            else % fixed parameter
                pfit{i} = pfix(i);
            end
        end
        % get negative log-likelihood
        lseq = get_lseq(pfit{:});
        ftrn = -sum(lseq(setdiff(1:nseq,iloo))); % training set
        ftst = -lseq(iloo); % test set
    end

    function [lseq,xseq,xcol] = get_lseq(betav,betac,betar,theta,omega,gamma)
        % get log-likelihood
        lseq = nan(nseq,1);
        xseq = nan(nseq,2);
        xcol = cell(nseq,1);
        for iseq = 1:nseq
            nsmp = numel(cfg.choices{iseq});
            lsmp = nan(nsmp,1);
            xc = nan(nsmp,2);
            pc = nan(nsmp,2);
            bc = true(1,2);
            if theta == 0
                bc(:) = false;
            end
            xc(1,:) = 0;
            pc(1,:) = 0.5;
            lsmp(1) = log(0.5);
            for ismp = 2:nsmp
                ch = cfg.choices{iseq}(ismp-1);
                un = 3-ch;
                xc(ismp,ch) = xc(ismp-1,ch)+cfg.colvals{iseq}(ch,ismp-1)*cfac;
                xc(ismp,un) = xc(ismp-1,un);
                dv =    betav*(xc(ismp,1)-xc(ismp,2))*cfg.targetc(iseq);
                dv = dv+betac*(abs(xc(ismp,1))-abs(xc(ismp,2)));
                dv = dv+betar*(3-2*ch);
                if bc(ch) == true
                    if gamma == 0
                        pinb = abs(xc(ismp,ch)) < theta;
                    else
                        pinb = 1-gamcdf(abs(xc(ismp,ch)),theta^2/gamma^2,gamma^2/theta);
                    end
                    pc_inb = nan(1,2);
                    pc_inb(ch) = omega;
                    pc_inb(un) = 1-omega;
                    pc_out = nan(1,2);
                    pc_out(1) = 1/(1+exp(-dv));
                    pc_out(2) = 1-pc_out(1);
                    pc(ismp,:) = pc_inb*pinb+pc_out*(1-pinb);
                else
                    pc(ismp,1) = 1/(1+exp(-dv));
                    pc(ismp,2) = 1-pc(ismp,1);
                end
                ch_prv = ch;
                ch_cur = cfg.choices{iseq}(ismp);
                if ch_cur ~= ch_prv
                    bc(ch_prv) = false;
                end
                ch = cfg.choices{iseq}(ismp);
                un = 3-ch;
                lsmp(ismp) = log(peps+(1-peps*2)*pc(ismp,ch));
            end
            xc(ismp+1,ch) = xc(ismp,ch)+cfg.colvals{iseq}(ch,ismp)*cfac;
            xc(ismp+1,un) = xc(ismp,un);
            xseq(iseq,:)  = xc(ismp+1,:);
            lseq(iseq)    = sum(lsmp);
            xcol{iseq}    = xc;
        end
    end

end

function [cfac] = get_cfac(perr)
% get color-to-evidence factor
x = -1:0.001:+1;
cfac = fzero(@(b)trapz(x(x > 0),1./(1+exp(x(x > 0)*b))/trapz(x,1./(1+exp(x*b))))-perr,[0,10]);
end