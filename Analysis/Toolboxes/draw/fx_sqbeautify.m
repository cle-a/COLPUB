function [fighdl] = fx_sqbeautify(fighdl, psz, fsz, printyes, widthratio)

if nargin < 5
    widthratio = 1;
end

if nargin < 4
    printyes = 1;
end

if nargin < 3
    fsz = 20;
end

if nargin < 2
    psz = nan;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

set(0, 'CurrentFigure', fighdl)
set(fighdl, 'Color', 'white')

%%% set fontsizes for axes
set(gca,'FontName','Helvetica','FontSize',0.9*fsz);

%%% set aspect ratio 
pbar = 1;
set(gca, 'Layer', 'bottom', 'Box', 'off');
set(gca, 'TickDir', 'out', 'TickLength', [1,1]*0.02/max(pbar,1));
set(gca, 'PlotBoxAspectRatio', [pbar*widthratio,1,1]);

%%% make all axes black and bring them to front
axes = findobj(gcf, 'type', 'axes');
for a = 1:length(axes),
    if axes(a).YColor <= [1 1 1]; axes(a).YColor = [0 0 0]; end
    if axes(a).XColor <= [1 1 1]; axes(a).XColor = [0 0 0]; end
    axes(a).LineWidth  = 0.75;
    axes(a).TickLength = [0.03 0.03];
end
set(gca ,'Layer', 'Top');

%%% fix _sizes_ of axes and fig
if printyes == 1
    if ~isnan(psz)
        if numel(psz) == 1
            psz = repelem(psz,2);
        end
%         set(gca, 'Units', 'Centimeters', 'position', [1.2 1.2 psz(2) psz(1)]);
%         set(gcf, 'Units', 'Centimeters', 'Position', [0 0 psz(2)+2 psz(1)+2]);
% 
%         set(gca, 'Units', 'Centimeters', 'position', [40/100*psz(2) 40/100*psz(1) psz(2) psz(1)]);
%         set(gcf, 'Units', 'Centimeters', 'Position', [0 0 psz(2)+60/100*psz(2) psz(1)+60/100*psz(2)]);
        
        set(gca, 'Units', 'Centimeters', 'position', [50/100*psz(2) 50/100*psz(1) psz(2) psz(1)]);
        set(gcf, 'Units', 'Centimeters', 'Position', [0 0 psz(2)+100/100*psz(2) psz(1)+100/100*psz(2)]);
    end
elseif printyes == 0
else
    fprintf('doesn''t know whether to resize fig and axes\n');
end


%%% aligning title:
yl = ylim;
xl = xlim;
yr = range(ylim);

if size(fighdl.Children, 1) < 1 % titles are stored differently if several graphics on same fig...
    set(fighdl.Children.Title, 'FontName', 'Helvetica', 'FontSize',fsz, 'FontWeight', 'normal',...
        'HorizontalAlignment', 'left', 'Position', [xl(1) yl(2)+0.05*yr 0]);
end 
if size(fighdl.Children, 1) >= 1
    a = gca;
    set(a.Title, 'FontName', 'Helvetica', 'FontSize',fsz, 'FontWeight', 'normal',...
        'HorizontalAlignment', 'left','Position', [xl(1) yl(2)+0.05*yr 0]); 
end


end