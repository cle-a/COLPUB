function [hf] = plot_violins(xdat,wdat,rgb,pbar,figh,grptyp,outline)
%  PLOT_VIOLINS  Plot violins
%
%  Usage: [hf] = PLOT_VIOLINS(xdat,rgb,pbar,figh,grptyp)
%
%  Valentin Wyart <valentin.wyart@ens.fr>

% check input arguments

if nargin < 7
    outline = true;
end
if nargin < 6 || isempty(grptyp)
    grptyp = 'avg_sem';
end
if nargin < 5
    figh = [];
end
if nargin < 4
    pbar = [];
end
if nargin < 3
    rgb = [];
end
if nargin < 2 || isempty(wdat)
    for i = 1:numel(xdat)
        wdat{i} = ones(size(xdat{i}));
    end
end
if nargin < 1
    error('Missing input arguments!');
end

% get number of violins
if iscell(xdat)
    nbar = numel(xdat);
else
    nbar = size(xdat,2);
end

% set default plot box aspect ratio
if isempty(pbar)
    pbar = 1/2*(nbar+0.2)/2.2;
end

% set bar colors
if isempty(rgb)
    rgb = rand(nbar,3);
elseif size(rgb,1) == 1
    rgb = repmat(rgb,[nbar,1]);
elseif size(rgb,1) < nbar
    error('Invalid number of colors provided!')
end

% plot violins
hf = figure;
hold on
xlim([0.4,nbar+0.6]);
MARKSIZE = 10; % posters, MarkerSize 100 AND FOR AVG/MED = 10; for figs markersize = 10, avg/med = 4
for ibar = 1:nbar
    x = xdat{ibar};
    w = wdat{ibar};
    pos = ibar;
    wid = 0.2;
%    minmax = ksdensity(x,[0.025,0.975],'Weights',w','Function','icdf');
%    xk = linspace(minmax(1),minmax(2),100);
    xk = linspace(min(x),max(x),100);
    
    pk = ksdensity(x,xk,'Weights',w);
    pk = pk/max(pk);
    str = interp1(xk,pk,x);
    jit = linspace(-1,+1,numel(x));
    if outline
        patch([pos+pk*wid,fliplr(pos-pk*wid)],[xk,fliplr(xk)], ...
            0.5*(rgb(ibar,:)+1),'EdgeColor','none','FaceAlpha',0.5);
        plot(pos+pk*wid,xk,'-','Color',0.5*(rgb(ibar,:)+1),'LineWidth',0.5);
        plot(pos-pk*wid,xk,'-','Color',0.5*(rgb(ibar,:)+1),'LineWidth',0.5);
    end
    for i = randperm(numel(x))
%         plot(pos+jit(i)*str(i)*wid,x(i),'wo', ...
%             'MarkerFaceColor',rgb(ibar,:)*w(i)+1-w(i),'MarkerSize',3,'LineWidth',0.5);

        % Clé: regular figure MarkerSize = 10-15
%         sc = scatter(pos+jit(i)*str(i)*wid,x(i),10,'o', ...
%             'MarkerFaceColor',0.5*(rgb(ibar,:)+1),'MarkerEdgeColor',0.5*(rgb(ibar,:)+1),...
%             'MarkerFaceAlpha',0.7, 'MarkerEdgeAlpha',0, 'Linewidth', 0.2);
        
        % posters, MarkerSize 100 AND FOR AVG/MED = plot = 10
        sc = scatter(pos+jit(i)*str(i)*wid,x(i),MARKSIZE,'o', ...
            'MarkerFaceColor',0.5*(rgb(ibar,:)+1),'MarkerEdgeColor',0.5*(rgb(ibar,:)+1),...
            'MarkerFaceAlpha',0.7, 'MarkerEdgeAlpha',0, 'Linewidth', 0.2);
        
%         sc.MarkerFaceAlpha = w(i);
%         sc.MarkerFaceAlpha = 0.7;
%         sc.MarkerEdgeAlpha = w(i);

        
        
    end
    switch grptyp
        case 'avg_sem'
            wavg = @(xs,ws)sum(ws.*xs)/sum(ws);
            wstd = @(xs,ws)sqrt(sum(ws.*(xs-wavg(xs,ws)).^2)/((numel(xs)-1)/numel(xs)*sum(ws)));
            plot(pos*[1,1],wavg(x,w)+wstd(x,w)/sqrt(sum(w))*1.96*[-1,+1],'-','Color',rgb(ibar,:), 'LineWidth', 1);
            plot(pos,wavg(x,w),'o', ...
                'MarkerFaceColor',rgb(ibar,:),'MarkerSize',4, 'LineWidth',0.5, 'Color','none');
            % MarkerSize 10 for posters
            % MarkerSize 4  otherwise
            
            
            %             plot(pos*[1,1],mean(x)+std(x)/sqrt(numel(x))*[-1,+1],'k-');
            %             plot(pos,mean(x),'ko', ...
            %                 'MarkerFaceColor','k','MarkerSize',4,'LineWidth',0.5,'Color','none');
        case 'avg_ci'
            wavg = @(xs,ws)sum(ws.*xs)/sum(ws);
            wstd = @(xs,ws)sqrt(sum(ws.*(xs-wavg(xs,ws)).^2)/((numel(xs)-1)/numel(xs)*sum(ws)));
            plot(pos*[1,1],wavg(x,w)+wstd(x,w)/sqrt(sum(w))*1.96*[-1,+1],'-','Color',rgb(ibar,:), 'LineWidth', 1);
            plot(pos,wavg(x,w),'o', ...
                'MarkerFaceColor',rgb(ibar,:),'MarkerSize',4, 'LineWidth',0.5, 'Color','none');
            
        case 'med_iqr'
            plot(pos*[1,1],quantile(x,[0.25,0.75]),'k-', 'Color', [0.6 0.6 0.6], 'LineWidth', 1);
            plot(pos,median(x),'ko', ...
                'MarkerFaceColor',[0.6 0.6 0.6],'MarkerSize',4, 'LineWidth',0.5,'Color','none');
            % MarkerSize 10 for posters
            % MarkerSize 4  otherwise
            
            
        otherwise
            error('Undefined group-level statistics type!');
    end
end
% plot([1,nbar],min(ylim)*[1,1],'k-');
hold off
set(gca,'Layer','top','Box','off','PlotBoxAspectRatio',[pbar,1,1]);
set(gca,'TickDir','out','TickLength',[1,1]*0.02/max(pbar,1));
set(gca,'FontName','Helvetica','FontSize',7.2);
set(gca,'XTick',1:nbar);
% make axes full black
axes = findobj(gcf, 'type', 'axes');
for a = 1:length(axes),
    if axes(a).YColor <= [1 1 1]
        axes(a).YColor = [0 0 0];
    end
    if axes(a).XColor <= [1 1 1]
        axes(a).XColor = [0 0 0];
    end
end
% if ~isempty(figh)
%     % set figure height (and other properties)
%     set(hf,'PaperPositionMode','manual', ...
%         'PaperPosition',[2.5,13,16,figh],'PaperUnits','centimeters', ...
%         'PaperType','A4','PaperOrientation','portrait');
%     % set figure as current figure (for printing)
%     figure(hf);
% end

end