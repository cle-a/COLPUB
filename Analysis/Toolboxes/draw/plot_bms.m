function [hf] = plot_bms(pavg,pstd,rgb,pbar,figh)
%  PLOT_BMS  Plot BMS results
%
%  Usage: [hf] = PLOT_BMS(pavg,pstd,rgb,pbar,figh)
%
%  Valentin Wyart <valentin.wyart@ens.fr>

% check input arguments
if nargin < 5
    figh = [];
end
if nargin < 4
    pbar = [];
end
if nargin < 3
    rgb = [];
end
if nargin < 2
    error('Missing input arguments!');
end

% get number of bars
nbar = numel(pavg);

% set default plot box aspect ratio
if isempty(pbar)
    pbar = 1/2*(nbar+0.2)/2.2;
end

% set bar colors
if isempty(rgb)
    rgb = rand(nbar,3);
elseif size(rgb,1) == 1
    rgb = repmat(rgb,[nbar,1]);
elseif size(rgb,1) ~= nbar
    error('Invalid number of colors provided!')
end

% plot figure
hf = figure;
hold on
xlim([0.4,nbar+0.6]);
ylim([0,1]);
for ibar = 1:nbar
    bar(ibar,pavg(ibar),0.8, ...
        'FaceColor',0.5*(rgb(ibar,:)+1),'FaceAlpha',0.5, ...
        'EdgeColor',rgb(ibar,:),'LineWidth',1);
    plot(ibar*[1,1],pavg(ibar)+pstd(ibar)*[+1,-1],'-','Color',rgb(ibar,:));
end
% plot([1,nbar],[0,0],'k-');
hold off
set(gca,'Layer','top','Box','off','PlotBoxAspectRatio',[pbar,1,1]);
set(gca,'TickDir','out','TickLength',[1,1]*0.02/max(pbar,1));
set(gca,'FontName','Helvetica','FontSize',7.2);
set(gca,'XTick',1:nbar);
set(gca,'YTick',0:0.2:1);
xlabel('model','FontSize',8);
ylabel('model frequency','FontSize',8);
% make axes full black
axes = findobj(gcf, 'type', 'axes');
for a = 1:length(axes),
    if axes(a).YColor <= [1 1 1]
        axes(a).YColor = [0 0 0];
    end
    if axes(a).XColor <= [1 1 1]
        axes(a).XColor = [0 0 0];
    end
end
% if ~isempty(figh)
%     % set figure height (and other properties)
%     set(hf,'PaperPositionMode','manual', ...
%         'PaperPosition',[2.5,13,16,figh],'PaperUnits','centimeters', ...
%         'PaperType','A4','PaperOrientation','portrait');
%     % set figure as current figure (for printing)
%     figure(hf);
% end

end