function [curve] = fx_drawcurve(Y, COLOR, ERBAR)
%%% takes: -  Y the vect to plot
%%%        - PATCH the type of interval to plot: either 'sem' or 'ci'
%%%        - COLOR the color of the curve and patch in [0 0 0]
%%% plots the curve, learning curve-style, no smoothing

    x = 1:size(Y,2);
    curve.meany = nanmean(Y, 1);
    curve.serry = nanstd(Y, 0, 1) / sqrt(size(Y, 1));
    curve.interval = ERBAR;
    if strcmp(ERBAR, 'ci')
        cv = 1.96; % critical value for CI 95%
        curve.ciup = sum([curve.meany; curve.serry*cv], 'omitnan');
        curve.cilo = sum([curve.meany; -curve.serry*cv], 'omitnan');
    elseif strcmp(ERBAR, 'sem')
        curve.ciup = sum([curve.meany; curve.serry], 'omitnan');
        curve.cilo = sum([curve.meany; -curve.serry], 'omitnan');
    else
        warning('default patch is confidence interval')
        cv = 1.96; % critical value for CI 95%
        curve.ciup = sum([curve.meany; curve.serry*cv], 'omitnan');
        curve.cilo = sum([curve.meany; -curve.serry*cv], 'omitnan');
    end

    % plot line and patch interval
    hold on
    curve.color = COLOR;
    x_ax        = x;
    X_patch     = [x_ax, fliplr(x_ax)];
    Y_patch     = [curve.cilo fliplr(curve.ciup)];
    curve.patch = fill(X_patch, Y_patch , 1,...
        'facecolor', COLOR, ...
        'edgecolor','none', ...
        'facealpha', 0.3);
    set(curve.patch, 'HandleVisibility','on');
    curve.meanline = plot(x, curve.meany, '-', 'Color', COLOR, 'LineWidth', 1);
    set(curve.meanline, 'HandleVisibility','off');

    % prettying
    set(gca,'PlotBoxAspectRatio', [1,1,1])
    set(gca,'Box','off','TickDir','out');
    set(gca,'FontName','Helvetica','FontSize', 12);
    axes = findobj(gcf, 'type', 'axes');
    for a = 1:length(axes)
        if axes(a).YColor <= [1 1 1]; axes(a).YColor = [0 0 0]; end
        if axes(a).XColor <= [1 1 1]; axes(a).XColor = [0 0 0]; end
    end

end