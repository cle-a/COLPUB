% matlab crashed at the end of the 4th bloc after the break = I restarted manually at block 5
% need to stitch together the bloc saves:

clearvars;
clc;

load(sprintf('S08_expe_b%02d.mat', 4))
firstfinal = final;
firstsampl = sampling;
load(sprintf('S08_expe_b%02d.mat', 8))

final    = [firstfinal, final(65:end)];
sampling = [firstsampl, sampling(65:end)];

save('S08_expe.mat', 'header', 'sampling', 'final', 'sqc', 'video', 'ME');