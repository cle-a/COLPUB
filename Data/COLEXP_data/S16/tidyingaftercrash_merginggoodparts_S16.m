clearvars

old = load('expout_S16_OLDPART.mat');
bl3 = load('expout_S16_b03.mat'); % has all the sampling and final choice data bl1 to bl3
bl8 = load('expout_S16_b08.mat'); % has all sampling and final choice data  bl4 to bl8

% recreating expout from partial data:
sqc = old.sqc; % generated runs
ME = 'note: was recreated by clemence by hand post-hoc because crashed in block 4 (adapter issue...)';
header.subj = 16;
header.start = bl3.header.start; %starts at the begining
header.end = old.header.end;
header.aborted = 0; % did abort in block 4

video = old.video; % PTB settings are unchanged

% sampling: (only has the elements of the corresponding blocks)
sampling = struct;
for i1 = 1:size([bl3.sampling.subj],2)
    sampling(i1).subj = bl3.sampling(i1).subj;
    sampling(i1).condition = bl3.sampling(i1).condition;
    sampling(i1).mirroring = bl3.sampling(i1).mirroring;
    sampling(i1).blk = bl3.sampling(i1).blk;
    sampling(i1).blkseq = bl3.sampling(i1).blkseq;
    sampling(i1).isqc = bl3.sampling(i1).isqc;
    sampling(i1).samplenb = bl3.sampling(i1).samplenb;
    sampling(i1).nsamples = bl3.sampling(i1).nsamples;
    sampling(i1).symbolpair = bl3.sampling(i1).symbolpair;
    sampling(i1).targetsym = bl3.sampling(i1).targetsym;
    sampling(i1).counterfness = bl3.sampling(i1).counterfness;
    sampling(i1).targetcolor = bl3.sampling(i1).targetcolor;
    sampling(i1).targetside = bl3.sampling(i1).targetside;
    sampling(i1).samplingchoice = bl3.sampling(i1).samplingchoice;
    sampling(i1).samplingcolor = bl3.sampling(i1).samplingcolor;
    sampling(i1).samplingrt = bl3.sampling(i1).samplingrt;
    sampling(i1).finaltargetside = bl3.sampling(i1).finaltargetside;
    sampling(i1).finalresponse = bl3.sampling(i1).finalresponse;
    sampling(i1).finalrt = bl3.sampling(i1).finalrt;
end

for i2 = 1 : size([bl8.sampling.subj],2)
    it = i2 + size([bl3.sampling.subj],2);
    sampling(it).subj = bl8.sampling(i2).subj;
    sampling(it).condition = bl8.sampling(i2).condition;
    sampling(it).mirroring = bl8.sampling(i2).mirroring;
    sampling(it).blk = bl8.sampling(i2).blk;
    sampling(it).blkseq = bl8.sampling(i2).blkseq;
    sampling(it).isqc = bl8.sampling(i2).isqc;
    sampling(it).samplenb = bl8.sampling(i2).samplenb;
    sampling(it).nsamples = bl8.sampling(i2).nsamples;
    sampling(it).symbolpair = bl8.sampling(i2).symbolpair;
    sampling(it).targetsym = bl8.sampling(i2).targetsym;
    sampling(it).counterfness = bl8.sampling(i2).counterfness;
    sampling(it).targetcolor = bl8.sampling(i2).targetcolor;
    sampling(it).targetside = bl8.sampling(i2).targetside;
    sampling(it).samplingchoice = bl8.sampling(i2).samplingchoice;
    sampling(it).samplingcolor = bl8.sampling(i2).samplingcolor;
    sampling(it).samplingrt = bl8.sampling(i2).samplingrt;
    sampling(it).finaltargetside = bl8.sampling(i2).finaltargetside;
    sampling(it).finalresponse = bl8.sampling(i2).finalresponse;
    sampling(it).finalrt = bl8.sampling(i2).finalrt;
end

% final: (always has 96 elements,  but the beginning is empty)
for i1 = 1:size([bl3.final.subj],2)
    bl8.final(i1).subj = bl3.final(i1).subj;
    bl8.final(i1).condition = bl3.final(i1).condition;
    bl8.final(i1).mirroring = bl3.final(i1).mirroring;
    bl8.final(i1).blk = bl3.final(i1).blk;
    bl8.final(i1).blkseq = bl3.final(i1).blkseq;
    bl8.final(i1).isqc = bl3.final(i1).isqc;
    bl8.final(i1).nsamples = bl3.final(i1).nsamples;
    bl8.final(i1).symbolpair = bl3.final(i1).symbolpair;
    bl8.final(i1).targetsymbol = bl3.final(i1).targetsymbol;
    bl8.final(i1).counterfness = bl3.final(i1).counterfness;
    bl8.final(i1).targetcolor = bl3.final(i1).targetcolor;
    bl8.final(i1).goodanswerside = bl3.final(i1).goodanswerside;
    bl8.final(i1).finalresponse = bl3.final(i1).finalresponse;
    bl8.final(i1).finalrt = bl3.final(i1).finalrt;
    bl8.final(i1).choices = bl3.final(i1).choices;
end
final = bl8.final;

save('expout_S16.mat', 'header', 'sampling', 'final', 'sqc', 'video', 'ME');



















