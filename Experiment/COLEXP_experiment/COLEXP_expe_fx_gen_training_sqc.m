function [training_sqc] = COLEXP_expe_fx_gen_training_sqc(perr)
%% MAKE TRAINING RUNS :

lengths         = [8 8 12 12 16 16 20 20];
conditions      = [1 1 1 1 2 2 2 2];
counterfness    = [1 2 3 1 2 3 1 3];
targetcolors    = [1 2 1 2 1 2 1 2];
symbols         = [1 2 3 4 5 6 7 8; 8 1 7 2 6 3 5 4]';
instrsides      = randi(2,[1, numel(lengths)]);
probesides      = randi(2,[1, numel(lengths)]);
colorvalues     = fx_get_linearcolorvalues(lengths, counterfness, targetcolors, perr);

%%%%%%% loop:
training_sqc = struct();
nruns = numel(lengths);
for irun = 1:nruns
    
    training_sqc(irun).genid = irun;
    training_sqc(irun).nsamples = lengths(irun);
    training_sqc(irun).condition = conditions(irun);
    training_sqc(irun).counterfness = counterfness(irun);
    training_sqc(irun).targetcolor = targetcolors(irun);
    training_sqc(irun).symbolpair = symbols(irun,:);
    training_sqc(irun).targetsymbol = training_sqc(irun).symbolpair(1);
    training_sqc(irun).othersymbol = training_sqc(irun).symbolpair(2);
    training_sqc(irun).instrside = instrsides(irun); % instruction symbol sides
    training_sqc(irun).probeside = probesides(irun); % final probe side

    
    % draw equalised-difficulty color values, and convert each to rgb
    training_sqc(irun).colorslin.target = colorvalues(irun).target';
    training_sqc(irun).colorslin.other = colorvalues(irun).other';
    training_sqc(irun).colors.target = fx_get_rgb(training_sqc(irun).colorslin.target);
    training_sqc(irun).colors.other = fx_get_rgb(training_sqc(irun).colorslin.other);
    
    % randomise symbol sides for each sample, max repeated side = 3
    nrepeated = 3; %
    symside = zeros(1,lengths(irun)); % init as many samples:
    while abs(diff(hist(symside,1:2)))>1 % while not equal number of 1s and 2s
        symside(1:nrepeated) = randi(2,1,nrepeated); %fill the first three
        for i = (nrepeated+1):lengths(irun) % loop on following ones:
            ipos = randi(2,1); % get one random
            if sum(symside((i-nrepeated):(i-1))==ipos)>=nrepeated %if last 3 were already == thisside
                symside(i) = 3-ipos;%take other side
            else; symside(i) = ipos;%else take this side
            end
        end
    end
    training_sqc(irun).symbolsides = symside; % at each sample,
    
end

%%%
% shuffling runs, with no more than twice same condition:
nbackcondition = 2;
while true %while shuffling doesn't match constraints, reshuffle the runs
    shuffle = randperm(nruns,nruns);
    training_sqc = training_sqc(shuffle);
    
    %for each run check constraints
    training_sqc(1).blkrunnb = 1;
    for rnk = 2:nruns
        % check nback for condition and counterfness
        if rnk == 2 ; diffcond = 1; diffcf = 1;
        else; diffcond = sum(abs(diff([training_sqc(rnk-nbackcondition:rnk).condition]))); 
              diffcf = sum(abs(diff([training_sqc(rnk-nbackcondition:rnk).counterfness])));  
        end
        % for condition target and counterfness badbad, check 1back
        if training_sqc(rnk).condition == 1 && training_sqc(rnk).counterfness == 2 
                diffbadbad = ~ (training_sqc(rnk-1).counterfness == training_sqc(rnk).counterfness);
            else; diffbadbad = 1;
        end
        % check current symbol wasn't in previous pair
        symlast = sum(ismember(training_sqc(rnk-1).symbolpair, training_sqc(rnk).symbolpair));
        if symlast ~= 0 || diffcond == 0 || diffcf == 0 || diffbadbad == 0%if doesnt work, re-shuffle
            break%break and reshuffle
        end
        training_sqc(rnk).blkrunnb = rnk;
    end
    %if successfully checked all runs, it's done = break
    if rnk == nruns && symlast == 0 && diffcond ~= 0 && diffcf ~= 0 && diffbadbad ~= 0
        break
    end
end%while this block is not good enough, shuffle again

%%% adding opt sampling behaviour:
training_sqc = fx_get_optsampling(training_sqc);

%%% saving:
stampedname = sprintf('training_sqc_%s.mat', datestr(now,'ddmmyy_HHMM'));
save(stampedname, 'training_sqc');
save('training_sqc.mat', 'training_sqc');

fprintf('Done making training_sqc.mat at %s\n', datestr(now,'HH:MM'));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
