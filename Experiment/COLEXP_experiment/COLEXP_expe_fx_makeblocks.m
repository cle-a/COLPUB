function [sqc] = COLEXP_expe_fx_makeblocks(sqc)
%%% making blocks:
%%% this runs on preconstituted sequences, it simply splits them into blocks
%%% the sequences within blocs are ordered afterwards with COLEXP_expe_orderblocks
%%%     - make random draw of 12 seq:
%%%     - check identical nb of ('condition' x 'counterfness') within block
%%%     - check same amount of blue/orange targets in each condition (trg/opn)
%%%     - check not over 7 times same symbol (noksymrepeats) within block
%%%     - check not over 2 times same pair (nuniquepairs) within block

NSEQ = numel(sqc);
nblocks = NSEQ/12;
nseqblock = NSEQ/nblocks;
nseqcond = nseqblock/2/3; % 6 possible sampling(2) X cf(3) conditions = 2 seqs per condition
nseqcol = nseqblock/2/2; % 2 possible target colors
noksymrepeats = 7;
nokpairsrepeats = 2;
nuniquepairs = nseqblock - nokpairsrepeats;

fprintf('Making blocks of seqs from >>>fx_makingblocks:\n');
while true
    
    %%%init temp structure with blk field: %todo
    currentpool = sqc;
    temp = sqc;
    temp(:) = [];
    [temp.blk] = [];
    
    %%%loop on blocks:
    for iblock = 1:nblocks
        fprintf('...block %d\n', iblock);

        while true
            
            %init drawsequences:
            idrawnseq = randperm(numel(currentpool),nseqblock);
            drawnseq = currentpool(idrawnseq);
            
            %check constraints: 
            c1a1 = sum([drawnseq.condition] == 1 & [drawnseq.counterfness] == 1);
            c1a2 = sum([drawnseq.condition] == 1 & [drawnseq.counterfness] == 2);
            c1a3 = sum([drawnseq.condition] == 1 & [drawnseq.counterfness] == 3);
            
            c2a1 = sum([drawnseq.condition] == 2 & [drawnseq.counterfness] == 1);
            c2a2 = sum([drawnseq.condition] == 2 & [drawnseq.counterfness] == 2);
            c2a3 = sum([drawnseq.condition] == 2 & [drawnseq.counterfness] == 3);
            
            maxsymreps = max(histcounts([drawnseq.symbolpair])); %get max nb of occurrences of same symbol
            pairs = reshape([drawnseq.symbolpair], [2, nseqblock])'; %get pairs from drawnseqs
            drawnpairs = size(unique(pairs,'rows'),1); %count unique pairs
            ncoltarg = sum([drawnseq.condition] == 1 & [drawnseq.targetcolor] == 2);
            ncolopen = sum([drawnseq.condition] == 2 & [drawnseq.targetcolor] == 2);
            
            if c1a1 == nseqcond && c1a2 == nseqcond && c1a3 == nseqcond && c2a1 == nseqcond && c2a2 == nseqcond && c2a3 == nseqcond ...
                    && maxsymreps < noksymrepeats && drawnpairs >= nuniquepairs && ncoltarg == nseqcol && ncolopen == nseqcol
                break
            elseif iblock == nblocks && (( maxsymreps >= noksymrepeats) || (drawnpairs < nuniquepairs)) %if at 6th block, too many of same symbol, restart at block1
                currentpool = sqc;
                iblock = 1; %go back to block1
                fprintf('...!!! BREAKING AT LAST BLOCK !!!\n')
            end%if checking conditions for this block: either break or redo this block
        end%while this block is not good enough: redo it
        
        % store drawn seqs, remove them from pool of seqs
        [drawnseq.blk] = deal(iblock);
        temp = [temp drawnseq];
        currentpool(idrawnseq) = [];
        
    end%looping over blocks
    
    %%%check all blocks succeeded:  
    if isempty(currentpool)
        break
    else  
    end%if all blocks succeeded, else restart from block 1
    
end%while havent found all blocks, restart at block 1

sqc = temp;

fprintf('Done making blocks!\n');


end% function definition