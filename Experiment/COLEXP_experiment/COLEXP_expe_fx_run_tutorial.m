%%% run tutorial sequences
%%% requires Valentin's IO Toolbox on the Experiment's path
%%% requires to have generated the tutorialsqc.mat structure
%%% NB: native KB keys are not inhibited: don't press a | p keys!
%%% NB: THIS IS NOT SAVING ANYTHING

%%% testing is 0|1 for debug mode (will/not spill keys and hide cursor)

function tutorialout = COLEXP_expe_fx_run_tutorial(testing)

try

%% addpaths & define running parameters:
addpath ./Toolboxes/IO % Valentin's toolbox in Experiment's path

% parameters:
video.testingON = testing;
video.synchflip = true;
ntrials = 7;

if testing == 1
    showtime = 0.5;
elseif testing == 0
    showtime = 2;
    HideCursor;
    FlushEvents;
    ListenChar(2);
end

% load tutorial structure: 
load('tutorial_sqc.mat');

%% PTB parameters:

%%% Set visual parameters
video.eyelink.inputphys_mm  = [660 337]; %downstairs
video.eyelink.inputscreendistance_mm = 550;
if ispc % in 2nd floor booths/downstairs
    video.screenwdth_cm = 66;
    video.screenwdth_px = 1920;
    video.ppd = round(tand(1) * video.eyelink.inputscreendistance_mm / 10/ video.screenwdth_cm * video.screenwdth_px);
    %  where:
    %    dist_cm - screen-eye distance in centimeters
    %    wdth_cm - screen width in centimeters
    %    wdth_px - screen width in pixels (native resolution)
elseif ismac % testing non mac, doesnt care.
    video.ppd = 40; % number of screen pixels per degree of visual angle
end
video.shape_sz          = 6*video.ppd; % video.ppd pixels per degree of visual angle = 40
video.shape_offset      = 4.5*video.ppd; % shape offset
video.respbutton_offset = 10*video.ppd; % response button offset
video.rep_sz            = 5*video.ppd;
video.fb_sz             = 6*video.ppd; % feedback square size
black                   = [0,0,0];
white                   = [1,1,1];
load('./color_space.mat', 'lbgd');
video.lbgd              = lbgd;
orange                  = [1,0.5,0];
blue                    = [0,0.5,1];

%%% which screens?
screens = Screen('Screens');
video.screen = max(screens); %screen index on PC: 0=extended, 1=native, 2=ext

%%% set synch properties:
if video.synchflip && ispc
    % set screen synchronization properties -- workaround Valentin for PC
    % see 'help SyncTrouble',
    %     'help BeampositionQueries' or
    %     'help ConserveVRAMSettings' for more information
    Screen('Preference','VisualDebuglevel',3); % verbosity
    Screen('Preference','SyncTestSettings',[],[],0.2,10); % soften synchronization test requirements
    Screen('Preference','ConserveVRAM',bitor(4096,Screen('Preference','ConserveVRAM'))); % enforce beamposition workaround for missing VBL interval
    fprintf('Synching flips with softer requirements...\n');
elseif ~video.synchflip || ismac
    % skip synchronization tests altogether
    % //!\\ force skipping tests, PTB wont work = timing inaccurate
    Screen('Preference','SkipSyncTests',2); % assumes 60Hz etc..
    Screen('Preference','VisualDebuglevel',0);
    Screen('Preference','SuppressAllWarnings',1);
    fprintf('||| SYNCHFLIP OFF or running on OSX => TIMINGS WILL BE INACCURATE! |||\n')
end

%%% open main window:
PsychImaging('PrepareConfiguration');
PsychImaging('AddTask','General','UseFastOffscreenWindows');
PsychImaging('AddTask','General','NormalizedHighresColorRange');
video.res = Screen('Resolution',video.screen);
[video.window, video.windowRect] = PsychImaging('OpenWindow',video.screen,video.lbgd);
fprintf('passed OpenWindow!\n')

[video.screenXpixels, video.screenYpixels] = Screen('WindowSize', video.window);
[video.xCenter, video.yCenter] = RectCenter(video.windowRect);
Screen('BlendFunction', video.window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');%smooth blending/antialiasing
video.blend = '''GL_SRC_ALPHA'', ''GL_ONE_MINUS_SRC_ALPHA''';
video.colorrange = 1;
Screen('ColorRange',video.window,video.colorrange);
% 1 = to pass color values in OpenGL's native floating point color range of 0.0 to
% 1.0: This has two advantages: First, your color values are independent of
% display device depth, i.e. no need to rewrite your code when running it on
% higher resolution hardware. Second, PTB can skip any color range remapping
% operations - this can speed up drawing significantly in some cases.   ...???
Screen('TextSize', video.window, 50);
video.textsize = 50;
video.ifi = Screen('GetFlipInterval', video.window);
video.priority = Priority(MaxPriority(video.window));
Screen('Flip', video.window);

%% Make textures:

%%% symbols textures:
shape_tex = zeros(2,8);
for sh = 1:8
    outline = double(imread(sprintf('./img/shape%dc.png',sh)))/255;
    outline = imresize(outline,video.shape_sz/size(outline,1));
    shape_tex(1,sh) = Screen('MakeTexture',video.window,cat(3,ones(size(outline)),outline),[],[],2);
    inside = double(imread(sprintf('./img/shape%d.png',sh)))/255;
    inside = imresize(inside,video.shape_sz/size(inside,1));
    shape_tex(2,sh) = Screen('MakeTexture', video.window, cat(3,ones(size(inside)),inside),[],[],2);
end
positions(1,:)  = CenterRectOnPoint(Screen('Rect',shape_tex(1)),video.xCenter-video.shape_offset,video.yCenter);
positions(2,:)  = CenterRectOnPoint(Screen('Rect',shape_tex(1)),video.xCenter+video.shape_offset,video.yCenter);
centerpos       = CenterRectOnPoint(Screen('Rect',shape_tex(1)),video.xCenter,video.yCenter);

% resp buttons textures:
outrep = double(imread('./img/bagout.png'))/255;
outrep = imresize(outrep,video.rep_sz/size(outrep,1));
inrep = double(imread('./img/bagin.png'))/255;
inrep = imresize(inrep,video.rep_sz/size(inrep,1));
rep_tex(1,1) = Screen('MakeTexture',video.window,cat(3,ones(size(outrep)),outrep),[],[],2);
rep_tex(2,1) = Screen('MakeTexture',video.window,cat(3,ones(size(inrep)),inrep),[],[],2);
rep_tex(1,2) = Screen('MakeTexture',video.window,cat(3,ones(size(outrep)),flip(outrep,2)),[],[],2);
rep_tex(2,2) = Screen('MakeTexture',video.window,cat(3,ones(size(inrep)),flip(inrep,2)),[],[],2);
rep_pos(1,:)  = CenterRectOnPoint(Screen('Rect',rep_tex(1)),video.xCenter-video.respbutton_offset,video.screenYpixels*0.80);
rep_pos(2,:)  = CenterRectOnPoint(Screen('Rect',rep_tex(1)),video.xCenter+video.respbutton_offset,video.screenYpixels*0.80);

buttonbuffer = 0.3*size(outrep,1);
selsize = [0 0 1.25*size(outrep,2)+buttonbuffer size(outrep,1)+buttonbuffer];
selection_rect(1,:) = CenterRectOnPoint(selsize, video.xCenter-video.respbutton_offset,video.screenYpixels*0.80);
selection_rect(2,:) = CenterRectOnPoint(selsize, video.xCenter+video.respbutton_offset,video.screenYpixels*0.80);

%score feedback: cup textures:
cupi = imread('./img/cupimg.png');
cupi         = imresize(cupi,video.shape_sz/size(cupi,1));
cuptext      = Screen('MakeTexture',video.window,cupi);
cuppos(1,:)  = CenterRectOnPoint(Screen('Rect',cuptext),video.xCenter-(video.shape_sz),video.yCenter);
cuppos(2,:)  = CenterRectOnPoint(Screen('Rect',cuptext),video.xCenter, video.yCenter);
cuppos(3,:)  = CenterRectOnPoint(Screen('Rect',cuptext),video.xCenter+(video.shape_sz),video.yCenter);

cupbuffer = 0.3*size(cupi,2);
cupframe = [0 0 3*size(cupi,2)+cupbuffer size(cupi,1)+cupbuffer];
cupframe_rect = CenterRectOnPoint(cupframe, video.xCenter,video.yCenter);

%% Initialise response keys:
clear PsychHID; % Force new enumeration of devices.
clear KbCheck;
KbName('UnifyKeyNames'); %across OSs
GetKeyboardIndices();

escapeKey   = KbName('ESCAPE'); %experimenter keys (native Kb)
continueKey = KbName('y');

spaceKey    = KbName('space'); %participant keys (numpad)
if ispc; leftKey = KbName('a');
elseif ismac; leftKey = KbName('q');
end
rightKey    = KbName('p');

respKeys     = [leftKey, rightKey];% left=1 right=2
expeKeys     = [escapeKey, continueKey, leftKey, rightKey, spaceKey];
allKeys      = [];
k = RestrictKeysForKbCheck(allKeys);

%% Start display:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% test kb, hide cursor, stop spilling keys, display welcome screen:
if ~video.testingON
    fprintf('Press [ESPACE] to check keyboard responsiveness...: ');
    if WaitKeyPress([],30) == 0
        fprintf('\n\n');
        error('No key press detected after 30 seconds.');
    else;fprintf('Good.\n\n');
    end
    HideCursor;
    FlushEvents;
    ListenChar(2);
end %setting testing parameters

header.start = datestr(now,'yyyymmdd-HHMM');

%%% welcome screen:
DrawFormattedText(video.window, sprintf('Please read the instructions for the experiment.\n\n\n'), 'center', video.screenYpixels * 0.25, black);
spaceline = 'Press [space] to start.';
DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.90, black);
Screen('Flip', video.window);
WaitKeyPress(spaceKey);

%% FIRST WARMUP SEQUENCE = no condition
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% intro screen: instructions + symbols for the sequence:
top = sprintf('In each sequence, you will be playing with a pair of symbols,\nfor example here the cross and star: \n\n');
DrawFormattedText(video.window, top, 'center', video.screenYpixels * 0.20, black);
spaceline = sprintf('Press [space] to start the sequence.'); 
DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.90, black);
targetsymbol = tutorialsqc(1).targetsymbol; % = 5 = star
othersymbol = tutorialsqc(1).othersymbol; % = 6 = cross
Screen('DrawDots', video.window, [video.xCenter video.yCenter], 15 , black, [0 0], 2);
Screen('DrawTexture', video.window, shape_tex(1,othersymbol), [], positions(1,:), 0, [], [], [0,0,0]);
Screen('DrawTexture', video.window, shape_tex(2,othersymbol), [], positions(1,:), 0, [], [], lbgd);
Screen('DrawTexture', video.window, shape_tex(1,targetsymbol), [], positions(2,:), 0, [], [], [0,0,0]);
Screen('DrawTexture', video.window, shape_tex(2,targetsymbol), [], positions(2,:), 0, [], [], lbgd);
vbl = Screen('Flip', video.window);% Flip and wait...
WaitSecs(2);
WaitKeyPress(spaceKey);

%%% loop on trials:
for trial = 1:ntrials
    % Check if abort key is pressed
    if CheckKeyPress(escapeKey)
        Priority(0); FlushEvents; ListenChar(0); ShowCursor;
        error('aborted by ESCAPE');
    end
    % draw alternative
    top = 'Choose a symbol with the [a] and [p] keys:';
    DrawFormattedText(video.window, top, 'center', video.screenYpixels * 0.25, black);
    targetside  = tutorialsqc(1).symbolsides(trial);
    otherside   = 3 - tutorialsqc(1).symbolsides(trial);
    Screen('DrawDots', video.window, [video.xCenter video.yCenter], 15 , black, [0 0], 2);
    Screen('DrawTexture', video.window, shape_tex(1,targetsymbol), [], positions(targetside,:), 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, shape_tex(2,targetsymbol), [], positions(targetside,:), 0, [], [], lbgd);
    Screen('DrawTexture', video.window, shape_tex(1,othersymbol), [], positions(otherside,:) , 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, shape_tex(2,othersymbol), [], positions(otherside,:) , 0, [], [], lbgd);
    DrawFormattedText(video.window, '[a]                      [p]', 'center', video.screenYpixels * 0.70, white);
    if trial ~= 5
        Screen('Flip', video.window, vbl+fx_roundfp(0.1,0,video.ifi));
        vbl = GetSecs;
    elseif trial == 5
        warning = sprintf('/ WATCH OUT \\: the symbols can switch sides!');
        DrawFormattedText(video.window, warning, 'center', video.screenYpixels * 0.35, black);
        Screen('Flip', video.window, vbl+fx_roundfp(0.1,0,video.ifi));
        WaitSecs(0.5);
        vbl = GetSecs;
    end
    
    % wait for choice & show outcome 4''
    while true
        [keyIsDown,secs,keylist] = KbCheck();
        if keyIsDown && sum(keylist) == 1 && (keylist(leftKey) == 1 || keylist(rightKey) == 1)
            %rt = secs - vbl;
            choice = find(respKeys == (find(keylist==1))); break
        end
    end
    top = sprintf('The computer reveals the colour drawn:');
    DrawFormattedText(video.window, top, 'center', video.screenYpixels * 0.25, black);
    %vbl = Screen('Flip', video.window);
    if choice == targetside
        samplecolor = tutorialsqc(1).colors.target(trial,:);
        Screen('DrawDots', video.window, [video.xCenter video.yCenter], 15 , black, [0 0], 2);
        Screen('DrawTexture', video.window, shape_tex(1,targetsymbol), [], positions(targetside,:), 0, [], [], [0,0,0]);
        Screen('DrawTexture', video.window, shape_tex(2,targetsymbol), [], positions(targetside,:), 0, [], [], samplecolor);
    elseif choice == otherside
        samplecolor = tutorialsqc(1).colors.other(trial,:);
        Screen('DrawDots', video.window, [video.xCenter video.yCenter], 15 , black, [0 0], 2);
        Screen('DrawTexture', video.window, shape_tex(1,othersymbol), [], positions(otherside,:) , 0, [], [], [0,0,0]);
        Screen('DrawTexture', video.window, shape_tex(2,othersymbol), [], positions(otherside,:) , 0, [], [], samplecolor);
    end
    vbl = Screen('Flip', video.window, secs+fx_roundfp(0.1,0,video.ifi));
    WaitSecs(showtime);
end

%%% end of sequence:
top = sprintf('Well done, you have completed one sequence!\n\n');
DrawFormattedText(video.window, top, 'center', video.screenYpixels * 0.25, black);
spaceline = sprintf('Press [space] to move on to the next sequence.');
DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.90, black);
Screen('Flip', video.window, vbl+fx_roundfp(0.1,0,video.ifi));
WaitKeyPress(spaceKey);

%% SECOND SEQUENCE: TARGET (BLUE)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% intro screen:
top1 = sprintf('At the beginning of each sequence, the computer decides on one of two rules.\n');
top2 = sprintf('The first rule is to draw a maximum from one colour:\n for instance here, try to draw blue!');
DrawFormattedText(video.window, [top1 top2], 'center', video.screenYpixels * 0.20, black);
spaceline = sprintf('Press [space] to start.'); 
DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.90, black);
% Draw sequence symbols
targetsymbol = tutorialsqc(2).targetsymbol;
othersymbol = tutorialsqc(2).othersymbol;
Screen('DrawDots', video.window, [video.xCenter video.yCenter], 15 , black, [0 0], 2);
Screen('DrawTexture', video.window, shape_tex(1,targetsymbol), [], positions(1,:), 0, [], [], [0,0,0]);
Screen('DrawTexture', video.window, shape_tex(2,targetsymbol), [], positions(1,:), 0, [], [], lbgd);
Screen('DrawTexture', video.window, shape_tex(1,othersymbol), [], positions(2,:), 0, [], [], [0,0,0]);
Screen('DrawTexture', video.window, shape_tex(2,othersymbol), [], positions(2,:), 0, [], [], lbgd);
% Flip and wait
Screen('Flip', video.window);
WaitSecs(2);
WaitKeyPress(spaceKey);

%%% warning screen:
top1 = '/ WATCH OUT \\n\n\nThe symbols are drawing *mainly* from one colour, but not only!\n';
top2 = 'If the symbol draws mostly orange, it can sometimes draw blue too...';
DrawFormattedText(video.window, [top1 top2], 'center', video.screenYpixels * 0.15, black);
spaceline = sprintf('Press [space] to start.');
DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.90, black);
% % Draw sequence symbols
% Flip and wait
vbl = Screen('Flip', video.window);
WaitSecs(2);
WaitKeyPress(spaceKey);

%%% instructions again:

line = 'These are the symbols, try to draw blue!';
DrawFormattedText(video.window, line, 'center', video.screenYpixels * 0.20, black);
spaceline = sprintf('Press [space] to start.'); 
DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.90, black);
% Draw sequence symbols
Screen('DrawDots', video.window, [video.xCenter video.yCenter], 15 , black, [0 0], 2);
Screen('DrawTexture', video.window, shape_tex(1,targetsymbol), [], positions(1,:), 0, [], [], [0,0,0]);
Screen('DrawTexture', video.window, shape_tex(2,targetsymbol), [], positions(1,:), 0, [], [], lbgd);
Screen('DrawTexture', video.window, shape_tex(1,othersymbol), [], positions(2,:), 0, [], [], [0,0,0]);
Screen('DrawTexture', video.window, shape_tex(2,othersymbol), [], positions(2,:), 0, [], [], lbgd);
% Flip and wait
Screen('Flip', video.window);
WaitKeyPress(spaceKey);

%%% loop on trials:
for trial = 1:ntrials
    % Check if abort key is pressed
    if CheckKeyPress(escapeKey)
        Priority(0); FlushEvents; ListenChar(0); ShowCursor;
        error('aborted by ESCAPE');
    end
    % draw alternative
    top = 'Choose a symbol:';
    DrawFormattedText(video.window, top, 'center', video.screenYpixels * 0.25, black);
    targetside  = tutorialsqc(2).symbolsides(trial);
    otherside   = 3 - tutorialsqc(2).symbolsides(trial);
    Screen('DrawDots', video.window, [video.xCenter video.yCenter], 15 , black, [0 0], 2);
    Screen('DrawTexture', video.window, shape_tex(1,targetsymbol), [], positions(targetside,:), 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, shape_tex(2,targetsymbol), [], positions(targetside,:), 0, [], [], lbgd);
    Screen('DrawTexture', video.window, shape_tex(1,othersymbol), [], positions(otherside,:) , 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, shape_tex(2,othersymbol), [], positions(otherside,:) , 0, [], [], lbgd);
    %DrawFormattedText(video.window, '[a]                      [p]', 'center', video.screenYpixels * 0.70, white);
    Screen('Flip', video.window, vbl+fx_roundfp(0.1,0,video.ifi));
    vbl = GetSecs;
    
    % wait for choice & show outcome 4''
    while true
        [keyIsDown,secs,keylist] = KbCheck();
        if keyIsDown && sum(keylist) == 1 && (keylist(leftKey) == 1 || keylist(rightKey) == 1)
            %rt = secs - vbl;
            choice = find(respKeys == (find(keylist==1))); break
        end
    end
    top = sprintf('The computer reveals the colour drawn:');
    DrawFormattedText(video.window, top, 'center', video.screenYpixels * 0.25, black);
    if choice == targetside
        samplecolor = tutorialsqc(2).colors.target(trial,:);
        Screen('DrawDots', video.window, [video.xCenter video.yCenter], 15 , black, [0 0], 2);
        Screen('DrawTexture', video.window, shape_tex(1,targetsymbol), [], positions(targetside,:), 0, [], [], [0,0,0]);
        Screen('DrawTexture', video.window, shape_tex(2,targetsymbol), [], positions(targetside,:), 0, [], [], samplecolor);
    elseif choice == otherside
        samplecolor = tutorialsqc(2).colors.other(trial,:);
        Screen('DrawDots', video.window, [video.xCenter video.yCenter], 15 , black, [0 0], 2);
        Screen('DrawTexture', video.window, shape_tex(1,othersymbol), [], positions(otherside,:) , 0, [], [], [0,0,0]);
        Screen('DrawTexture', video.window, shape_tex(2,othersymbol), [], positions(otherside,:) , 0, [], [], samplecolor);
    end
    vbl = Screen('Flip', video.window, secs+fx_roundfp(0.1,0,video.ifi));
    WaitSecs(showtime);
end

% end of sequence:
top = sprintf('Well done!');
DrawFormattedText(video.window, top, 'center', video.screenYpixels * 0.25, black);
spaceline = sprintf('Press [space] to learn the second rule.');
DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.90, black);
Screen('Flip', video.window, vbl+fx_roundfp(0.1,0,video.ifi));
WaitSecs(1);
WaitKeyPress(spaceKey);

%% THIRD SEQUENCE: OPEN SAMPLING (ORANGE)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% intro screen:
top1 = sprintf('The second rule is to guess from which colour each symbols is drawing.\n\n');
top2 = 'Try to guess which colour these symbols are drawing from:';
DrawFormattedText(video.window, [top1 top2], 'center', video.screenYpixels * 0.20, black);
spaceline = sprintf('Press [space] to start.'); 
DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.90, black);
targetsymbol = tutorialsqc(3).targetsymbol;
othersymbol = tutorialsqc(3).othersymbol;
Screen('DrawDots', video.window, [video.xCenter video.yCenter], 15 , black, [0 0], 2);
Screen('DrawTexture', video.window, shape_tex(1,targetsymbol), [], positions(1,:), 0, [], [], [0,0,0]);
Screen('DrawTexture', video.window, shape_tex(2,targetsymbol), [], positions(1,:), 0, [], [], lbgd);
Screen('DrawTexture', video.window, shape_tex(1,othersymbol), [], positions(2,:), 0, [], [], [0,0,0]);
Screen('DrawTexture', video.window, shape_tex(2,othersymbol), [], positions(2,:), 0, [], [], lbgd);
Screen('Flip', video.window);
WaitSecs(2);
WaitKeyPress(spaceKey);

% warning screen:
attention = '/ WATCH OUT \\\n\n';
l1 = '(1) the symbols can draw:';
opt1 = ' - both mainly blue,\n';
opt2 = ' - both mainly orange,\n';
opt3 = ' - or one mainly blue and the other one mainly orange!\n';
l2 = sprintf('It is not possible to guess the colour of one from the other :)\n\n\n\n');
DrawFormattedText(video.window, [attention l1], 'center', video.screenYpixels * 0.20, black);
DrawFormattedText(video.window, [opt1 opt2 opt3], video.screenXpixels * 0.35, video.screenYpixels * 0.35, black);
DrawFormattedText(video.window, l2, 'center', video.screenYpixels * 0.50, black);
l3 = sprintf('(2) Symbols can also change colours *between* sequences:\n');
l4 = sprintf('so the moon can sometimes obtain mainly orange draws, \nthen mainly blue draws in the next sequence.');
DrawFormattedText(video.window, [l3 l4], 'center', video.screenYpixels * 0.7, black);
spaceline = sprintf('Press [space] to start.'); 
DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.90, black);
vbl = Screen('Flip', video.window);
WaitSecs(2);
WaitKeyPress(spaceKey);

% instructions again:
line = 'These are the symbols, try to guess which colour they draw from!';
DrawFormattedText(video.window, line, 'center', video.screenYpixels * 0.25, black);
spaceline = sprintf('Press [space] to start.'); 
DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.90, black);
Screen('DrawDots', video.window, [video.xCenter video.yCenter], 15 , black, [0 0], 2);
Screen('DrawTexture', video.window, shape_tex(1,targetsymbol), [], positions(1,:), 0, [], [], [0,0,0]);
Screen('DrawTexture', video.window, shape_tex(2,targetsymbol), [], positions(1,:), 0, [], [], lbgd);
Screen('DrawTexture', video.window, shape_tex(1,othersymbol), [], positions(2,:), 0, [], [], [0,0,0]);
Screen('DrawTexture', video.window, shape_tex(2,othersymbol), [], positions(2,:), 0, [], [], lbgd);
Screen('Flip', video.window);
WaitKeyPress(spaceKey);

% loop on trials:
for trial = 1:ntrials
    % Check if abort key is pressed
    if CheckKeyPress(escapeKey)
        Priority(0); FlushEvents; ListenChar(0); ShowCursor;
        error('aborted by ESCAPE');
    end
    % draw alternative
    top = 'Choose a symbol:';
    DrawFormattedText(video.window, top, 'center', video.screenYpixels * 0.25, black);
    targetside  = tutorialsqc(3).symbolsides(trial);
    otherside   = 3 - tutorialsqc(3).symbolsides(trial);
    Screen('DrawDots', video.window, [video.xCenter video.yCenter], 15 , black, [0 0], 2);
    Screen('DrawTexture', video.window, shape_tex(1,targetsymbol), [], positions(targetside,:), 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, shape_tex(2,targetsymbol), [], positions(targetside,:), 0, [], [], lbgd);
    Screen('DrawTexture', video.window, shape_tex(1,othersymbol), [], positions(otherside,:) , 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, shape_tex(2,othersymbol), [], positions(otherside,:) , 0, [], [], lbgd);
    vbl = Screen('Flip', video.window, vbl);
    
    % wait for choice & show outcome 4''
    while true
        [keyIsDown,secs,keylist] = KbCheck();
        if keyIsDown && sum(keylist) == 1 && (keylist(leftKey) == 1 || keylist(rightKey) == 1)
            %rt = secs - vbl;
            choice = find(respKeys == (find(keylist==1))); break
        end
    end
    top = sprintf('The computer reveals the colour drawn:');
    DrawFormattedText(video.window, top, 'center', video.screenYpixels * 0.25, black);
    if choice == targetside
        samplecolor = tutorialsqc(3).colors.target(trial,:);
        Screen('DrawDots', video.window, [video.xCenter video.yCenter], 15 , black, [0 0], 2);
        Screen('DrawTexture', video.window, shape_tex(1,targetsymbol), [], positions(targetside,:), 0, [], [], [0,0,0]);
        Screen('DrawTexture', video.window, shape_tex(2,targetsymbol), [], positions(targetside,:), 0, [], [], samplecolor);
    elseif choice == otherside
        samplecolor = tutorialsqc(3).colors.other(trial,:);
        Screen('DrawDots', video.window, [video.xCenter video.yCenter], 15 , black, [0 0], 2);
        Screen('DrawTexture', video.window, shape_tex(1,othersymbol), [], positions(otherside,:) , 0, [], [], [0,0,0]);
        Screen('DrawTexture', video.window, shape_tex(2,othersymbol), [], positions(otherside,:) , 0, [], [], samplecolor);
    end
    vbl = Screen('Flip', video.window, secs+fx_roundfp(0,0,video.ifi)); %flip ASAP after response
    WaitSecs(showtime); % show it for showtime
    
    
end

% final probe: circle is orange is target
blueside = 1;
orangeside = 2;
top = 'In this rule, the computer will stop at random and ask you about one of the symbols:\n\n';
line = sprintf('In this sequence, from which colour was this symbol mainly drawing from?');
DrawFormattedText(video.window,  [top line], 'center', video.screenYpixels*0.15, black);
Screen('DrawTexture', video.window, shape_tex(1, targetsymbol), [], centerpos, 0, [], [], [0,0,0]);
Screen('DrawTexture', video.window, shape_tex(2, targetsymbol), [], centerpos, 0, [], [], video.lbgd);
Screen('DrawTexture', video.window, rep_tex(1, orangeside), [], rep_pos(orangeside,:), 0, [], [], [0,0,0]);
Screen('DrawTexture', video.window, rep_tex(2, orangeside), [], rep_pos(orangeside,:), 0, [], [], orange);
Screen('DrawTexture', video.window, rep_tex(1, blueside), [], rep_pos(blueside,:), 0, [], [], [0,0,0]);
Screen('DrawTexture', video.window, rep_tex(2, blueside), [], rep_pos(blueside,:), 0, [], [], blue);
Screen('DrawingFinished', video.window);
vbl = Screen('Flip', video.window, vbl+fx_roundfp(0,0,video.ifi));
while true
    [keyIsDown,secs,keylist] = KbCheck();
    if keyIsDown && (keylist(leftKey) == 1 ||  keylist(rightKey) == 1) && sum(keylist) == 1
        respprobe = find(respKeys == (find(keylist==1)));
        %rtprobe  = secs - vbl;
        break
    end
end
% selection and feedback
line = sprintf('In this sequence, from which colour was this symbol mainly drawing from?');
Screen('DrawTexture', video.window, shape_tex(1,targetsymbol), [], centerpos, 0, [], [], [0,0,0]);
Screen('DrawTexture', video.window, shape_tex(2,targetsymbol), [], centerpos, 0, [], [], video.lbgd);
DrawFormattedText(video.window,  line, 'center', video.screenYpixels*0.20, black);
Screen('DrawTexture', video.window, rep_tex(1, orangeside), [], rep_pos(orangeside,:), 0, [], [], [0,0,0]);
Screen('DrawTexture', video.window, rep_tex(2, orangeside), [], rep_pos(orangeside,:), 0, [], [], orange);
Screen('DrawTexture', video.window, rep_tex(1, blueside), [], rep_pos(blueside,:), 0, [], [], [0,0,0]);
Screen('DrawTexture', video.window, rep_tex(2, blueside), [], rep_pos(blueside,:), 0, [], [], blue);
Screen('FrameRect', video.window, black, selection_rect(respprobe,:), 6);
Screen('DrawingFinished', video.window);
vbl = Screen('Flip', video.window, secs+fx_roundfp(0,0,video.ifi)); %show ASAP after selection
nextfliptime = vbl+fx_roundfp(0.4,0,video.ifi); % show selection for 0.4''

% color feedback on final probe: 0.8secs (tutorial => IRL will be 0.5)
tcol = orange;
Screen('DrawTexture', video.window, shape_tex(1,targetsymbol), [], centerpos, 0, [], [], [0,0,0]);
Screen('DrawTexture', video.window, shape_tex(2,targetsymbol), [], centerpos, 0, [], [], tcol);
DrawFormattedText(video.window,  line, 'center', video.screenYpixels*0.20, black);
Screen('DrawTexture', video.window, rep_tex(1,orangeside), [], rep_pos(orangeside,:), 0, [], [], [0,0,0]);
Screen('DrawTexture', video.window, rep_tex(2,orangeside), [], rep_pos(orangeside,:), 0, [], [], orange);
Screen('DrawTexture', video.window, rep_tex(1,blueside), [], rep_pos(blueside,:), 0, [], [], [0,0,0]);
Screen('DrawTexture', video.window, rep_tex(2,blueside), [], rep_pos(blueside,:), 0, [], [], blue);
Screen('FrameRect', video.window, black, selection_rect(respprobe,:), 6);
Screen('DrawingFinished', video.window);
vbl = Screen('Flip', video.window, nextfliptime);
nextfliptime = vbl+fx_roundfp(0.8,0,video.ifi); % show feedback for 0.8

% end of sequence:
top = sprintf('Well done, now you know both rules!');
DrawFormattedText(video.window, top, 'center', video.screenYpixels * 0.25, black);
spaceline = sprintf('Press [space] to continue.');
DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.90, black);
Screen('Flip', video.window, nextfliptime);
WaitSecs(1);
WaitKeyPress(spaceKey);

%% FOURTH SEQUENCE: REALTIME + EYETRACKING INSTRUCTIONS: TARGET (ORANGE)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% intro screens:
top1 = sprintf('During the experiment, the colours will be flashed more rapidly,\n');
top2 = 'in addition, we will be recording your pupil information, so:\n';
top3 = '(1) please do not move your head: rest it on the chinrest,\n';
top4 = '(2) please do not move your eyes: stay on the fixation point,\n';
top5 = sprintf('    symbols are close enough to still see the colours while fixating the point,\n');
top6 = sprintf('(3) please do not blink:\n    if necessary, do it right before pressing a button to choose.\n');
spaceline = sprintf('Press [space] to start.'); 
DrawFormattedText(video.window, [top1 top2], 'center', video.screenYpixels * 0.20, black);
DrawFormattedText(video.window, [top3 top4 top5 top6], video.screenXpixels * 0.10, video.screenYpixels * 0.4, black);
DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.90, black);
Screen('Flip', video.window);
WaitSecs(2);
WaitKeyPress(spaceKey);

% instructions:
targetsymbol = tutorialsqc(4).targetsymbol;
othersymbol = tutorialsqc(4).othersymbol;
top = 'These are the symbols, try to draw orange!';
DrawFormattedText(video.window, top, 'center', video.screenYpixels * 0.2, black);
Screen('DrawDots', video.window, [video.xCenter video.yCenter], 15 , black, [0 0], 2);
Screen('DrawTexture', video.window, shape_tex(1,targetsymbol), [], positions(1,:), 0, [], [], [0,0,0]);
Screen('DrawTexture', video.window, shape_tex(2,targetsymbol), [], positions(1,:), 0, [], [], lbgd);
Screen('DrawTexture', video.window, shape_tex(1,othersymbol), [], positions(2,:), 0, [], [], [0,0,0]);
Screen('DrawTexture', video.window, shape_tex(2,othersymbol), [], positions(2,:), 0, [], [], lbgd);
spaceline = sprintf('Press [space] to start.');
DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.90, black);
vbl = Screen('Flip', video.window);
WaitKeyPress(spaceKey);

%%% loop on 7 trials:
for trial = 1:ntrials
    % Check if abort key is pressed
    if CheckKeyPress(escapeKey)
        Priority(0); FlushEvents; ListenChar(0); ShowCursor;
        error('aborted by ESCAPE');
    end
    
    % draw alternative
    targetside  = tutorialsqc(4).symbolsides(trial);
    otherside   = 3 - tutorialsqc(4).symbolsides(trial);
    Screen('DrawDots', video.window, [video.xCenter video.yCenter], 15 , black, [0 0], 2);
    Screen('DrawTexture', video.window, shape_tex(1,targetsymbol), [], positions(targetside,:), 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, shape_tex(2,targetsymbol), [], positions(targetside,:), 0, [], [], lbgd);
    Screen('DrawTexture', video.window, shape_tex(1,othersymbol), [], positions(otherside,:) , 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, shape_tex(2,othersymbol), [], positions(otherside,:) , 0, [], [], lbgd);
    vbl = Screen('Flip', video.window, vbl+fx_roundfp(0.1,0,video.ifi));
    
    % wait for choice & show outcome 
    while true
        [keyIsDown,secs,keylist] = KbCheck();
        if keyIsDown && sum(keylist) == 1 && (keylist(leftKey) == 1 || keylist(rightKey) == 1)
            %rt = secs - vbl;
            choice = find(respKeys == (find(keylist==1))); break
        end
    end
    if choice == targetside
        samplecolor = tutorialsqc(4).colors.target(trial,:);
        Screen('DrawDots', video.window, [video.xCenter video.yCenter], 15 , black, [0 0], 2);
        Screen('DrawTexture', video.window, shape_tex(1,targetsymbol), [], positions(targetside,:), 0, [], [], [0,0,0]);
        Screen('DrawTexture', video.window, shape_tex(2,targetsymbol), [], positions(targetside,:), 0, [], [], samplecolor);
    elseif choice == otherside
        samplecolor = tutorialsqc(4).colors.other(trial,:);
        Screen('DrawDots', video.window, [video.xCenter video.yCenter], 15 , black, [0 0], 2);
        Screen('DrawTexture', video.window, shape_tex(1,othersymbol), [], positions(otherside,:) , 0, [], [], [0,0,0]);
        Screen('DrawTexture', video.window, shape_tex(2,othersymbol), [], positions(otherside,:) , 0, [], [], samplecolor);
    end
    vbl = Screen('Flip', video.window, secs+fx_roundfp(0.1,0,video.ifi));
    WaitSecs(0.5);
    
    % Draw fixation point for 0.5 secs
    Screen('DrawDots', video.window, [video.xCenter video.yCenter], 15 , black, [0 0], 2);
    vbl = Screen('Flip', video.window, vbl+fx_roundfp(0.1,0,video.ifi));
    WaitSecs(0.5);
    
end

%%% show score cups progressively
top = sprintf('Well done!\n\nScore:\n');
DrawFormattedText(video.window, top, 'center', video.screenYpixels * 0.25, black);
spaceline = sprintf('If you understood the instructions,\nyou are ready for training and for eyetracker familiarisation!');
DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.80, black);
Screen('FrameRect', video.window, black, cupframe_rect, 4);
Screen('Flip', video.window);
WaitSecs(0.3);
DrawFormattedText(video.window, top, 'center', video.screenYpixels * 0.25, black);
DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.80, black);
Screen('FrameRect', video.window, black, cupframe_rect, 4);
Screen('DrawTexture', video.window, cuptext, [], cuppos(1,:));
Screen('Flip', video.window);
WaitSecs(0.3);
DrawFormattedText(video.window, top, 'center', video.screenYpixels * 0.25, black);
DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.80, black);
Screen('FrameRect', video.window, black, cupframe_rect, 4);
Screen('DrawTexture', video.window, cuptext, [], cuppos(1,:));
Screen('DrawTexture', video.window, cuptext, [], cuppos(2,:));
Screen('Flip', video.window);
WaitSecs(0.3);
DrawFormattedText(video.window, top, 'center', video.screenYpixels * 0.25, black);
DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.80, black);
Screen('FrameRect', video.window, black, cupframe_rect, 4);
Screen('DrawTexture', video.window, cuptext, [], cuppos(1,:));
Screen('DrawTexture', video.window, cuptext, [], cuppos(2,:));
Screen('DrawTexture', video.window, cuptext, [], cuppos(3,:));
Screen('Flip', video.window);
WaitSecs(1);
WaitKeyPress(spaceKey);

sca;

%%

header.end = datestr(now,'yyyymmdd-HHMM');
header.aborted = 0;
tutorialout.header = header;


catch ME
    
    header.aborted = 1;
    tutorialout.header = header;
    tutorialout.ME = ME;
    rethrow(ME)
    
end % try/catch

end % function definition
