%%% running the whole colexp experiment from runner:
%%% NB: to be able to run calibration from PTB PC native keyboard is not inhibited
%%% don't press keys
%% clearing + adding Valentin's IO toolbox
close all
clear all java
close all hidden
clc
addpath ./Toolboxes/IO % Valentin's toolbox in Experiment's path
addpath ./img

%% options:
testing = 1;
eyeON   = 0;

%% create data folder for subject

% where to save?
while true
    % query subj number in command line and makeup dir name:
    subj = input('>>>  Input participant nb: ');
    if ~isscalar(subj) || mod(subj,1) ~= 0
        error('Invalid subject number!');
    end
    subjdir = sprintf('../Data/S%02d',subj);
    
    % check if dir already exists, if not, create it:
    if ~exist(subjdir,'dir')
        mkdir(subjdir);
        break;
    elseif exist(subjdir,'dir')
    % if dir exists, either overwrite or input another subj name:    
        overwrite = input('>>>  subjdir already exists, overwite(1) or input new subj nb(0): ');
        if overwrite
            mkdir(subjdir);
            break %overwrite
        end
    end
end
clearvars overwrite 

diary(sprintf('../Data/S%02d/logS%02d_%s.txt', subj, subj, datestr(now,'mmdd_HHMM')));

fprintf('Saving into %s\n', subjdir);

%% run tutorial:

fprintf('...Launching tutorial now \n');
tutorialout = COLEXP_expe_fx_run_tutorial(testing);
save(sprintf('../Data/S%02d/tutorialout_runner_%02d.mat', subj, subj), 'tutorialout');
if tutorialout.header.aborted == 1; error('error in tutorial'); end

fprintf('...Finished tutorial, went from %s to %s ... \n', tutorialout.header.start, tutorialout.header.end);

fprintf('>>> Waiting for [t] press... \n');
WaitKeyPress(KbName('t'));

%% run training:
load('training_sqc.mat');

fprintf('...Launching training now \n');
trainingout = COLEXP_expe_fx_run_training(training_sqc, subj, testing, eyeON);
save(sprintf('%s/trout_S%02d_runner.mat', subjdir, subj), 'trainingout');
 
if trainingout.header.aborted == 1; error('error in training'); end
fprintf('...Finished training, went from %s to %s ... \n', trainingout.header.start, trainingout.header.end);

fprintf('>>> Waiting for [t] press... \n');
WaitKeyPress(KbName('t'));

%% run experiment

% Generate or load runs?
generate = input('>>>  Generate runs & blocks for experiment? 1=yes anythingelse=no ');
if generate == 1
    fprintf('...Generating sequences for participant # %i...\n', subj)
    sqc = COLEXP_expe_fx_gen_expe_sqc(subj, 96, 1/3);
    save(sprintf('%s/sqc_S%02d.mat', subjdir, subj), 'sqc');
else
    fprintf('...Loading existing runs from ''%s/sqc_S%02d.mat''...\n', subjdir, subj)
    load(sprintf('%s/sqc_S%02d.mat', subjdir, subj),'sqc');
end
clearvars generate

% Launching real experiment runs:
fprintf('...Launching experiment now... \n');
expout = COLEXP_expe_fx_run_expe(sqc, subj, testing, eyeON);
save(sprintf('%s/expout_S%02d_runner.mat', subjdir, subj), 'expout');
if expout.header.aborted == 1; error('error in experience'); end

fprintf('FINISHED EXPERIMENT, went from %s to %s ... \n', expout.header.start, expout.header.end);
diary off;

