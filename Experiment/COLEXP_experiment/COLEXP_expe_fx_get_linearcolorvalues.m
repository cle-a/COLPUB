function [bestcolorvalues] = COLEXP_expe_fx_get_linearcolorvalues(nsampleslist, counterfnesslist, targetcolorslist, perr)
%% returns color values for runs of condition (1):
% % % % % % % % % %
% % % % % % % % % %     takes:
% % % % % % % % % %     - nsampleslist: a list of all the lengths of all the runs,
% % % % % % % % % %                     in the shape [8 8 8 8 8 8 8 8 8 8 8 8 12 12 12 12.. etc]
% % % % % % % % % %     - counterfnesslist: list of all counterfness conditions of all runs
% % % % % % % % % %                        1 = good / good
% % % % % % % % % %                        2 = bad / bad 
% % % % % % % % % %                        3 = good / bad
% % % % % % % % % %                     in the shape [1 1 1 1 2 2 2 2 3 3 3 3 1 1 1 1 2 2 2 2 etc..]
% % % % % % % % % %     - targetcolorslist: list of all targetcolors 
% % % % % % % % % %                     in the shape [1 2 1 2 1 2 1 2etc..]
% % % % % % % % % %     - % perr : probability of misclassification of the form based on a single sample/color (here 1/3)
% % % % % % % % % %
% % % % % % % % % %     returns:
% % % % % % % % % %     bestcolorvalues: structure of same length (= length(nsampleslist)),
% % % % % % % % % %     with fields:
% % % % % % % % % %                   - target: the color draws for target shape of the run
% % % % % % % % % %                   - other: color draws for the other shape of this run
% % % % % % % % % %                   - nsamples: n of samples in run (=numel(other))
% % % % % % % % % %                   - counterfness: cf condition in run
% % % % % % % % % %                   - targetcolor: target color in run
% % % % % % % % % %
% % % % % % % % % %     NB: the order of fields couterfness/targetcolors matters => otherwise won't be paired well
%
% (1) defines the probability distribution of the color
%       // NB: draws from the "high prob on 0-1" distribution => so the orange
%       and flips at the end for the blue target condition \\
% (2) for each length-counterfness combination:
%       - get ndraws of length-many colors
%       - run nmodelsimulations of the sampling sequences on this color draw
%       - sort the color draws by (a) final amount of info accumulated, and (b) similarity to the original underlying color distribution
%         NB: "difficulty" is the median "amount of information" accumulated for the options abs()
%       - store as many draws as we have sequences of this length-counterfness combination
%         flipped when the target was blue.
% // NB: RETURNS COLORS ON -1/+1 CONTINUUM => TRANSFORM TO ISOLUMINANT RGB AFTERWARDS \\
%

%% parameters:

%init:
lengthxcf        = unique([nsampleslist' counterfnesslist'],'rows'); % for each nsamples*CF condition
bestcolorvalues  = struct;

% model parameters:
alpha = 1;
sigma = 0.5;

% probability distribution of the colors: here p1 is high prob on the 0/+1 range = so rather orange
x     = -1:0.002:+1;
x     = 0.5*(x(1:end-1)+x(2:end)); % all color values: -1 to +1
b     = fzero(@(p)trapz(x(x > 0),1./(1+exp(x(x > 0)*p))/trapz(x,1./(1+exp(x*p))))-perr,[0,1000]);
% computes ratio between area under the curve after 0 / total area under the curve - perr
% leads ratio to 1 to get the beta that leads the curve to have exactly perr area under the curve before zero
p1    = 1./(1+exp(-x*b)); % probabilities of each color (x) for shape1
p1    = p1/trapz(x,p1); % divided by sum of probs => 0 to 1

%% I// draw colors & simulate discriminability/difficulty based on model learning => nsim times:
stored = 0;
  
all = struct();
for ilengthxcf = 1:size(lengthxcf,1)
        
    nsamples        = lengthxcf(ilengthxcf, 1);
    counterfness    = lengthxcf(ilengthxcf, 2);
    
    ncolsim         = 1e2; % nb of color draws simulations
    nmodsim         = 1e3; % nb of model simulations to run on each color draw
    colorvalues     = nan(ncolsim, nsamples, 2);
    drawncolval     = nan(2, nsamples);
    dval            = nan(ncolsim, nmodsim, nsamples, 3); % decision values to compute ranks on
    colrnk          = nan(ncolsim, nsamples);
    colors          = nan(ncolsim, nsamples);

    for icolsim = 1:ncolsim
        if mod(icolsim,250) == 0; fprintf('...\n'); end
        
        % init for current color simulation:
        samplings = zeros(1, nsamples, 3); % sampling response symbol (which form to look at)
        learntval = nan(2, nsamples+1, 3); % expected/infered/learnt color of each sym
        seenval   = nan(2, nsamples, 3); % actual colors values *seen* by model: drawn(sampled)
        
        %%% (a) draw colvals for this round of simulations depending on condition:
        colors(icolsim, :) = itrnd([1,nsamples],x,p1,'exact');
        switch counterfness
            case 1; drawncolval(1,:) = colors(icolsim, :);        drawncolval(2,:) = drawncolval(1,:); % goodgood
            case 2; drawncolval(1,:) = (-1) * colors(icolsim, :); drawncolval(2,:) = drawncolval(1,:); % badbad
            case 3; drawncolval(1,:) = colors(icolsim, :);        drawncolval(2,:) = (-1) * drawncolval(1,:); %goodbad
        end
        colorvalues(icolsim, :, :) = drawncolval';
        
        %%% (b) run nmodsim model simulations on each color draw simulation:
        for imodsim = 1:nmodsim          
            for ismp = 1:nsamples
                
                %%% choose sym to sample from
                if ismp == 1 % if first one, choose randomly
                    ro = randi(2,1);
                    tr = ro;
                    samplings(:,ismp,:) = ro; % for row one = random but same
                    learntval(:,ismp,:) = 0;
                    dval(icolsim, imodsim, ismp, :) = 0;
                elseif ismp > 1
                    % target: seeks orange:
                    [trv, tr] = max(learntval(:,ismp,1),[],1); % orange = 1 = max
                    samplings(:,ismp,1) = tr;
                    dval(icolsim, imodsim, ismp, 1) = trv;
                    % open: symbol with least information
                    [rov, ro] = min(abs(learntval(:,ismp,2)),[],1); % min abs learnt value
                    samplings(:,ismp,2) = ro;
                    dval(icolsim, imodsim, ismp, 2) = rov;
                end%choosing which symbol to sample from

                %%% learn = accumulate color values:
                seenval(tr, ismp,1) = drawncolval(tr, ismp);
                seenval(ro, ismp,2) = drawncolval(ro, ismp);
                
                learntval(tr,ismp+1,1) = alpha * learntval(tr,ismp,1) + ((sigma * randn(1)) + seenval(tr,ismp,1));
                learntval(ro,ismp+1,2) = alpha * learntval(ro,ismp,2) + ((sigma * randn(1)) + seenval(ro,ismp,2));
                
                learntval(3-tr,ismp+1,1) = learntval(3-tr,ismp,1);
                learntval(3-ro,ismp+1,2) = learntval(3-ro,ismp,2);  
            end%for each sample, choose and learn
        end%for imodsim: model simulations on this color draw
        
        %%% for each colsim, take median value learnt by models for each sample:
        colrnk(icolsim,:,1) = median(dval(icolsim,:,:,1),2);
        colrnk(icolsim,:,2) = median(dval(icolsim,:,:,2),2);          
        
    end%for each color draw simulation
    
    %%% (c) rank simulations & sum ranks:
    arnk = nan(ncolsim,nsamples,2);
    for ismp = 1:nsamples % at each sample 
        arnk(:,ismp,1) = tiedrank( abs ( colrnk(:,ismp,1) - mean(colrnk(:,ismp,1)) )); % difficulty rank in the target condition
        arnk(:,ismp,2) = tiedrank( abs ( colrnk(:,ismp,2) - mean(colrnk(:,ismp,2)) )); % difficulty rank in the open condition
    end
    srnk  = squeeze(sum(arnk,2)); % for each colsim, sum of the ranks of all the samples
    all(ilengthxcf).arnk = arnk;
    all(ilengthxcf).srnk = srnk;
    all(ilengthxcf).colrnk = colrnk;
    
    %%% (d) add constraint on color distribution for each block:
    mucol = tiedrank( abs (  mean(colors,2) - mean(mean(colors,2)) ) ) ;
    sdcol = tiedrank( abs (  std(colors,0,2) - mean(std(colors,0,2)) ) ) ;
    scrnk = (mucol + sdcol);
    all(ilengthxcf).scrnk = scrnk;
    
    %%% (e) pick color series whose sum of ranks is lowest & store them
    npck  = numel(nsampleslist) / size(lengthxcf,1);
    [~,i] = sort(tiedrank(srnk(:,1)) + tiedrank(srnk(:,2)) + tiedrank(scrnk)); % sort sims by sum of ranks summed across smp and conditions
    pk = i(1:npck);
    all(ilengthxcf).colors = colorvalues(pk, :, :);
    
    %%% (f) store selected color values and conditions
    for ist = 1:npck % store final colvals
        stored = stored +1;
        if targetcolorslist(stored) == 1
            bestcolorvalues(stored).target = colorvalues(pk(ist),:,1) .* (-1);
            bestcolorvalues(stored).other  = colorvalues(pk(ist),:,2) .* (-1);
        elseif targetcolorslist(stored) == 2
            bestcolorvalues(stored).target  = colorvalues(pk(ist),:,1);
            bestcolorvalues(stored).other   = colorvalues(pk(ist),:,2);
        end
        bestcolorvalues(stored).nsamples = nsamples;
        bestcolorvalues(stored).counterfness = counterfness;
        bestcolorvalues(stored).targetcolor = targetcolorslist(stored);
    end

fprintf('...done with length %d & cf %d...\n', nsamples, counterfness);
end% for each length x cf condition

fprintf('Done choosing color values!\n')


end%function def


