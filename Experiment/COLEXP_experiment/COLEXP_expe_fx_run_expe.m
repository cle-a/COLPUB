%%% run experiment
%%% NB: native KB keys are not inhibited: don't press a | p keys!

% requires PTB-3
% requires Valentins IO Toolbox in the Experiment dir
% requires an sqc structure

% sqc         = the experiment structure
% subj        = participant nb
% testing     = debug (1) | real expe (O)
% eyeinputpar = whether or not to record eyelink: 0 | 1 

% iblock    = block idx in nb of blocks
% iseq      = run idx in nb of run in this block == blkseq
% isqc      = sqc idx out of total nb of sqc in the sqc struct
% ismp      = sample idx in nb of samples in this run
% isamples  = sample idx out of total nb of samples



function expout = COLEXP_expe_fx_run_expe(sqc, subj, testing, eyeinputpar)

try
    
    %% Add IO toolbox, init subj & set running parameters
    
    addpath ./Toolboxes/IO % Valentin's toolbox in Experiment's path
    
    subjdir = sprintf('../Data/S%02d',subj);
    
    % create output structures:
    header.subj = subj;
    header.start = datestr(now,'yyyymmdd-HHMM');
    header.aborted = 1; % changed to 0 if completes
    sampling = struct();
    final = struct();
    ME = struct();
    
    video.testingON = testing;
    video.synchflip = true;
    video.eyelink.eyeON = eyeinputpar;
    video.mouseON = false;
    video.eyelink.inputphys_mm  = [660 337];
    video.eyelink.inputscreendistance_mm = 550;
    if ispc % in 2nd floor booths/downstairs
        video.screenwdth_cm = 66;
        video.screenwdth_px = 1920;
        video.ppd = round(tand(1) * video.eyelink.inputscreendistance_mm / 10/ video.screenwdth_cm * video.screenwdth_px);
        %  where:
        %    dist_cm - screen-eye distance in centimeters
        %    wdth_cm - screen width in centimeters
        %    wdth_px - screen width in pixels (native resolution)
    elseif ismac % testing non mac, doesnt care.
        video.ppd = 40; % number of screen pixels per degree of visual angle
    end
    
    
    % PTB parameters:
    
    %%% Set visual parameters
    video.shape_sz          = 6*video.ppd; % video.ppd pixels per degree of visual angle = 40
    video.shape_offset      = 4.5*video.ppd; % shape offset
    video.respbutton_offset = 10*video.ppd; % response button offset
    video.rep_sz            = 5*video.ppd;
    video.fb_sz             = 6*video.ppd; % selection square size
    video.cup_sz            = 6*video.ppd;
    black                   = [0,0,0];
    white                   = [1,1,1];
    load('./color_space.mat', 'lbgd');
    video.lbgd              = lbgd;
    orange                  = [1,0.5,0];
    blue                    = [0,0.5,1];
    
    %%% which screens?
    screens = Screen('Screens');
    video.screen = max(screens); %screen index on PC: 0=extended, 1=native, 2=ext
    
    %%% set synch properties:
    if video.synchflip && ispc
        % set screen synchronization properties -- workaround Valentin for PC
        % see 'help SyncTrouble',
        %     'help BeampositionQueries' or
        %     'help ConserveVRAMSettings' for more information
        Screen('Preference','VisualDebuglevel',3); % verbosity
        Screen('Preference','SyncTestSettings',[],[],0.2,10); % soften synchronization test requirements
        Screen('Preference','ConserveVRAM',bitor(4096,Screen('Preference','ConserveVRAM'))); % enforce beamposition workaround for missing VBL interval
        fprintf('Synching flips with softer requirements...\n');
    elseif ~video.synchflip || ismac
        % skip synchronization tests altogether
        % //!\\ force skipping tests, PTB wont work = timing inaccurate
        Screen('Preference','SkipSyncTests',2); % assumes 60Hz etc..
        Screen('Preference','VisualDebuglevel',0);
        Screen('Preference','SuppressAllWarnings',1);
        fprintf('||| SYNCHFLIP OFF or running on OSX => TIMINGS WILL BE INACCURATE! |||\n')
    end
    
    %%% open main window:
    PsychImaging('PrepareConfiguration');
    PsychImaging('AddTask','General','UseFastOffscreenWindows');
    PsychImaging('AddTask','General','NormalizedHighresColorRange');
    video.res = Screen('Resolution',video.screen);
    [video.window, video.windowRect] = PsychImaging('OpenWindow',video.screen,video.lbgd);
    fprintf('passed OpenWindow!\n')
    
    [video.screenXpixels, video.screenYpixels] = Screen('WindowSize', video.window);
    [video.xCenter, video.yCenter] = RectCenter(video.windowRect);
    Screen('BlendFunction', video.window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');%smooth blending/antialiasing
    video.blend = '''GL_SRC_ALPHA'', ''GL_ONE_MINUS_SRC_ALPHA''';
    video.colorrange = 1;
    Screen('ColorRange',video.window,video.colorrange);
    % 1 = to pass color values in OpenGL's native floating point color range of 0.0 to
    % 1.0: This has two advantages: First, your color values are independent of
    % display device depth, i.e. no need to rewrite your code when running it on
    % higher resolution hardware. Second, PTB can skip any color range remapping
    % operations - this can speed up drawing significantly in some cases.   ...???
    video.textsize = 50;
    Screen('TextSize', video.window, video.textsize);
    video.ifi = Screen('GetFlipInterval', video.window);
    video.priority = Priority(MaxPriority(video.window));
    vbl = Screen('Flip', video.window); % first flip
    nextfliptime = vbl+fx_roundfp(0.01,0,video.ifi); % wait 10ms
    
    %% Make textures:
    
    %%% symbols textures:
    shape_tex = zeros(2,8); % shape_tex(1,i)=black (contour) shape_tex(2,i)=grey (inside)
    for sh = 1:8
        outline         = double(imread(sprintf('./img/shape%dc.png',sh)))/255;
        outline         = imresize(outline,video.shape_sz/size(outline,1));
        shape_tex(1,sh) = Screen('MakeTexture',video.window,cat(3,ones(size(outline)),outline),[],[],2);
        inside          = double(imread(sprintf('./img/shape%d.png',sh)))/255;
        inside          = imresize(inside,video.shape_sz/size(inside,1));
        shape_tex(2,sh) = Screen('MakeTexture', video.window, cat(3,ones(size(inside)),inside),[],[],2);
    end
    positions(1,:)  = CenterRectOnPoint(Screen('Rect',shape_tex(1)),video.xCenter-video.shape_offset,video.yCenter);
    positions(2,:)  = CenterRectOnPoint(Screen('Rect',shape_tex(1)),video.xCenter+video.shape_offset,video.yCenter);
    centerpos       = CenterRectOnPoint(Screen('Rect',shape_tex(1)),video.xCenter,video.yCenter);
    
    % resp buttons textures:
    outrep = double(imread('./img/bagout.png'))/255;
    outrep = imresize(outrep,video.rep_sz/size(outrep,1));
    inrep  = double(imread('./img/bagin.png'))/255;
    inrep  = imresize(inrep,video.rep_sz/size(inrep,1));
    rep_tex(1,1) = Screen('MakeTexture',video.window,cat(3,ones(size(outrep)),outrep),[],[],2);
    rep_tex(2,1) = Screen('MakeTexture',video.window,cat(3,ones(size(inrep)),inrep),[],[],2);
    rep_tex(1,2) = Screen('MakeTexture',video.window,cat(3,ones(size(outrep)),flip(outrep,2)),[],[],2);
    rep_tex(2,2) = Screen('MakeTexture',video.window,cat(3,ones(size(inrep)),flip(inrep,2)),[],[],2);
    rep_pos(1,:) = CenterRectOnPoint(Screen('Rect',rep_tex(1)),video.xCenter-video.respbutton_offset,video.screenYpixels*0.80);
    rep_pos(2,:) = CenterRectOnPoint(Screen('Rect',rep_tex(1)),video.xCenter+video.respbutton_offset,video.screenYpixels*0.80);
    
    buttonbuffer        = 0.3*size(outrep,1);
    selsize             = [0 0 1.25*size(outrep,2)+buttonbuffer size(outrep,1)+buttonbuffer];
    selection_rect(1,:) = CenterRectOnPoint(selsize, video.xCenter-video.respbutton_offset,video.screenYpixels*0.80);
    selection_rect(2,:) = CenterRectOnPoint(selsize, video.xCenter+video.respbutton_offset,video.screenYpixels*0.80);
    
    %score feedback: cup textures:
    cupi         = imread('./img/cupimg.png');
    cupi         = imresize(cupi,video.shape_sz/size(cupi,1));
    cuptext      = Screen('MakeTexture',video.window,cupi);
    cuppos(1,:)  = CenterRectOnPoint(Screen('Rect',cuptext),video.xCenter-(video.shape_sz),video.yCenter);
    cuppos(2,:)  = CenterRectOnPoint(Screen('Rect',cuptext),video.xCenter, video.yCenter);
    cuppos(3,:)  = CenterRectOnPoint(Screen('Rect',cuptext),video.xCenter+(video.shape_sz),video.yCenter);
    
    buffer        = 0.3*size(cupi,2);
    cupframe      = [0 0 3*size(cupi,2)+buffer size(cupi,1)+buffer];
    cupframe_rect = CenterRectOnPoint(cupframe, video.xCenter,video.yCenter);    
    
    %% Init keys:
    clear PsychHID; % Force new enumeration of devices.
    clear KbCheck;
    KbName('UnifyKeyNames'); %across OSs
    GetKeyboardIndices();
    
    escapeKey   = KbName('ESCAPE'); %experimenter keys (native Kb)
    continueKey = KbName('y');
    
    spaceKey    = KbName('space'); %participant keys (numpad)
    if ispc; leftKey = KbName('a');
    elseif ismac; leftKey = KbName('q');
    end
    rightKey    = KbName('p');
    
    respKeys     = [leftKey, rightKey];% left=1 right=2
    expeKeys     = [escapeKey, continueKey, leftKey, rightKey, spaceKey];
    allKeys      = [];
    k = RestrictKeysForKbCheck(allKeys);
    
    %% Init EYELINK:
    
    if video.eyelink.eyeON
        % Check the connection is up and running:
        ip = '100.1.1.2';
        if ispc
            system(sprintf('ping %s -n 3', ip));
        elseif ismac
            system(sprintf('ping %s -c 5', ip));
        end
        
        % Initialize Eyelink computer:
        status = Eyelink('Initialize', 'PsychEyelinkDispatchCallback'); % ZERO IF OK
        if status ~= 0; error('could not initialize eye-tracker connection!'); end
        [~,ev] = Eyelink('GetTrackerVersion');
        connected = Eyelink('IsConnected'); %1=ok, 2=broadcast, -1=dummy, 0=none
        fprintf('Connection to %s eye-tracker initialized = %d.\n',ev, connected);
        el = EyelinkInitDefaults(video.window); % initialize a PTB el structure
        
        % UPDATE SET OPTIONS SCREEN:
        % calibration & validation options:
        Eyelink('Command', 'automatic_calibration_pacing = 1000');
        Eyelink('Command', 'randomize_calibration_order = YES');
        Eyelink('Command', 'enable_automatic_calibration = YES'); % force manual accept
        Eyelink('Command', 'select_eye_after_validation = NO');
        Eyelink('Command', 'cal_repeat_first_target = YES');
        Eyelink('Command', 'val_repeat_first_target = YES');
        
        % tracking options:
        %Eyelink('Command', 'search_limits = YES');
        %Eyelink('Command', 'move_limits = YES');
        if video.mouseON; Eyelink('Command', 'aux_mouse_simulation = YES');
        elseif video.mouseON == false; Eyelink('Command', 'aux_mouse_simulation = NO');
        end
        
        % events & data processing options:
        Eyelink('Command','select_parser_configuration = 0'); %this is the cognitive configuration
        
        % UPDATE CAMERA SETUP SCREEN:
        % Tracking mode:
        Eyelink('Command', 'pupil_size_diameter = YES'); % = Valentin does diameters
        Eyelink('Command', 'use_ellipse_fitter = NO'); % = centroid
        Eyelink('Command', 'sample_rate = 1000'); % Valentin does 500..?
        Eyelink('Command', 'elcl_tt_power = 2'); % = 75% = default
        Eyelink('Command', 'active_eye = LEFT'); % in front of camera (less noise)
        video.eyelink.eyeused = 0; % lefteye
        Eyelink('Command','binocular_enabled = NO'); % monoc
        Eyelink('Command','corneal_mode = YES'); % pupilCR
        Eyelink('Command','use_high_speed = YES'); % defines sampling rate... -> redundant
        
        % SCREEN PARAMETERS (physical and pixels):
        Eyelink('Command', 'simulation_screen_distance = %d', video.eyelink.inputscreendistance_mm);
        video.eyelink.pixelcoords = {video.windowRect(1), video.windowRect(2), video.windowRect(3)-1, video.windowRect(4)-1}';
        Eyelink('Command', 'screen_pixel_coords = %d, %d, %d, %d', video.eyelink.pixelcoords{:});
        video.eyelink.physicalscreensize = {-round(video.eyelink.inputphys_mm(1)/2) round(video.eyelink.inputphys_mm(2)/2) round(video.eyelink.inputphys_mm(1)/2) -round(video.eyelink.inputphys_mm(2)/2)}';
        Eyelink('Command', 'screen_phys_coords = %d, %d, %d, %d', video.eyelink.physicalscreensize{:});
        % NB only update calibration parameters after display parameters
        % call calibration type afterwards
        
        % CALIBRATION PARAMETERS VALENTIN:
        el.backgroundcolour = video.lbgd;
        Eyelink('Command','calibration_type = HV5'); % HV5 or HV9
        Eyelink('Command','generate_default_targets = NO'); % YES:default or NO:custom
        cnt = [video.screenXpixels/2,video.screenYpixels/2];
        off = video.ppd*9;
        pnt = zeros(5,2);
        pnt(1,:) = cnt;
        pnt(2,:) = cnt-[0,off];
        pnt(3,:) = cnt+[0,off];
        pnt(4,:) = cnt-[off,0];
        pnt(5,:) = cnt+[off,0];
        pnt = num2cell(reshape(pnt',[],1));
        %calib
        Eyelink('Command','calibration_samples = 6');
        Eyelink('Command','calibration_sequence = 0,1,2,3,4,5');
        Eyelink('Command','calibration_targets = %d,%d %d,%d %d,%d %d,%d %d,%d',pnt{:});
        %valid
        Eyelink('Command','validation_samples = 5');
        Eyelink('Command','validation_sequence = 0,1,2,3,4,5');
        Eyelink('Command','validation_targets = %d,%d %d,%d %d,%d %d,%d %d,%d',pnt{:});
        Eyelink('Command','calibration_type = HV5'); % HV5 or HV9
        el.displayCalResults = 1;
        EyelinkUpdateDefaults(el); % pass the values back to the Eyelink
        
        % File contents:   - defaults from manual adapted to my task:
        Eyelink('Command','file_event_data = GAZE, GAZERES, AREA, HREF, VELOCITY');
        Eyelink('Command','file_event_filter = LEFT, RIGHT, FIXATION, SACCADE, BLINK, MESSAGE');
        Eyelink('Command','file_sample_data = LEFT, RIGHT, GAZE, GAZERES, AREA, STATUS');
        % NB: area here merely means pupilsize: was set to diameter higher up in settings
        Eyelink('Command','link_sample_data = GAZE, AREA');% gaze in display coords (px)
        
        % Add structs for missed and outoffix samples: 1 per block:
        video.eyelink.nmiss = zeros(1,numel(unique([sqc.blk])));
        video.eyelink.nout = zeros(1,numel(unique([sqc.blk])));
        video.eyelink.nevt = zeros(1,numel(unique([sqc.blk])));
        
        % Compute limits for fixation tolerance :
        video.eyelink.xtolerance = video.shape_offset;
        video.eyelink.ytolerance = video.shape_sz / 2;
        video.eyelink.tolerancebox = [0, 0, (video.eyelink.xtolerance*2), video.eyelink.ytolerance*2];
        video.eyelink.tolerancebox = CenterRectOnPoint(video.eyelink.tolerancebox, video.xCenter,video.yCenter);
        
    end
    
    %% Start display:
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% test kb, hide cursor, stop spilling keys
    if ~video.testingON
        HideCursor;
        FlushEvents;
        ListenChar(2);
    end %setting testing parameters
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% welcome screen:
    line1 = sprintf('Beginning of the experiment :\n\n\n');
    line2 = sprintf('    - You will do 8 blocks of 12 sequences (around 6min per block).\n');
    line3 = sprintf('    - You can take breaks between blocks.\n    - First, let''s calibrate the eyetracker !');
    DrawFormattedText(video.window, [line1 line2 line3], video.screenYpixels * 0.25, video.screenYpixels * 0.25, black);
    spaceline = 'Press [space] to start.';
    DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.90, black);
    vbl = Screen('Flip', video.window, nextfliptime);
    nextfliptime = vbl+fx_roundfp(0.01,0,video.ifi); % wait 10ms
    WaitKeyPress(spaceKey);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% loop over blocks
    fprintf('Starting looping over blocks \n\n');
    blocks = unique([sqc.blk]);
    nblocks = numel(blocks);
    
    isamples = 0;
    for iblock = 1:nblocks
        
        thisblock = sqc([sqc.blk] == iblock);
        nseq = numel(thisblock);
        choicesgrades = NaN(1, nseq);
        
        % EYELINK: Calibrate & lock eye after calibration
        % EYELINK: Open EDF file, write header
        % EYELINK: Send SYNCHTIME trigger   
        if video.eyelink.eyeON
            
            k = RestrictKeysForKbCheck(allKeys);
            
            %%% warn calibration
            fprintf('Calibrate eyetracker for block %d\n', iblock);
            calibtxt = sprintf('CALIBRATION BLOCK %d / %d...', iblock, nblocks);
            DrawFormattedText(video.window, calibtxt, 'center', video.screenYpixels * 0.50, black);
            spaceline = 'Press [space] to start.';
            DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.90, black);
            vbl = Screen('Flip', video.window, nextfliptime);
            nextfliptime = vbl+fx_roundfp(0.01,0,video.ifi); % wait 10ms
            WaitKeyPress(spaceKey);
            
            %%% calibrate:
            calibration = EyelinkDoTrackerSetup(el, el.ENTER_KEY); %el.ENTER_KEY shows the eye image on display screen
            [~, calmessage] = Eyelink('CalMessage');
            fprintf('\nCalibration resulted in %s \n', calmessage); %TODO what's this?
            
            %%% start file:
            edfname = sprintf('COX%03d_%d', subj, iblock); % name is >1:8<chars
            status = Eyelink('OpenFile', edfname);
            if status ~= 0 ; error('... clemence: couldnt open EDF\n'); end
            fprintf('\nOpening EDF for block %d returns: %d (zero is ok!) \n', iblock, status);
            Eyelink('Command', 'set_idle_mode');
            Eyelink('Message', 'DISPLAY_COORDS %ld %ld %ld %ld', video.eyelink.pixelcoords{:});
            Eyelink('Message', 'PHY_SCREEN_SIZE %ld %ld %ld %ld', video.eyelink.physicalscreensize{:});
            Eyelink('Message', 'PHY_SCREEN_DISTANCE = %d', video.eyelink.inputscreendistance_mm);
            Eyelink('Message',calmessage);
            Eyelink('command', 'draw_cross %d %d 1', video.xCenter, video.yCenter); % will stay as long as not changed...
            WaitSecs(0.1);
            Eyelink('StartRecording');
            WaitSecs(1); % record 1sec -> preproc Valentin removes 1'' before & after block
            recording = Eyelink('CheckRecording');
            if recording ~= 0 ; error('... clemence: EL cannot record...\n'); end
        end
        
        k = RestrictKeysForKbCheck(expeKeys);
        
        % Begin block screen:
        fprintf('................\n ');
        fprintf('>>>Start block %.f/%.f----- %s<<<\n', iblock, nblocks, datestr(now, 'HH:MM:SS'));
        line = sprintf('START OF BLOCK %.f/%.f !\n', iblock, nblocks);
        DrawFormattedText(video.window, line, 'center', video.screenYpixels * 0.30, black);
        spaceline = 'Press [space] to start.';
        DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.90, black);
        vbl = Screen('Flip', video.window);
        nextfliptime = vbl+fx_roundfp(1,0,video.ifi); % wait 1 sec before begining block.
        WaitKeyPress(spaceKey);
        startblk = GetSecs;
        
        % EYELINK: send synchtime message
        if video.eyelink.eyeON
            Eyelink('Message', 'SYNCHTIME'); %TODO IS THAT NECESSARY???
        end
        
        if video.mouseON
            warning('mouseON break');
            break
        end
        
        %%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% loop over runs:
        fprintf('...Starting looping over runs\n');
        for iseq = 1:nseq
            
            % Get current run index in full sqc str
            isqc = find([sqc.blk] == iblock & [sqc.blkseq] == iseq);
            if mod(isqc,3) == 0; fprintf('Seq # %d/%d...\n', iseq, nseq); end
            
            % Draw instructions for the run
            targetsymbol = sqc(isqc).targetsymbol;
            othersymbol  = sqc(isqc).othersymbol;
            condition    = sqc(isqc).condition;
            targetcolor  = sqc(isqc).targetcolor;
            sidea  = sqc(isqc).instrside; % get side of target at the instructions
            sideb  = 3 - sidea;
            if      targetcolor == 1; colorstr = 'blue'; % get target color string
            elseif  targetcolor == 2; colorstr = 'orange';
            end
            if condition == 1 % get instructions string
                line = sprintf('These are the symbols, try to draw %s!', colorstr);
            elseif condition == 2
                line = 'These are the symbols, try to guess which colour they draw from!';
            end
            DrawFormattedText(video.window, line, 'center', video.screenYpixels * 0.30, black);
            
            % Draw run symbols
            Screen('DrawDots', video.window, [video.xCenter video.yCenter], 15 , black, [0 0], 2);
            Screen('DrawTexture', video.window, shape_tex(1,targetsymbol), [], positions(sidea,:), 0, [], [], [0,0,0]);
            Screen('DrawTexture', video.window, shape_tex(2,targetsymbol), [], positions(sidea,:), 0, [], [], video.lbgd);
            Screen('DrawTexture', video.window, shape_tex(1,othersymbol), [], positions(sideb,:), 0, [], [], [0,0,0]);
            Screen('DrawTexture', video.window, shape_tex(2,othersymbol), [], positions(sideb,:), 0, [], [], video.lbgd);
            
            % Draw run progress at the bottom
            spaceline = 'Press [space] to start.';
            progressline = sprintf('Sequence %.f/%.f of this block, keep going!\n', iseq, nseq);
            nextline = [progressline spaceline];
            DrawFormattedText(video.window, nextline, 'center', video.screenYpixels * 0.90, black);
            
            % Flip, wait for spacebar press, wait 1''
            Screen('DrawingFinished', video.window);
            vbl = Screen('Flip', video.window, nextfliptime);
            % EYELINK: check recording
            % EYELINK: send begin runx message
            if video.eyelink.eyeON
                err=Eyelink('CheckRecording');
                if(err~=0); error('CheckRecording problem'); end
                msg = sprintf('ISQC_instr_%03d', isqc);
                Eyelink('Message', msg);
            end
            nextfliptime = vbl+fx_roundfp(1,0,video.ifi); % wait 1sec showing the instructions
            k = RestrictKeysForKbCheck(spaceKey);
            WaitKeyPress(spaceKey);

            % Flip fixation point for 1sec before starting
            Screen('DrawDots', video.window, [video.xCenter video.yCenter], 15 , black, [0 0], 2);
            % EYELINK: send start message
            if video.eyelink.eyeON
                msg = sprintf('ISQC_start_%03d', isqc);
                Eyelink('Message', msg);
            end
            vbl = Screen('Flip', video.window, nextfliptime);
            nextfliptime = vbl+fx_roundfp(1,0,video.ifi); %WaitSecs(1) before next flip
            
            %%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%% loop over samples
            nsamples = sqc(isqc).nsamples;
            choices = nan(1,nsamples);
            
            for ismp = 1:nsamples
                
                k = RestrictKeysForKbCheck(expeKeys); %reset by clearall or RestrictKeysForKbCheck([])
                
                % Draw sampling alternatives: draw "target" on "symbolside"
                targetside  = sqc(isqc).symbolsides(ismp);
                otherside   = 3 - sqc(isqc).symbolsides(ismp);
                Screen('DrawDots', video.window, [video.xCenter video.yCenter], 15 , black, [0 0], 2);
                Screen('DrawTexture', video.window, shape_tex(1,targetsymbol), [], positions(targetside,:), 0, [], [], [0,0,0]);
                Screen('DrawTexture', video.window, shape_tex(2,targetsymbol), [], positions(targetside,:), 0, [], [], video.lbgd);
                Screen('DrawTexture', video.window, shape_tex(1,othersymbol), [], positions(otherside,:) , 0, [], [], [0,0,0]);
                Screen('DrawTexture', video.window, shape_tex(2,othersymbol), [], positions(otherside,:) , 0, [], [], video.lbgd);
                Screen('DrawingFinished', video.window);
                
                % Before next flip, check for pupil status and abort key:
                while GetSecs < nextfliptime
                    if CheckKeyPress(escapeKey)
                        Priority(0); FlushEvents; ListenChar(0); ShowCursor;
                        WaitSecs(1); Eyelink('Command', 'set_idle_mode'); Eyelink('StopRecording'); Eyelink('CloseFile');
                        edfpath = fullfile(pwd, '/', subjdir);
                        Eyelink('ReceiveFile', edfname, edfpath, 1);
                        system(sprintf('edf2asc %s.edf', [edfpath '/' edfname])); %convert to asc
                        fprintf('\nData converted from edf to asc file in local >>>%s. \n', edfpath);
                        error('aborted by ESCAPE');
                    end
                    if video.eyelink.eyeON == 1; video = COLEXP_expe_fx_check_missing_pupil(video, iblock); end
                end
                vbl = Screen('Flip', video.window, nextfliptime);
                
                % EYELINK: flip sampling probe x
                if video.eyelink.eyeON
                    msg = sprintf('ISMP_smpprobe_run%03d_smp%02d', isqc, ismp);
                    Eyelink('Message', msg);
                end
                
                % Get sampling choice:
                while true
                    [keyIsDown,secs,keylist] = KbCheck();
                    if video.eyelink.eyeON == 1; video = COLEXP_expe_fx_check_missing_pupil(video, iblock); end
                    if keyIsDown && sum(keylist) == 1 && (keylist(leftKey) == 1 || keylist(rightKey) == 1)
                        % EYELINK: resp sampling x
                        if video.eyelink.eyeON
                            msg = sprintf('ISMP_resp_run%03d_smp%02d', isqc, ismp);
                            Eyelink('Message', msg);
                        end
                        rt = secs - vbl;
                        choice = find(respKeys == (find(keylist==1)));
                        break
                    end
                end%while awaiting for resp
                nextfliptime = secs+fx_roundfp(0,0,video.ifi); %don't wait, flip asap?
                
                % draw color outcome:
                if choice == targetside
                    samplecolor = sqc(isqc).colors.target(ismp,:);
                    Screen('DrawDots', video.window, [video.xCenter video.yCenter], 15 , black, [0 0], 2);
                    Screen('DrawTexture', video.window, shape_tex(1,targetsymbol), [], positions(targetside,:), 0, [], [], [0,0,0]);
                    Screen('DrawTexture', video.window, shape_tex(2,targetsymbol), [], positions(targetside,:), 0, [], [], samplecolor);
                    choices(ismp) = 1;
                elseif choice == otherside
                    samplecolor = sqc(isqc).colors.other(ismp,:);
                    Screen('DrawDots', video.window, [video.xCenter video.yCenter], 15 , black, [0 0], 2);
                    Screen('DrawTexture', video.window, shape_tex(1,othersymbol), [], positions(otherside,:) , 0, [], [], [0,0,0]);
                    Screen('DrawTexture', video.window, shape_tex(2,othersymbol), [], positions(otherside,:) , 0, [], [], samplecolor);
                    choices(ismp) = 2;
                end
                
                % check pupil status before flipping outcome:
                if video.eyelink.eyeON == 1
                    while GetSecs < nextfliptime; video = COLEXP_expe_fx_check_missing_pupil(video, iblock); end
                end
                
                % flip outcome:
                vbl = Screen('Flip', video.window, nextfliptime);
                
                % EYELINK: color outcome sampling x
                % write outcome:
                if video.eyelink.eyeON
                    msg = sprintf('ISMP_colout_run%03d_smp%02d', isqc, ismp);
                    Eyelink('Message', msg);
                end
                
                % draw fxpoint:
                Screen('DrawDots', video.window, [video.xCenter video.yCenter], 15 , black, [0 0], 2);
                
                % check pupil status during 0.5 before next flip (showingoutcome):
                nextfliptime = vbl+fx_roundfp(0.5,0,video.ifi);
                if video.eyelink.eyeON == 1
                    while GetSecs < nextfliptime; video = COLEXP_expe_fx_check_missing_pupil(video, iblock); end
                end
                
                % flip fixation point:
                vbl = Screen('Flip', video.window, nextfliptime);
                
                % check pupil status for 0.5'' before next flip (showingfxpt):
                nextfliptime = vbl+fx_roundfp(0.5,0,video.ifi);
                if video.eyelink.eyeON == 1
                    while GetSecs < nextfliptime; video = COLEXP_expe_fx_check_missing_pupil(video, iblock); end
                end
                
                % Store results:
                isamples = isamples +1;
                sampling(isamples).subj = subj;
                sampling(isamples).condition = condition;
                sampling(isamples).mirroring = sqc(isqc).mirroring;
                sampling(isamples).blk = sqc(isqc).blk;
                sampling(isamples).blkseq = iseq;
                sampling(isamples).isqc = isqc;     % 1:96
                sampling(isamples).samplenb = ismp;
                sampling(isamples).nsamples = sqc(isqc).nsamples;
                sampling(isamples).symbolpair = sqc(isqc).symbolpair;
                sampling(isamples).targetsym = targetsymbol;
                
                sampling(isamples).counterfness = sqc(isqc).counterfness;
                sampling(isamples).targetcolor = targetcolor;
                
                sampling(isamples).targetside = targetside;
                sampling(isamples).samplingchoice = choice;
                sampling(isamples).samplingcolor = samplecolor;
                sampling(isamples).samplingrt = rt;
                
            end%FOR each samples
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % End of each run: draw final probe while no answer yet:
            k = RestrictKeysForKbCheck(expeKeys);
            
            % Final probe text; depending on condition,
            switch condition
                case 1 % target: don't show final probe
                    respprobe = NaN;
                    rtprobe = NaN;
                    goodanswer = NaN;
                    % EYELINK: end run x
                    if video.eyelink.eyeON
                        msg = sprintf('ISQC_end_%03d', isqc);
                        Eyelink('Message', msg);
                    end
                case 2 % open: show final probe:
                    % color buttons
                    goodanswer = sqc(isqc).probeside;% this is the side of the target color
                    wronganswer = 3-goodanswer;
                    if targetcolor == 1%blue
                        bluebuttonside = goodanswer;
                        redbuttonside = wronganswer;
                        %fbcol = blue;
                        %if sqc(isqc).counterfness == 2; fbcol = orange; end %FLIPS IF BOTH ARE OF THE BAD COLOR?
                    elseif targetcolor == 2%red
                        bluebuttonside = wronganswer;
                        redbuttonside = goodanswer;
                        %fbcol = orange;
                        %if sqc(isqc).counterfness == 2; fbcol = blue; end %FLIPS IF BOTH ARE OF THE BAD COLOR?
                    end
                    % draw probe:
                    line = sprintf('In this sequence, from which colour was this symbol mainly drawing from?');
                    DrawFormattedText(video.window,  line, 'center', video.screenYpixels*0.20, black);
                    Screen('DrawTexture', video.window, shape_tex(1,targetsymbol), [], centerpos, 0, [], [], [0,0,0]);
                    Screen('DrawTexture', video.window, shape_tex(2,targetsymbol), [], centerpos, 0, [], [], video.lbgd);
                    
                    Screen('DrawTexture', video.window, rep_tex(1,redbuttonside), [], rep_pos(redbuttonside,:), 0, [], [], [0,0,0]);
                    Screen('DrawTexture', video.window, rep_tex(2,redbuttonside), [], rep_pos(redbuttonside,:), 0, [], [], orange);
                    Screen('DrawTexture', video.window, rep_tex(1, bluebuttonside), [], rep_pos(bluebuttonside,:), 0, [], [], [0,0,0]);
                    Screen('DrawTexture', video.window, rep_tex(2, bluebuttonside), [], rep_pos(bluebuttonside,:), 0, [], [], blue);
                    Screen('DrawingFinished', video.window);
                    
                    % check pupil status before showing final probe:
                    if video.eyelink.eyeON == 1
                        while GetSecs < nextfliptime; video = COLEXP_expe_fx_check_missing_pupil(video, iblock); end
                    end
                    vbl = Screen('Flip', video.window, nextfliptime);
                    nextfliptime = secs+fx_roundfp(0,0,video.ifi); % flip ASAP after response
                    
                    % EYELINK: final probe run x
                    if video.eyelink.eyeON
                        msg = sprintf('ISQC_finalprobe_%03d', isqc);
                        Eyelink('Message', msg);
                    end
                    
                    % wait for final response:
                    while true
                        [keyIsDown,secs,keylist] = KbCheck();
                        if keyIsDown && (keylist(leftKey) == 1 ||  keylist(rightKey) == 1) && sum(keylist) == 1
                            % EYELINK: final resp run x
                            if video.eyelink.eyeON
                                msg = sprintf('ISQC_finresp_%03d', isqc);
                                Eyelink('Message', msg);
                            end
                            respprobe = find(respKeys == (find(keylist==1)));
                            rtprobe  = secs - vbl;
                            break
                        end
                    end
                    
                    % redraw probe with selection square: 0.4secs:
                    Screen('DrawTexture', video.window, shape_tex(1,targetsymbol), [], centerpos, 0, [], [], [0,0,0]);
                    Screen('DrawTexture', video.window, shape_tex(2,targetsymbol), [], centerpos, 0, [], [], video.lbgd);
                    DrawFormattedText(video.window,  line, 'center', video.screenYpixels*0.20, black);
                    Screen('DrawTexture', video.window, rep_tex(1,redbuttonside), [], rep_pos(redbuttonside,:), 0, [], [], [0,0,0]);
                    Screen('DrawTexture', video.window, rep_tex(2,redbuttonside), [], rep_pos(redbuttonside,:), 0, [], [], orange);
                    Screen('DrawTexture', video.window, rep_tex(1,bluebuttonside), [], rep_pos(bluebuttonside,:), 0, [], [], [0,0,0]);
                    Screen('DrawTexture', video.window, rep_tex(2,bluebuttonside), [], rep_pos(bluebuttonside,:), 0, [], [], blue);
                    Screen('FrameRect', video.window, black, selection_rect(respprobe,:), 6);
                    Screen('DrawingFinished', video.window);
                    vbl = Screen('Flip', video.window, nextfliptime);
                    nextfliptime = vbl+fx_roundfp(0.4,0,video.ifi); % show selection squares for 0.4
                    
                    % EYELINK: show selection run x
                    if video.eyelink.eyeON
                        msg = sprintf('ISQC_finsel_%03d', isqc);
                        Eyelink('Message', msg);
                    end
                    
                    % color feedback on final probe: 0.5secs:
                    if sqc(isqc).counterfness == 2; tcol = 3 - targetcolor; else; tcol = targetcolor; end
                    if tcol == 1; tcol = blue; elseif tcol == 2; tcol = orange; end
                    Screen('DrawTexture', video.window, shape_tex(1,targetsymbol), [], centerpos, 0, [], [], [0,0,0]);
                    Screen('DrawTexture', video.window, shape_tex(2,targetsymbol), [], centerpos, 0, [], [], tcol);
                    DrawFormattedText(video.window,  line, 'center', video.screenYpixels*0.20, black);
                    Screen('DrawTexture', video.window, rep_tex(1,redbuttonside), [], rep_pos(redbuttonside,:), 0, [], [], [0,0,0]);
                    Screen('DrawTexture', video.window, rep_tex(2,redbuttonside), [], rep_pos(redbuttonside,:), 0, [], [], orange);
                    Screen('DrawTexture', video.window, rep_tex(1,bluebuttonside), [], rep_pos(bluebuttonside,:), 0, [], [], [0,0,0]);
                    Screen('DrawTexture', video.window, rep_tex(2,bluebuttonside), [], rep_pos(bluebuttonside,:), 0, [], [], blue);
                    Screen('FrameRect', video.window, black, selection_rect(respprobe,:), 6);
                    Screen('DrawingFinished', video.window);
                    vbl = Screen('Flip', video.window, nextfliptime);
                    nextfliptime = vbl+fx_roundfp(0.5,0,video.ifi); % show feedback for 0.8
                    
                    % EYELINK: show feedback final probe run x
                    % EYELINK: end run x
                    if video.eyelink.eyeON
                        msg = sprintf('ISQC_finfb_%03d', isqc);
                        Eyelink('Message', msg);
                        msg = sprintf('ISQC_end_%03d', isqc);
                        Eyelink('Message', msg);
                    end
                    
            end %CASE condition open, show final probe
            
            Screen('FillRect', video.window, video.lbgd);
            vbl = Screen('Flip', video.window, nextfliptime);
            nextfliptime = vbl+fx_roundfp(0.3,0,video.ifi); % 0.3 before nextrun
            
            choicesgrades(iseq) = sum(choices == sqc(isqc).optchoices(choices(1),:)) /nsamples;
            
            % Store final response: cuisine pour remettre info dans tous les samples: TODO deal()?
            for i = (isamples - (nsamples-1)) : isamples
                sampling(i).finaltargetside = goodanswer;
                sampling(i).finalresponse = respprobe;
                sampling(i).finalrt = rtprobe;
            end
            final(isqc).subj = subj;
            final(isqc).condition = condition;
            final(isqc).mirroring = sqc(isqc).mirroring;
            final(isqc).blk       = sqc(isqc).blk;
            final(isqc).blkseq    = sqc(isqc).blkseq;
            final(isqc).isqc     = isqc;
            final(isqc).nsamples  = sqc(isqc).nsamples;
            final(isqc).symbolpair     = sqc(isqc).symbolpair;
            final(isqc).targetsymbol   = targetsymbol;
            final(isqc).counterfness   = sqc(isqc).counterfness;
            final(isqc).targetcolor    = sqc(isqc).targetcolor;
            final(isqc).goodanswerside = sqc(isqc).probeside;
            final(isqc).finalresponse  = respprobe;
            final(isqc).finalrt        = rtprobe;
            
            final(isqc).choices        = choices;
            
        end%FOR each run of samples in this block
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % EYELINK: close EDF and transfer
        if video.eyelink.eyeON
            %save
            WaitSecs(1); Eyelink('Command', 'set_idle_mode'); Eyelink('StopRecording'); Eyelink('CloseFile');
            % flip to participant to wait:
            line = sprintf('... please wait while we transfer your data for this block ...');
            DrawFormattedText(video.window, line, 'center', video.screenYpixels * 0.5, black);
            vbl = Screen('Flip', video.window, nextfliptime);
            nextfliptime = vbl+fx_roundfp(0.01,0,video.ifi); %wait 10ms
            %transfer
            edfpath = fullfile(pwd, '/', subjdir);
            gotit = Eyelink('ReceiveFile', edfname, edfpath, 1);
            fprintf('\nData transfered = %d Mo, file >>>%s.edf can be found in >>>%s. \n', gotit, edfname, edfpath);
            %convert to asc
            system(sprintf('edf2asc %s.edf', [edfpath '/' edfname]));
            fprintf('\nData converted from edf to asc file in local >>>%s. \n', edfpath);
        end
        
       save(sprintf('%s/expout_S%02d_b%02d.mat', subjdir, subj, iblock), 'header', 'sampling', 'final', 'sqc', 'video', 'ME');
        
       
        %%% End of each block, insert pause & flip score
        endblk = GetSecs;
        blkkeypresses = [sampling([sampling.blk] == iblock).samplingchoice]; % get samples from this block & check key presses:
        keypresses = sum(blkkeypresses==1) / numel(blkkeypresses) *100;
        fprintf('\n...******************************\n...DONE WITH BLOCK %d!\n', iblock);
        fprintf('...Block lasted %.f:%02.f min\n', (endblk - startblk)/60, mod((endblk - startblk),60))
        fprintf('...Mean prop of optimal responses is %2.f%%\n', nanmean(choicesgrades)*100);
        fprintf('...Participant pressed [a] %.f %%\n', keypresses);
        if video.eyelink.eyeON == 1
            fprintf('...total BADPUPIL (miss =%.01f%%, out =%.01f%%) =  %f\n',video.eyelink.nmiss/video.eyelink.nevt*100, video.eyelink.nout/video.eyelink.nevt*100, (video.eyelink.nmiss+video.eyelink.nout)/(video.eyelink.nevt)*100);
        end
        fprintf('...*****Participant is on BREAK!\n...*****Press [t] and [y] to continue.\n...******************************\n')
        %%% show block score feedback
        if nanmean(choicesgrades) < 0.5
            pscr = 0;
        elseif nanmean(choicesgrades) >= 0.5 && nanmean(choicesgrades) <= 0.6
            pscr = 1;
        elseif nanmean(choicesgrades) >= 0.61 && nanmean(choicesgrades) < 0.65
            pscr = 2;
        elseif nanmean(choicesgrades) >= 0.66
            pscr = 3;
        end
        %%%% show score for 0.8 seconds
        switch pscr
            case 0
                Screen('FrameRect', video.window, black, cupframe_rect, 4);
            case 1
                Screen('FrameRect', video.window, black, cupframe_rect, 4);
                Screen('DrawTexture', video.window, cuptext, [], cuppos(1,:));
            case 2
                Screen('FrameRect', video.window, black, cupframe_rect, 4);
                Screen('DrawTexture', video.window, cuptext, [], cuppos(1,:));
                Screen('DrawTexture', video.window, cuptext, [], cuppos(2,:));
            case 3
                Screen('FrameRect', video.window, black, cupframe_rect, 4);
                Screen('DrawTexture', video.window, cuptext, [], cuppos(1,:));
                Screen('DrawTexture', video.window, cuptext, [], cuppos(2,:));
                Screen('DrawTexture', video.window, cuptext, [], cuppos(3,:));
        end
        vbl = Screen('Flip', video.window, nextfliptime);
        nextfliptime = vbl+fx_roundfp(0.8,0,video.ifi); % schow score for 0.8
        %%%% show break until pressed space
        switch pscr
            case 0
                Screen('FrameRect', video.window, black, cupframe_rect, 4);
            case 1
                Screen('FrameRect', video.window, black, cupframe_rect, 4);
                Screen('DrawTexture', video.window, cuptext, [], cuppos(1,:));
            case 2
                Screen('FrameRect', video.window, black, cupframe_rect, 4);
                Screen('DrawTexture', video.window, cuptext, [], cuppos(1,:));
                Screen('DrawTexture', video.window, cuptext, [], cuppos(2,:));
            case 3
                Screen('FrameRect', video.window, black, cupframe_rect, 4);
                Screen('DrawTexture', video.window, cuptext, [], cuppos(1,:));
                Screen('DrawTexture', video.window, cuptext, [], cuppos(2,:));
                Screen('DrawTexture', video.window, cuptext, [], cuppos(3,:));
        end
        line1 = sprintf('This block is done!\n');
        DrawFormattedText(video.window, line1, 'center', video.screenYpixels * 0.25, black);
        line2 = sprintf('TAKE A BREAK!\n When you are ready to continue, call the experimenter in.\n');
        DrawFormattedText(video.window, line2, 'center', video.screenYpixels * 0.75, black);
        Screen('DrawingFinished', video.window);
        vbl = Screen('Flip', video.window, nextfliptime);
        nextfliptime = vbl+fx_roundfp(0.3,0,video.ifi);
        k = RestrictKeysForKbCheck(spaceKey);
        WaitKeyPress(spaceKey);
        k = RestrictKeysForKbCheck(continueKey);
        WaitKeyPress(continueKey);
        
    end%FOR EACH BLOCK IN EXPRUN STRUCTURE
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %% DRAW FINAL THANK YOU SCREEN
    line = sprintf('Thank you for you participation, the experiment is over!\n\n\n');
    endline = 'TERMINER -- [espace] -- TERMINER';
    DrawFormattedText(video.window, [line endline], 'center', video.yCenter, black);
    Screen('DrawingFinished', video.window);
    vbl = Screen('Flip', video.window, nextfliptime);
    k = RestrictKeysForKbCheck(spaceKey);
    WaitKeyPress(spaceKey);
    sca;
    
    % save PTB data
    header.end = datestr(now,'yyyymmdd-HHMM');
    header.aborted = 0;
    expout.header = header;
    expout.sampling = sampling;
    expout.final = final;
    expout.sqc = sqc;
    expout.video = video;
    save(sprintf('%s/expout_S%02d.mat', subjdir, subj), 'header', 'sampling', 'final', 'sqc', 'video', 'ME');
    
    % close PTB
    Priority(0); FlushEvents; ListenChar(0); ShowCursor; Screen('CloseAll');
    Screen('Preference', 'SkipSyncTests', 0);%restore synch testing
    Screen('Preference','VisualDebuglevel',4);%restore warning verbosity
    Screen('Preference','SuppressAllWarnings',0);%=1 == warning verbosity=0
    k = RestrictKeysForKbCheck([]);
    
    % close EYELINK:
    if video.eyelink.eyeON
        recording = Eyelink('CheckRecording');
        if recording == 0 % still recording rn
            %save
            WaitSecs(1); Eyelink('Command', 'set_idle_mode'); Eyelink('StopRecording'); Eyelink('CloseFile');
            %transfer
            subjdir = sprintf('../Data/S%02d',subj);
            edfpath = fullfile(pwd, '/', subjdir);
            edfname = sprintf('COX%03d_%d', subj, iblock);
            gotit = Eyelink('ReceiveFile', edfname, edfpath, 1);
            fprintf('\nData transfered = %d Mo, file >>>%s.edf can be found in >>>%s. \n', gotit, edfname, edfpath);
            %convert to asc
            system(sprintf('edf2asc %s.edf', [edfpath '/' edfname]));
            fprintf('\nData converted from edf to asc file in local >>>%s. \n', edfpath);
        end
        Eyelink('Command','generate_default_targets = YES'); % YES:default or NO:custom
        Eyelink('Shutdown');
    end
    
    fprintf('\n...fx_run_expe OVER AND OUT...\n');
    diary off;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% catching results so far if crashes
catch ME
    
    % clean save behaviour:
    header.end = datestr(now,'yyyymmdd-HHMM');
    header.aborted = 1;
    expout.header = header;
    expout.sampling = sampling;
    expout.final = final;
    expout.sqc = sqc;
    expout.video = video;
    expout.ME = ME;
    save(sprintf('%s/expout_S%02d_b%02d_part.mat', subjdir, subj, iblock)) % if crash, save all variables in workspace
    
    % close PTB
    Priority(0); FlushEvents; ListenChar(0); ShowCursor; Screen('CloseAll');
    Screen('Preference', 'SkipSyncTests', 0);%restore synch testing
    Screen('Preference','VisualDebuglevel',4);%restore warning verbosity
    Screen('Preference','SuppressAllWarnings',0);%=1 == warning verbosity=0
    k = RestrictKeysForKbCheck([]);
    
    % close EYELINK:
    if video.eyelink.eyeON
        recording = Eyelink('CheckRecording');
        if recording == 0 % still recording rn
            %save
            WaitSecs(1); Eyelink('Command', 'set_idle_mode'); Eyelink('StopRecording'); Eyelink('CloseFile');
            %transfer
            subjdir = sprintf('../Data/S%02d',subj);
            edfpath = fullfile(pwd, '/', subjdir);
            edfname = sprintf('COX%03d_%d', subj, iblock);
            gotit = Eyelink('ReceiveFile', edfname, edfpath, 1);
            fprintf('\nData transfered = %d Mo, file >>>%s.edf can be found in >>>%s. \n', gotit, edfname, edfpath);
            %convert to asc
            system(sprintf('edf2asc %s.edf', [edfpath '/' edfname]));
            fprintf('\nData converted from edf to asc file in local >>>%s. \n', edfpath);
        end
        Eyelink('Command','generate_default_targets = YES'); % YES:default or NO:custom
        Eyelink('Shutdown');
    end
    
    rethrow(ME);
        
end% TRY/CATCH

end% function def fx_run_expe.m


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


