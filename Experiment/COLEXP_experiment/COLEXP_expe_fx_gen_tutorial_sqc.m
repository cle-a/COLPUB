 function tutorialsqc = COLEXP_expe_fx_gen_tutorial_sqc()
% gen runs for instructions runs, 7 trials in each run:

instructionscolors = [...
    0.4450    0.8030    0.5630    0.3610; ...
    0.5190    0.6830    0.9250    0.5350;...
    0.7110    0.4450    0.5320   -0.2770;...
    0.8190   -0.3110   -0.1670    0.4890;...
    0.9910    0.7250    0.8890    0.6650;...
    0.3630   -0.1150   -0.0370    0.4770;...
    0.4490    0.4030   -0.2320    0.7950];

tutorialsqc(1).symbols = [5, 6];
tutorialsqc(1).targetsymbol = 5; %star
tutorialsqc(1).othersymbol = 6; %cross
tutorialsqc(1).symbolsides = [1 1 1 1 2 2 2];
tutorialsqc(1).colors.target = fx_get_rgb(instructionscolors(:,1)); % star is orange shape
tutorialsqc(1).colors.other = fx_get_rgb((-1) * instructionscolors(:,1)); % they're counterfactual, blue shape

tutorialsqc(2).symbols = [8, 7];
tutorialsqc(2).targetsymbol = 8; %moon
tutorialsqc(2).othersymbol = 7; %square
tutorialsqc(2).symbolsides = [1 2 1 1 1 2 2];
tutorialsqc(2).colors.target = fx_get_rgb((-1) * instructionscolors(:,2)); %target is blue & they're counterfactual:
tutorialsqc(2).colors.other = fx_get_rgb(instructionscolors(:,2));

tutorialsqc(3).symbols = [1, 6];
tutorialsqc(3).targetsymbol = 1; %circle
tutorialsqc(3).othersymbol = 6; %cross
tutorialsqc(3).symbolsides = [1 1 1 2 2 1 2];
tutorialsqc(3).colors.target = fx_get_rgb(instructionscolors(:,3)); % target is orange & they're counterfactual:
tutorialsqc(3).colors.other = fx_get_rgb((-1) * instructionscolors(:,3));

tutorialsqc(4).symbols = [4, 2];
tutorialsqc(4).targetsymbol = 4; %penta
tutorialsqc(4).othersymbol = 2; %triangle
tutorialsqc(4).symbolsides = [2 2 1 1 2 1 1];
tutorialsqc(4).colors.target = fx_get_rgb(instructionscolors(:,4)); % target is orange & they're counterfactual:
tutorialsqc(4).colors.other = fx_get_rgb((-1) * instructionscolors(:,4));

stampedname = sprintf('tutorialsqc_%s.mat', datestr(now,'ddmmyy'));
save(stampedname, 'tutorialsqc');
save('tutorialsqc.mat', 'tutorialsqc');
