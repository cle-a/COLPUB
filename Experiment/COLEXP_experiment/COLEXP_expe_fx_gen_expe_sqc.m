function [sqc] = COLEXP_expe_fx_gen_expe_sqc(subj, NSEQ, perr)
%% making sequences
% (1) define cond, length of run, 'counterfactualness', target colors
% (2) get color draws of equalised difficulty for each sequence (fx_get_linearcolorvalues)
% (3) assort seqs for first condition
% ((3b) add the rgb transformed colours - COLEXP_expe_fx_get_rgb)
% (4) mirror them for second condition
% (5) add simulations of the optimal sampling behaviour (for scores - fx_get_optsampling)
% (6) divide sequences into blocks and order them within blocks (fx_makeblocks - fx_orderblocks)
fprintf('Creating sequences from >>>fx_gen_sqc\n')
tic

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% arg check: 
if nargin < 1
    subj = 99;
    NSEQ = 32;
    warning('Cle: Will take default subj (99) and nseq = (32)');
elseif nargin == 1
    NSEQ = 32;
    warning('///Will take default nseq = (32)');
end
if mod(NSEQ, 64) ~= 0
    warning('Cle: will not balance probeside with %d seq, choose multiple of 64 (64 128 192)\n', NSEQ);
end
%32 if not controlling for good response side (32, 64, 96, 128, 192)
%64 if controlling for motor response (64, 128, 192)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% defining conditions:

sqc = struct();

%%% target/open sampling condition = 1s or 2s
nseq_condition  = NSEQ/2;
condition       = [repelem(1,nseq_condition) repelem(2,nseq_condition)]; %size=1*64 (FULL)

%%% lengths of seqs = 8,12,16,20 samples per seq (MUST BE EVEN)
nlengths        = 4;
nseqs_length    = nseq_condition/nlengths; % nb of seqs per length condition (4different lengths)
nsamples        = [repelem(8,nseqs_length) repelem(12,nseqs_length) repelem(16,nseqs_length) repelem(20,nseqs_length)]; %size=HALF

%%% counterfactual or same colors = for each length : 1/3 counterfactual(1), 1/3 good/good, 1/3 bad/bad
cf_length    = nseqs_length/3;
counterfness = repmat([repelem(1,cf_length),repelem(2,cf_length),repelem(3,cf_length)], [1,nlengths]); %size=HALF

%%% for each cf, half blue target half orange:
ntarget_cf   = cf_length/2;
targetcolors = repmat([repelem(1,ntarget_cf),repelem(2,ntarget_cf)], [1,nseqs_length]); %size=HALF

%%% probeside is not controlled here:
if mod(NSEQ, 64) == 0
    nprobeside = ncolors_agency/2;
    probeside = repmat([repelem(1, nprobeside) repelem(2, nprobeside)], [1,nlenghts*2*2]);
elseif mod(NSEQ, 64) ~= 0
    probeside = randi(2,[1,nseq_condition]);
end

%%% instuctionside is random, but mirrored
instrside = randi(2,[1,nseq_condition]);

%%% get a pool of symbol pairs with least amount of repeats, shuffled
symbolpairs = nchoosek(1:8,2); % 28 unique possible symbol pairs
npairs      = size(symbolpairs,1); % get pool to make sure not too many repeats
if nseq_condition <= npairs %if we need fewer pairs, take random pairs, no need to control > we'll only use to pilot loosely
    pool = symbolpairs(randperm(npairs, nseq_condition),:);
elseif nseq_condition > npairs % if we need more pairs, try to have least nb of repeats
    padsize = randperm(npairs, mod(nseq_condition,npairs)); % choose unique pairs to add up at the end
    pool = [repmat(symbolpairs, [1, floor(nseq_condition/npairs)]); symbolpairs(padsize,:)]; % pool as many full sets of all pairs + padding
end
pool = pool(randperm(size(pool,1)),:);

%%% get colordraws for the 48 seqs of condition (1)
colorvalues = COLEXP_expe_fx_get_linearcolorvalues(nsamples, counterfness, targetcolors, perr);


%% build sequences info in first condition (target):
for isqc = 1:nseq_condition
    
    sqc(isqc).genid = isqc; % this is used to test ordering within blocks
    
    sqc(isqc).subj = subj;
    
    sqc(isqc).condition = condition(isqc); 
    sqc(isqc).mirroring = isqc;
    sqc(isqc).seqgenerationidx  = isqc;

    sqc(isqc).nsamples = nsamples(isqc);
    
    sqc(isqc).counterfness = counterfness(isqc);
    
    sqc(isqc).targetcolor = targetcolors(isqc);
    
    sqc(isqc).symbolpair = pool(isqc,:);
    sqc(isqc).targetsymbol = pool(isqc,1);
    sqc(isqc).othersymbol = pool(isqc,2);
    
    % draw equalised-difficulty color values, and convert each to rgb
    sqc(isqc).colorslin.target = colorvalues(isqc).target';
    sqc(isqc).colorslin.other = colorvalues(isqc).other';
    sqc(isqc).colors.target = COLEXP_expe_fx_get_rgb(sqc(isqc).colorslin.target);
    sqc(isqc).colors.other = COLEXP_expe_fx_get_rgb(sqc(isqc).colorslin.other);

    % randomise symbol sides for each sample, max repeated side = 3
    nrepeated = 3; %
    symside = zeros(1,nsamples(isqc)); % init as many samples:
    while abs(diff(hist(symside,1:2)))>1 % while not equal number of 1s and 2s
        symside(1:nrepeated) = randi(2,1,nrepeated); %fill the first three
        for i = (nrepeated+1):nsamples(isqc) % loop on following ones:
            ipos = randi(2,1); % get one random
            if sum(symside((i-nrepeated):(i-1))==ipos)>=nrepeated %if last 3 were already == thisside
                symside(i) = 3-ipos;%take other side
            else; symside(i) = ipos;%else take this side
            end
        end
    end
    
    sqc(isqc).symbolsides = symside;
    sqc(isqc).instrside = instrside(isqc);
    sqc(isqc).probeside = probeside(isqc); 
    
end%end for first half of seq = condition #1

%% duplicate for 2nd condition (open condition = exact same sequences)

for isqc = nseq_condition+1 : NSEQ
    iduplicat                   = isqc - nseq_condition;
    
    sqc(isqc).genid = isqc;

    sqc(isqc).subj          = sqc(iduplicat).subj;

    sqc(isqc).condition     = condition(isqc);
    sqc(isqc).mirroring     = iduplicat;
    sqc(isqc).seqgenerationidx = isqc;

    sqc(isqc).nsamples      = sqc(iduplicat).nsamples;
    sqc(isqc).counterfness  = sqc(iduplicat).counterfness;
    sqc(isqc).targetcolor   = sqc(iduplicat).targetcolor;
    
    sqc(isqc).symbolpair    = sqc(iduplicat).symbolpair;
    sqc(isqc).targetsymbol  = sqc(iduplicat).targetsymbol;
    sqc(isqc).othersymbol   = sqc(iduplicat).othersymbol;
    
    sqc(isqc).colorslin     = sqc(iduplicat).colorslin;
    sqc(isqc).colors        = sqc(iduplicat).colors;
    
    sqc(isqc).symbolsides   = sqc(iduplicat).symbolsides; % side of symbols on screen at each sample
    sqc(isqc).instrside     = sqc(iduplicat).instrside; % side of symbols on screen at instructions screen
    sqc(isqc).probeside     = sqc(iduplicat).probeside; % final target/goodanswer side 

end%for second half = condition #2

fprintf('Done creating seqs!\n');

%%% adding optimal sampling:
sqc = COLEXP_expe_fx_get_optsampling(sqc);

%%% adding blocks and order in blocks: --also orders the seqs in block, within block
sqc = COLEXP_expe_fx_makeblocks(sqc);
sqc = COLEXP_expe_fx_orderblocks(sqc);

toc

end%function