function [training_sqc] = COLREP_fx_gen_training_sqc(perr)

%% making sequences for the training block:
% the function is for documenting the contents of training
% it was really only ran only once bc I ran all participants with the same training sequences.
% get_linearcolorvalues nb of iterations 1e3

nsamples        = [8 8 12 12 16 16 20 20];
conditions      = [1 2 1  2   1  2  1  2];
symmetries      = [1 3 2  3   1  3  2  3]; 
targetcolors    = [1 2 2  1   1  2  2  1];
symbols         = [1 2 3 4 5 6 7 8;...
                   8 1 7 2 6 3 5 4]';
instrsides      = randi(2,[1, numel(nsamples)]);
probesides      = randi(2,[1, numel(nsamples)]);
colorvalues     = COLREP_fx_get_linearcolorvalues(nsamples, symmetries, targetcolors, perr, 1e2);

%%%%%%% loop:
training_sqc = struct();
nseq = numel(nsamples);
for isqc = 1:nseq
    
    training_sqc(isqc).genid        = isqc;
    training_sqc(isqc).nsamples     = nsamples(isqc);
    training_sqc(isqc).condition    = conditions(isqc);
    training_sqc(isqc).symmetry     = symmetries(isqc);
    training_sqc(isqc).targetcolor  = targetcolors(isqc);
    training_sqc(isqc).symbolpair   = symbols(isqc,:);
    training_sqc(isqc).targetsymbol = training_sqc(isqc).symbolpair(1);
    training_sqc(isqc).othersymbol  = training_sqc(isqc).symbolpair(2);
    training_sqc(isqc).instrside    = instrsides(isqc); % instruction symbol sides
    training_sqc(isqc).probeside    = probesides(isqc); % final probe side
    training_sqc(isqc).blk          = 1;
    training_sqc(isqc).mirroring    = 0;
    training_sqc(isqc).colorslin    = colorvalues{isqc};
    
    % randomise symbol sides for each sample, max repeated side = 3
    nrepeated = 3; %
    symside = zeros(1,nsamples(isqc)); % init as many samples:
    while abs(diff(hist(symside,1:2)))>1 % while not equal number of 1s and 2s
        symside(1:nrepeated) = randi(2,1,nrepeated); %fill the first three
        for i = (nrepeated+1):nsamples(isqc) % loop on following ones:
            ipos = randi(2,1); % get one random
            if sum(symside((i-nrepeated):(i-1))==ipos)>=nrepeated %if last 3 were already == thisside
                symside(i) = 3-ipos;%take other side
            else; symside(i) = ipos;%else take this side
            end
        end
    end
    training_sqc(isqc).symbolsides = symside; % at each sample,
    
end

%%%
% shuffling sequences, with no more than twice same condition in a row:
nbackcondition = 2;
while true %while shuffling doesn't match constraints, reshuffle the sequences:
    shuffle = randperm(nseq,nseq);
    training_sqc = training_sqc(shuffle);
    
    %for each sequence check constraints
    training_sqc(1).blkseq = 1;
    for rnk = 2:nseq
        % check nback for condition and symmetry
        if rnk == 2 ; diffcond = 1; diffsymm = 1;
        else; diffcond = sum(abs(diff([training_sqc(rnk-nbackcondition:rnk).condition]))); 
              diffsymm = sum(abs(diff([training_sqc(rnk-nbackcondition:rnk).symmetry])));  
        end
        % for condition target and symmetry badbad, check 1back
        if training_sqc(rnk).condition == 1 && training_sqc(rnk).symmetry == 2 
                diffbadbad = ~ (training_sqc(rnk-1).symmetry == training_sqc(rnk).symmetry);
            else; diffbadbad = 1;
        end
        % check current symbol wasn't in previous pair
        symlast = sum(ismember(training_sqc(rnk-1).symbolpair, training_sqc(rnk).symbolpair));
        if symlast ~= 0 || diffcond == 0 || diffsymm == 0 || diffbadbad == 0 %if doesnt work, re-shuffle
            break %break and reshuffle
        end
        training_sqc(rnk).blkseq = rnk;
    end
    %if successfully checked all sequences, mission accomplished = break
    if rnk == nseq && symlast == 0 && diffcond ~= 0 && diffsymm ~= 0 && diffbadbad ~= 0
        break
    end
end%while this block is not good enough, shuffle again

%%% adding opt sampling behaviour:
training_sqc = COLREP_fx_get_optsampling(training_sqc);

%%% saving:
stampedname = sprintf('training_sqc_%s.mat', datestr(now,'ddmmyy_HHMM'));
save(stampedname, 'training_sqc');
save('training_sqc.mat', 'training_sqc');

fprintf('Done making training_sqc.mat at %s\n', datestr(now,'HH:MM'));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
