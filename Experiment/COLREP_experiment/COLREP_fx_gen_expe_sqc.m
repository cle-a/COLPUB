function [sqc] = COLREP_fx_gen_expe_sqc(subj, NSEQ, perr)

%% making sequences for the experimental blocks:
% NB: for the replication, we changed the proportion of good/bad/different sequences
% used to be 1/3 good-good, 1/3 bad-bad and 1/3 good-bad
% now 1/4 good-good, 1/4 bad-bad, 1/4 good bad, 1/4 bad good so half same half different

% (1) define conditions, length of sequences, symmetry conditions, target colors...
% (2) get color draws of equalised difficulty for each sequence (with function >>>fx_get_linearcolorvalues)
% (3) assort sequences for first condition
% (4) mirror them for second condition
% (5) add simulations of the optimal sampling behaviour (for scores >>>fx_get_optsampling)
% (6) divide sequences into blocks and order them within blocks (>>>fx_makeblocks >>>fx_orderblocks)

fprintf('Creating sequences from >>>fx_gen_sqc\n')
tic
if mod(NSEQ, 64) ~= 0
    warning('Cle: will not balance probeside with %d seq, choose multiple of 64 (64 128 192)\n', NSEQ);
end

%% I/ define conditions:

sqc = struct();

%%% target/open sampling condition = 1s or 2s
%%% this is defined for all the trials
nseqXcondition  = NSEQ/2;
conditions      = [repelem(1,nseqXcondition) repelem(2,nseqXcondition)];

%%% the following vectors are defined for only half the trials (condition 1)
%%% they're mirrored later for condition 2

%%% lengths of sequences: 8, 12, 16 or 20 samples per sequence:
lengths         = [8, 12, 16, 20];
nlengths        = numel(lengths);
nseqsXlength    = nseqXcondition/nlengths;
nsamples        = [repelem(lengths(1),nseqsXlength) repelem(lengths(2),nseqsXlength) repelem(lengths(3),nseqsXlength) repelem(lengths(4),nseqsXlength)];

%%% symmetry = whether or not the symbols are associated to same or different colors (for each length 1/4 good/good (1), 1/4 bad/bad (2), 1/2 counterfactual (3))
nsymXlength = nseqsXlength / 4;
symmetries  = repmat([repelem(1,nsymXlength),repelem(2,nsymXlength),repelem(3,nsymXlength*2)], [1,nlengths]);

%%% for each symmetry condition, half blue target half orange:
ntargetcolXsymmetry = nsymXlength/2;
targetcolors        = repmat([repelem(1,ntargetcolXsymmetry),repelem(2,ntargetcolXsymmetry)], [1,nseqsXlength]);

%%% side of the first/anchor symbol at final question (condition 2, open sampling):
respside = repmat([1 2], [1,nseqXcondition/2]);

%%% instuctionside is random, but mirrored:
instrside = randi(2,[1,nseqXcondition]);

%%% get a pool of symbol pairs with least amount of repeats, shuffled
% take the 28 possible pairs of the 8 symbols
% take as many full replications of these as possible (64/28 = 2.2)
% for the remaining pairs, randomly select pairs in the 28 that will be repeated a third time.
% // NB: the nb of occurence of a single symbol is not controlled \\
symbolpairs     = nchoosek(1:8,2);
npossiblepairs  = size(symbolpairs,1);
nneededpairs    = nseqXcondition;
paddingsize     = mod(nneededpairs, npossiblepairs);
pairspadding    = symbolpairs(randperm(npossiblepairs, paddingsize),:); % drawing N=paddingsize random pairs from the full set of possible pairs
symbolpairs     = [repmat(symbolpairs, [floor(nneededpairs/npossiblepairs),1]) ; pairspadding]; % concatenate AMRAP of the full set of pairs + the padding
symbolpairs     = symbolpairs(randperm(size(symbolpairs,1)),:);

%% II/ get color draws for the 48 seqs of condition (1):
linearcolorvalues = COLREP_fx_get_linearcolorvalues(nsamples, symmetries, targetcolors, perr, 1e3);

%% build sequences for first condition (target):
for isqc = 1:nseqXcondition
    
    sqc(isqc).genid       = isqc; % this is used to test ordering within blocks
    
    sqc(isqc).subj        = subj;
    
    sqc(isqc).condition   = conditions(isqc); 
    sqc(isqc).mirroring   = isqc;
    sqc(isqc).nsamples    = nsamples(isqc);
    sqc(isqc).symmetry    = symmetries(isqc);
    sqc(isqc).targetcolor = targetcolors(isqc);
    
    sqc(isqc).symbolpair   = symbolpairs(isqc,:);
    sqc(isqc).targetsymbol = symbolpairs(isqc,1);
    sqc(isqc).othersymbol  = symbolpairs(isqc,2);
    
    % draw equalised-difficulty color values, and convert each to rgb: 
    sqc(isqc).colorslin = linearcolorvalues{isqc};

    % randomise symbol sides for each sample, max repeated side = 3
    nrepeated = 3;
    symbside = zeros(1,nsamples(isqc)); % init as many samples:
    while abs(diff(hist(symbside,1:2)))>1 % while not equal number of 1s and 2s
        symbside(1:nrepeated) = randi(2,1,nrepeated); % fill the first three
        for i = (nrepeated+1):nsamples(isqc) % loop on following ones:
            ipos = randi(2,1); % get one random
            if sum(symbside((i-nrepeated):(i-1))==ipos)>=nrepeated % if last 3 were already == thisside
                symbside(i) = 3-ipos;% take other side
            else; symbside(i) = ipos;% else take this side
            end
        end
    end
    
    sqc(isqc).symbolsides = symbside;
    sqc(isqc).instrside   = instrside(isqc);
    sqc(isqc).probeside   = respside(isqc); 
    
end%end for first half of seq = condition #1

%% duplicate for 2nd condition (open condition = exact same sequences)

for isqc = nseqXcondition+1 : NSEQ
    iduplicat               = isqc - nseqXcondition;
    
    sqc(isqc).genid = isqc;

    sqc(isqc).subj          = sqc(iduplicat).subj;

    sqc(isqc).condition     = conditions(isqc);
    sqc(isqc).mirroring     = iduplicat;

    sqc(isqc).nsamples      = sqc(iduplicat).nsamples;
    sqc(isqc).symmetry      = sqc(iduplicat).symmetry;
    sqc(isqc).targetcolor   = sqc(iduplicat).targetcolor;
    
    sqc(isqc).symbolpair    = sqc(iduplicat).symbolpair;
    sqc(isqc).targetsymbol  = sqc(iduplicat).targetsymbol;
    sqc(isqc).othersymbol   = sqc(iduplicat).othersymbol;
    
    sqc(isqc).colorslin     = sqc(iduplicat).colorslin;
    
    sqc(isqc).symbolsides   = sqc(iduplicat).symbolsides; % side of symbols on screen at each sample
    sqc(isqc).instrside     = sqc(iduplicat).instrside;   % side of symbols on screen at instructions screen
    sqc(isqc).probeside     = sqc(iduplicat).probeside;   % final target/goodanswer side 

end%for second half = condition #2

%% add optimal sampling to each sequence:
sqc = COLREP_fx_get_optsampling(sqc);

%% make and order blocks:
%%% adding blocks and order in blocks: --also orders the sequences in block, within block
sqc = COLREP_fx_makeblocks(sqc);
sqc = COLREP_fx_orderblocks(sqc);


fprintf('Done creating seqs!\n');
toc

end%function