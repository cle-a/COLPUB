function  fx_checkblocks(sqc)

noksymrepeats = 8;
nokpairsrepeats = 4;

nblk = numel(unique([sqc.blk]));
for iblk = 1:nblk
    
    thisblk = sqc([sqc.blk] == iblk);
    
    %%%%% check composition:
    % conditions: 
    for ic = 1:3
        if sum([thisblk.condition] == ic) ~= 8; error('conditions not equalised'); end
        if sum([thisblk.condition] == ic & [thisblk.symmetry] == 1) ~= 2; error('symmetries not equalised'); end
        if sum([thisblk.condition] == ic & [thisblk.symmetry] == 2) ~= 2; error('symmetries not equalised'); end
        if sum([thisblk.condition] == ic & [thisblk.symmetry] == 3) ~= 4; error('symmetries not equalised'); end
    end
    
    % symbol repeats:
    maxsymreps  = max(histcounts([thisblk.symbolpair]));
    
    pairs             = reshape([thisblk.symbolpair], [2, size(thisblk,2) ] )';
    [~, ~, pairs_id]  = unique(pairs, 'rows');
    pairsoccurrences  = histcounts(pairs_id);
    
    if maxsymreps > noksymrepeats || max(pairsoccurrences) > nokpairsrepeats
        error('block %d doesn''t match! -a', iblk);
    end
    
    %%%%% check sequential constraints:
    for iseq = 2:numel(thisblk)
        
        % symbol = max 1
        if (thisblk(iseq).symbolpair == thisblk(iseq-1).symbolpair)
            error('seq %d in block %d doesn''t match! -symbol',iseq, iblk);
        end
        
        % badbad = max 1
        if thisblk(iseq).symmetry == 2 && thisblk(iseq-1).symmetry == 2
            error('seq %d in block %d doesn''t match! -symmetry badbdad',iseq, iblk);
        end
        
        % condition = max 2
        if iseq > 2 && sum(abs(diff([thisblk(iseq-2:iseq).condition]))) == 0
            error('seq %d in block %d doesn''t match! -condition',iseq, iblk);
        end
        
        % symmetry = max 3
        if iseq > 3 && ...
                (sum(abs(diff([thisblk(iseq-3:iseq).symmetry]))) == 0)
            error('seq %d in block %d doesn''t match! -symmetry',iseq, iblk);
        end
        
        % condition with final probe AND same probe side = max 3
        if iseq > 3 && ...
                (sum( [thisblk(iseq-3:iseq).condition] == 2 | [thisblk(iseq-3:iseq).condition] == 3 ) == 4) && ...
                (sum(abs(diff([thisblk(iseq-3:iseq).probeside]))) == 0)
            error('seq %d in block %d doesn''t match! -respsides for final probes',iseq, iblk);
        end
        
    end% checking each sequence in the block
    
    % checking I have drawn the right nb of colour samples:
    for iseq = 1:numel(thisblk)
        if thisblk(iseq).nsamples ~= size([thisblk(iseq).colorslin],2)
            error('seq %d in block %d doesn''t match! -wrong nb of color draws!',iseq, iblk);
        end
    end
    
end% checking each block

fprintf('all matching constraints\n')
end
