function [sqc] = fx_orderblocks(sqc)

% order blocks:
[sqc.blkseq] = deal([]);
temp         = sqc;
temp(:)      = [];
nblk         = numel(unique([sqc.blk]));

for iblk = 1:nblk
    fprintf('...ordering block #%d ...', iblk);
    thisblk = sqc([sqc.blk] == iblk);
    
    while true
        currentpool      = Shuffle(thisblk);
        tempblk          = thisblk;
        tempblk(:)       = [];
        
        for rnk = 1:numel(thisblk)
            
            % if first rnk, take first seq
            if rnk == 1
                mask = [1; zeros(numel(thisblk)-1,1)];
            end
            
            % check previous 1 sequence for symbol shape:
            if rnk >=2
                leftoversymbols = reshape([currentpool.symbolpair], [2, size(currentpool,2)]);
                mask            = sum(leftoversymbols ~= tempblk(rnk-1).symbolpair(1) & leftoversymbols ~= tempblk(rnk-1).symbolpair(2), 1 ) == 2;
            end
            
            % check previous 1 sequence for symmetry badbad:
            if rnk >=2 && tempblk(rnk-1).symmetry == 2
                mask = [currentpool.symmetry] ~= 2 & mask;
            end
            
            % check previous 2 sequences for condition:
            if rnk >=3 && ...
                    tempblk(rnk-2).condition == tempblk(rnk-1).condition
                mask  = [currentpool.condition] ~= tempblk(rnk-1).condition & mask;
            end          
            
            % check previous 3 sequences for symmetry, and if has final response for response side:
            if rnk >=4
                
                % if all previous 3 have the same colour, change symmetry
                if sum(abs(diff([tempblk(rnk-3:rnk-1).targetcolor]))) == 0
                    mask = [currentpool.targetcolor] ~= tempblk(rnk-1).symmetry & mask;
                end
                
                % if all previous 3 have the same symmetry, change symmetry
                if sum(abs(diff([tempblk(rnk-3:rnk-1).symmetry]))) == 0
                    mask = [currentpool.symmetry] ~= tempblk(rnk-1).symmetry & mask;
                end
                
                % if previous 3 are conditions with response side (2 or 3), if previous 3 also have same resp side, change side:
                if sum( [tempblk(rnk-3:rnk-1).condition] == 2 |  [tempblk(rnk-3:rnk-1).condition] == 3 )  == 3 && ... % all 3 were of condition 2 or 3
                        sum(abs(diff([tempblk(rnk-3:rnk-1).probeside]))) == 0                                         % all have response on the same side
                    mask = [currentpool.probeside] ~= tempblk(rnk-1).probeside & mask;
                end
                
            end
            
            % filter currentpool to match constraints encoded in mask, draw to tempblk and remove from currentpool:
            tempool = find(mask == 1);
            if isempty(tempool)
                currentpool = Shuffle(currentpool);
                tempblk(:)  = [];
                break
            elseif numel(tempool) > 0
                idraw               = tempool(randi(numel(tempool), 1));
                tempblk(rnk)        = currentpool(idraw);
                tempblk(rnk).blkseq = rnk;
                currentpool(idraw)  = [];
            end
            
            clearvars mask
            
        end% for all rnks
        
        % if we've gone through all ranks successfully, block is done:
        if numel(tempblk) == numel(thisblk)
            temp = [temp tempblk];
            break
        end
        
    end% if all rnks match
end% for each blk order rnks

sqc = temp;

end % def

