function [bestcolorvalues] = fx_get_linearcolorvalues(nsampleslist, symmetrieslist, targetcolorslist, perr, it)

%% returns linear values of color "intensities" to transform to real RGB colors afterwards:
% % % % % % % % % %
% % % % % % % % % %     takes:
% % % % % % % % % %     - nsampleslist: a list of all the lengths of all the sequences,
% % % % % % % % % %                     in the shape [8 8 8 8 8 8 8 8 8 8 8 8 12 12 12 12.. etc]
% % % % % % % % % %     - symmetrylist: list of all "symmetry" conditions of all sequences:
% % % % % % % % % %                        1 = good / good
% % % % % % % % % %                        2 = bad / bad 
% % % % % % % % % %                        3 = good / bad
% % % % % % % % % %                     in the shape [1 1 1 1 2 2 2 2 3 3 3 3 1 1 1 1 2 2 2 2 etc..]
% % % % % % % % % %     - targetcolorslist: list of all targetcolors
% % % % % % % % % %                     in the shape [1 2 1 2 1 2 1 2 etc..]
% % % % % % % % % %     - perr: probability of misclassification of the form based on a single sample/color
% % % % % % % % % %     - it: nb of color draws to take and model simulations to run on each color draw
% % % % % % % % % %
% % % % % % % % % %     returns:
% % % % % % % % % %     bestcolorvalues: structure of same length (= length(nsampleslist)),
% % % % % % % % % %     with fields:
% % % % % % % % % %                   - target: the color draws for target shape of the sequence
% % % % % % % % % %                   - other: color draws for the other shape of this sequence
% % % % % % % % % %                   - nsamples: n of samples in sequence ( = numel(other) )
% % % % % % % % % %                   - symmetry: condition of each sequence
% % % % % % % % % %                   - targetcolor: target color in sequence
% % % % % % % % % %
% % % % % % % % % %     NB: fields of the output str (esp. nsamples, symmetry and target) will be in the same order as the input lists


% (1) defines the probability distribution of the color
%       // NB: draws from the "high prob on 0-1" distribution
%              so for the positive color first (2), and flips it at the end if targetcolor was the negative (1) .\\
% (2) for each length-symmetry combination:
%       - get N = ndraws of length-many colors,
%       - run model simulation of sampling sequences on this color draw N = nmodelsimulations times,
%       - sort the color draws by (a) final amount of info accumulated, and (b) similarity to the original underlying color distribution
%         NB: "difficulty" is the median "amount of information" accumulated for the options abs()
%       - keep in store as many sequence draws as we have sequences of this lengthXsymmetry combination
%         flip the colors if the target was 1 (the negative color).
% // NB: RETURNS COLORS ON -1/+1 CONTINUUM => TRANSFORM TO ISOLUMINANT RGB AFTERWARDS \\

fprintf('---> Drawing color values! >>>fx_get_linearcolorvalues\n')

%% parameters:

%init:
lengthXsymmetry  = unique([nsampleslist' symmetrieslist'],'rows', 'stable'); % for each nsamples*CF condition
bestcolorvalues  = num2cell(NaN(size(nsampleslist,2),1));
str = struct();

% model parameters:
alpha = 1;
sigma = 0.5;

% probability distribution of the colors: here p1 is high prob on the 0/+1 range so we're drawing from the positive color first:
x     = -1:0.002:+1;
x     = 0.5*(x(1:end-1)+x(2:end)); % all color values: -1 to +1
b     = fzero(@(p)trapz(x(x > 0),1./(1+exp(x(x > 0)*p))/trapz(x,1./(1+exp(x*p))))-perr,[0,1000]);
% computes ratio between area under the curve after 0 / total area under the curve - perr
% leads ratio to 1 to get the beta that leads the curve to have exactly perr area under the curve before zero
p1    = 1./(1+exp(-x*b)); % probabilities of each color (x) for shape1
p1    = p1/trapz(x,p1); % divided by sum of probs => 0 to 1

%% I// draw colors & simulate discriminability/difficulty based on model learning => nsim times:
stored = 0;
  
all              = struct();
for ilengthXsymm = 1:size(lengthXsymmetry,1)
        
    nsamples        = lengthXsymmetry(ilengthXsymm, 1);
    symmetry        = lengthXsymmetry(ilengthXsymm, 2);
    
    ncolsim         = it; % nb of color draws simulations
    nmodsim         = it; % nb of model simulations to run on each color draw
    colorvalues     = nan(ncolsim, nsamples, 2);
    drawncolval     = nan(2, nsamples);
    dval            = nan(ncolsim, nmodsim, nsamples, 3); % decision values to compute ranks on
    colrnk          = nan(ncolsim, nsamples);
    colors          = nan(ncolsim, nsamples);

    for icolsim = 1:ncolsim
        if mod(icolsim,250) == 0; fprintf('...\n'); end
        
        % init for current color simulation:
        samplings = zeros(1, nsamples, 3); % sampling response symbol (which form to look at)
        learntval = nan(2, nsamples+1, 3); % expected/infered/learnt color of each symbol
        seenval   = nan(2, nsamples, 3); % actual colors values *seen* by model: drawn(sampled)
        
        %%% (a) draw colvals for this round of simulations depending on condition:
        colors(icolsim, :) = itrnd([1,nsamples],x,p1,'exact');
        switch symmetry
            case 1; drawncolval(1,:) = colors(icolsim, :);        drawncolval(2,:) = drawncolval(1,:); % goodgood
            case 2; drawncolval(1,:) = (-1) * colors(icolsim, :); drawncolval(2,:) = drawncolval(1,:); % badbad
            case 3; drawncolval(1,:) = colors(icolsim, :);        drawncolval(2,:) = (-1) * drawncolval(1,:); %goodbad
%             case 4; drawncolval(1,:) = (-1) * colors(icolsim, :); drawncolval(2,:) = colors(icolsim, :); %badgood
        end
        colorvalues(icolsim, :, :) = drawncolval';
        
        %%% (b) run model simulations N = nmodsim, on each color draw simulation:
        for imodsim = 1:nmodsim          
            for ismp = 1:nsamples
                
                %%% choose symbol to sample from
                if ismp == 1 % if first one, choose randomly
                    ro = randi(2,1);
                    tr = ro;
                    samplings(:,ismp,:) = ro; % for row one = random but same
                    learntval(:,ismp,:) = 0;
                    dval(icolsim, imodsim, ismp, :) = 0;
                elseif ismp > 1
                    % cond1: seeks orange:
                    [trv, tr] = max(learntval(:,ismp,1),[],1); % orange = 1 = max
                    samplings(:,ismp,1) = tr;
                    dval(icolsim, imodsim, ismp, 1) = trv;
                    
                    % cond2: symbol with least information:
                    [rov, ro] = min(abs(learntval(:,ismp,2)),[],1); % min abs learnt value
                    samplings(:,ismp,2) = ro;
                    dval(icolsim, imodsim, ismp, 2) = rov;
                end %choosing which symbol to sample from

                %%% learn = accumulate color values:
                seenval(tr, ismp,1) = drawncolval(tr, ismp);
                seenval(ro, ismp,2) = drawncolval(ro, ismp);
                
                learntval(tr,ismp+1,1) = alpha * learntval(tr,ismp,1) + ((sigma * randn(1)) + seenval(tr,ismp,1));
                learntval(ro,ismp+1,2) = alpha * learntval(ro,ismp,2) + ((sigma * randn(1)) + seenval(ro,ismp,2));
                
                learntval(3-tr,ismp+1,1) = learntval(3-tr,ismp,1);
                learntval(3-ro,ismp+1,2) = learntval(3-ro,ismp,2);  
            end%for each sample, choose and learn
        end%for imodsim: model simulations on this color draw
        
        %%% for each colsim, take median value learnt by models for each sample (median per sample)
        colrnk(icolsim,:,1) = median(dval(icolsim,:,:,1),2);
        colrnk(icolsim,:,2) = median(dval(icolsim,:,:,2),2);
    end%for each color draw simulation, running model N=nmodsim times
    
    %%% (c) rank colordraws simulations & sum ranks (ranks are distance between diff of each draw and mean diff of all draws)
    arnk = nan(ncolsim,nsamples,2);
    for ismp = 1:nsamples % at each sample 
        arnk(:,ismp,1) = tiedrank( abs ( colrnk(:,ismp,1) - mean(colrnk(:,ismp,1)) )); % difficulty rank in the target condition
        arnk(:,ismp,2) = tiedrank( abs ( colrnk(:,ismp,2) - mean(colrnk(:,ismp,2)) )); % difficulty rank in the open condition
    end
    srnk  = squeeze(sum(arnk,2)); % for each colsim, sum of the ranks of all the samples
    all(ilengthXsymm).arnk = arnk;
    all(ilengthXsymm).srnk = srnk;
    all(ilengthXsymm).colrnk = colrnk;
    
    %%% (d) add constraint on color distribution for each block:
    mucol = tiedrank( abs (  mean(colors,2)  - mean(mean(colors,2)) ) ) ;
    sdcol = tiedrank( abs (  std(colors,0,2) - mean(std(colors,0,2)) ) ) ;
    scrnk = (mucol + sdcol);
    all(ilengthXsymm).scrnk = scrnk;
    
    %%% (e) pick color series whose sum of ranks is lowest & store them
    npck  = sum(nsampleslist == nsamples & symmetrieslist == symmetry);
    [~,i] = sort(tiedrank(srnk(:,1)) + tiedrank(srnk(:,2)) + tiedrank(scrnk)); % sort sims by sum of ranks summed across smp and conditions
    pk = i(1:npck);
    all(ilengthXsymm).colors    = colorvalues(pk, :, :);
    all(ilengthXsymm).allcolors = colorvalues(:, :, 1);
    all(ilengthXsymm).iselected = pk;
    
    %%% (f) store selected color values and conditions
    % going back to the initial targetcolorslist (not lengthXsymmetry)
    for ist = 1:npck % store final colvals
        stored = stored +1;
        if targetcolorslist(stored) == 1 % target is the negative color
            bestcolorvalues{stored} = [colorvalues(pk(ist),:,1) .* (-1); colorvalues(pk(ist),:,2) .* (-1)];
        elseif targetcolorslist(stored) == 2 % target is the positive color (which we generated)
            bestcolorvalues{stored} = [colorvalues(pk(ist),:,1); colorvalues(pk(ist),:,2)];
        end
        str(stored).nsmp    = nsamples;
        str(stored).symm    = symmetry;
        str(stored).tcol    = targetcolorslist(stored);
    end
    
fprintf('...done with length %d & symmetry %d...\n', nsamples, symmetry);
end%for each length x symmerty condition

fprintf('Done choosing color values!\n')


%% ranks correlation
% 
% figure ()
% lcol = lines;
% for ilengthXsymm = 1:size(lengthXsymmetry,1)
%     subplot(4,4,ilengthXsymm)
%     
%     % plot all the ranks:
%     scatter(all(ilengthXsymm).srnk(:,1), all(ilengthXsymm).srnk(:,2), '.')
%     xlim([0 1500]);
%     ylim([0 1500]);
%     hold on
%     
%     % draw corr lines:
%     x = [0:1:1500];
%     plot(x, x, '-r');
%     ls=lsline;
%     ls.Color = lcol(1,:);
%     
%     % highlight selected draws:
%     selected = all(ilengthXsymm).iselected;
%     scatter(all(ilengthXsymm).srnk(selected,1), all(ilengthXsymm).srnk(selected,2), 'MarkerEdgeColor', [1, 0, 0])
%     
%     xlabel('dir', 'FontWeight','normal');
%     ylabel('opn', 'FontWeight','normal');
%     title(['nsmp ' num2str(lengthXsymmetry(ilengthXsymm,1)) ' symm ' num2str(lengthXsymmetry(ilengthXsymm,2))], 'FontWeight', 'Normal');
% 
% end


%% 3D figure
% 
% figure ()
% lcol = lines;
% for ilengthXsymm = 1:size(lengthXsymmetry,1)
%     subplot(4,4,ilengthXsymm)
%     
%     % plot all the ranks:
%     scatter3(all(ilengthXsymm).srnk(:,1), all(ilengthXsymm).srnk(:,2), all(ilengthXsymm).scrnk, '.')
% %     contour([1:100]', [1:100]', [all(ilengthXsymm).srnk(:,1) all(ilengthXsymm).srnk(:,2) all(ilengthXsymm).scrnk]')
%     xlim([0 1500]);
%     ylim([0 1500]);
%     hold on
% 
%     % highlight selected draws:
%     selected = all(ilengthXsymm).iselected;
%     scatter3(all(ilengthXsymm).srnk(selected,1), all(ilengthXsymm).srnk(selected,2), all(ilengthXsymm).scrnk(selected), 'MarkerEdgeColor', [1, 0, 0])
%     
%     xlabel('dirdiff', 'FontWeight','normal');
%     ylabel('opndiff', 'FontWeight','normal');
%     zlabel('thprox', 'FontWeight','normal');
%     title(['nsmp ' num2str(lengthXsymmetry(ilengthXsymm,1)) ' symm ' num2str(lengthXsymmetry(ilengthXsymm,2))], 'FontWeight', 'Normal');
% 
% end


%% kdensities
% figure()
% for ilengthXsymm = 1:size(lengthXsymmetry,1)
%     subplot(4,4,ilengthXsymm);
%     
%     % pdf of all draws in grey dots:
%     draws = all(ilengthXsymm).allcolors;
%     if lengthXsymmetry(ilengthXsymm,2) == 2 || lengthXsymmetry(ilengthXsymm,2) == 4
%         draws = -1 * draws;
%     end
%     for idraw = 1:size(draws,1)
%         d  = draws(idraw,:);
%         bw = 0.9 * min(std(d), iqr(d)/1.34) * size(d,2)^(-1/5);
%         [f,xi] = ksdensity(d, 'Bandwidth', bw); 
%         plot(xi, f, ':k')
%         xlim([-1 1]);
%         ylim([-0.5 1.5]);
%         hold on    
%     end
%     
%     % average draw in blue:
%     d = reshape(draws, size(draws,2)*size(draws,1), 1);
%     bw = 0.9 * min(std(d), iqr(d)/1.34) * size(d,2)^(-1/5);
%     [f,xi] = ksdensity(d, 'Bandwidth', bw); 
%     plot(xi, f, '-g', 'LineWidth', 2);
%     plot(d)
% 
%     % selected draws in green:
%     draws = all(ilengthXsymm).colors(:,:,1);
%     if lengthXsymmetry(ilengthXsymm,2) == 2 || lengthXsymmetry(ilengthXsymm,2) == 4
%         draws = -1 * draws;
%     end
%     for idraw = 1:size(draws,1)
%         d  = draws(idraw,:);
%         bw = 0.9 * min(std(d), iqr(d)/1.34) * size(d,2)^(-1/5);
%         [f,xi] = ksdensity(d, 'Bandwidth', bw); 
%         plot(xi, f, '-b', 'LineWidth', 1)
%         xlim([-1 1]);
%         hold on    
%     end
%     d = reshape(draws, size(draws,2)*size(draws,1), 1);
%     bw = 0.9 * min(std(d), iqr(d)/1.34) * size(d,2)^(-1/5);
%     [f,xi] = ksdensity(d, 'Bandwidth', bw); 
%     plot(xi, f, '--c', 'LineWidth', 1);
%     plot(d)
%     
%     % theorethical in red:
%     x = -1:0.002:+1;
%     x = 0.5*(x(1:end-1)+x(2:end)); %all color values: -1 to +1
%     b = fzero(@(p)trapz(x(x > 0),1./(1+exp(x(x > 0)*p))/trapz(x,1./(1+exp(x*p))))-perr,[0,1000]);
%     p1 = 1./(1+exp(-x*b)); % probabilities of each color (x) for shape
%     p1 = p1/trapz(x,p1);
%     plot(x, p1, '--r');
%     
%    title(['nsmp ' num2str(lengthXsymmetry(ilengthXsymm,1)) ' symm ' num2str(lengthXsymmetry(ilengthXsymm,2))], 'FontWeight', 'Normal');
% end



end%function def
 
