%%% COLMID make the sequence structures for all the participants 
%%% generate sequences, make balanced blocks, order blocks, check blocks
%%%
%%% NB: for clarity this version of the script will save generated sequences in
%%%     a Data subfolder _within Master/Experiment/Experiment_COLMID folder_

nsubj    = 1;
perr     = 1/3; % should be 1/3 for the task
colgenit = 1e3; % should be 1e3 (nb of iterations for gen color draws)

% for each participant:
for isubj = 1:nsubj
    fprintf('\n\n\n\n\n------------------\n Subj # %d %s \n------------------\n', isubj, datestr(datetime('now')));
        
    % get directory
    ti = GetSecs;
    fprintf('%s\n', datestr(now,'HH:MM'))
    subjdir = sprintf('../Data/S%02d', isubj);
        if ~exist(subjdir,'dir')
            mkdir(subjdir);
        elseif exist(subjdir,'dir') % if dir already exists: crash the whole thing -> check.
            error('there is already subj data in the target directory: check Data/S%02d', isubj)
        end
    
    % create sequences and blocks
        while true
            fprintf('Creating sequences ...\n')
            sqc = fx_makesqc(isubj, perr, colgenit);
            
            sqc = fx_makeblocks(sqc);
            if ~isempty(sqc)
                sqc = fx_orderblocks(sqc);
                break
            end
        end

    fx_checkblocks(sqc);
    
    % save
    sqc = orderfields(sqc, {'subj', 'genid', 'mirroring', 'blk', 'blkseq', 'condition', 'symmetry', 'nsamples', 'targetcolor', 'probeside', 'instrside', 'symbolpair', 'colorslin', 'symbolsides'});
    save(sprintf('%s/S%02d_sqc.mat', subjdir, isubj), 'sqc');
    
    to = GetSecs;
    fprintf('subj %d took %.f min \n', isubj, (to-ti)/60)
    
end% per subj