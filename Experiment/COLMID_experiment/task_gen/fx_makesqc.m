function [sqc] = fx_makesqc(isubj, perr, colgenit)
% returns sqc structure of the sequences - no blocks


% basic conditions of the sequences:
% perr             = 1/3;
% colgenit         = 1e1; % should be 1e3
NSEQUENCES       = 96;
nconditions      = 3;
nlengths         = 4;
nsymetries       = 4; % actually 3, but we generate twice as many "different" as "same" (good or bad)

nseq_percondition = NSEQUENCES/nconditions;
nseq_perlength    = NSEQUENCES/nconditions/nlengths;
nseq_persymetry   = NSEQUENCES/nconditions/nlengths/nsymetries;

nsamples         = [repelem(8, nseq_perlength) repelem(12, nseq_perlength) repelem(16, nseq_perlength) repelem(20, nseq_perlength)];
symmetries       = repmat( [repelem(1, nseq_persymetry) repelem(2, nseq_persymetry) repelem(3, nseq_persymetry) repelem(3, nseq_persymetry)], [1,nlengths] );
instrcolours     = repmat(  repmat( [1 2], [1, nsymetries]), [1, nlengths]);

% instuctionside and respsides are random but identical in all three conditions:
instrsides = randi(2, [1, nseq_percondition]);

% we check that probesides are not too imbalanced, also check per targetcolour:
while true
    respsides  = randi(2, [1, nseq_percondition]);
    % checking that sides are not too imbalanced globally: 45<>55%
    if (sum(respsides==1)/nseq_percondition*100) > 45 || (sum(respsides==1)/nseq_percondition*100) < 55
        % checking that sides are not too imbalanced per targetcolour: 6<>10
        if sum(instrcolours == 1 & respsides == 1) > 6 && sum(instrcolours == 1 & respsides == 1) < 10 && ...
           sum(instrcolours == 1 & respsides == 2) > 6 && sum(instrcolours == 1 & respsides == 2) < 10 && ...
           sum(instrcolours == 2 & respsides == 1) > 6 && sum(instrcolours == 2 & respsides == 1) < 10 && ...
           sum(instrcolours == 2 & respsides == 2) > 6 && sum(instrcolours == 2 & respsides == 2) < 10
            break
        end
    end
end

% take as many unique reps as possible + padding, shuffle // NB: the nb of occurences of a single symbol is not controlled \\
symbolpairs    = nchoosek(1:8,2);
npossiblepairs = size(symbolpairs,1);
nneededpairs   = nseq_percondition;
paddingsize    = mod(nneededpairs, npossiblepairs);
pairspadding   = symbolpairs(randperm(npossiblepairs, paddingsize),:); % drawing N = paddingsize random pairs from the full set of possible pairs
symbolpairs    = [repmat(symbolpairs, [floor(nneededpairs/npossiblepairs),1]) ; pairspadding]; % concatenate AMRAP of the full set of pairs + the padding
symbolpairs    = symbolpairs(randperm(size(symbolpairs,1)),:)';

% get linear color values:
linearcolorvalues = fx_get_linearcolorvalues(nsamples, symmetries, instrcolours, perr, colgenit)';

% get response sides at each trial/sample of each sequence:
symbolsides = num2cell(nan(1,nseq_percondition));
for is = 1:nseq_percondition
    nrepeated = 3;
    symbside = zeros(1,nsamples(is)); % init as many samples:
    while abs(diff(hist(symbside,1:2))) > 1 % while not equal number of 1s and 2s
        symbside(1:nrepeated) = randi(2,1,nrepeated); % fill the first three
        for i = (nrepeated+1):nsamples(is) % loop on following ones:
            ipos = randi(2,1);             % get one random
            if sum(symbside((i-nrepeated):(i-1))==ipos)>=nrepeated % if last 3 were already == thisside
                symbside(i) = 3-ipos; % take other side
            else; symbside(i) = ipos; % else take this side
            end
        end
    end
    symbolsides{is} = symbside;
end

% replicate the same data in the other two conditions:
cl(:,1)  = num2cell( repelem(isubj, NSEQUENCES) );                      % subj
cl(:,2)  = num2cell( 1:NSEQUENCES );                                    % genid
cl(:,3)  = num2cell( repmat(1:nseq_percondition, [1, nconditions]) );   % mirroring id
cl(:,4)  = num2cell( [repelem(1, nseq_percondition) repelem(2, nseq_percondition) repelem(3, nseq_percondition)] ); % condition 1 = DRAW, 2 = GUESS, 3 = FIND
cl(:,5)  = num2cell( repmat(nsamples, [1, nconditions]) );              % nb of samples in the sequence
cl(:,6)  = num2cell( repmat(symmetries, [1, nconditions]) );            % symmetry  1 = good/good, 2 = bad/bad, 3=good/bad
cl(:,7)  = num2cell( repmat(instrcolours, [1, nconditions]) );          % colour (targetcolour as generated) 1 = blue, 2 = orange
cl(:,8)  = num2cell( repmat(respsides, [1, nconditions]) );             % side of target symbol on final probe 1 = left, 2 = right
cl(:,9)  = num2cell( [symbolpairs symbolpairs symbolpairs]', 2);        % pair of symbols to be used for each sequence
cl(:,10) = num2cell( repmat(instrsides, [1,3]) );                      % side of the target symbol for the sequence instructions screen 1 = left, 2 = right
cl(:,11) = [symbolsides symbolsides symbolsides];                       % side of the target symbol in each trial/sample
cl(:,12) = [linearcolorvalues linearcolorvalues linearcolorvalues];     % linear colour values for both symbols (1st is target, 2nd is other),
% transformed to rgb in display script

% convert to structure with fieldnames matching display script:
sqc = cell2struct(cl, {'subj', 'genid', 'mirroring', 'condition', 'nsamples', 'symmetry', 'targetcolor', 'probeside', 'symbolpair', 'instrside', 'symbolsides', 'colorslin'}, 2)';