%% making sequences for the training block:
% the function is for documenting the contents of training
% it was ran only once bc I ran all participants with the same training sequences
% perr     = prob of error in drawing the colours for each sequence = should be 1/3
% colgenit = nb of iterations of the sequences to draw from when drawing the colours for each sequence = should be 1e3

perr     = 1/3;
colgenit = 1e3;


nsamples        = [8  8  12  12  12  16  16  20  20];
conditions      = [1  2  2   3   1   3   1   2   3];
symmetries      = [1  1  3   2   2   3   3   3   3]; 
targetcolors    = [1  1  2   2   1   1   2   2   1];

symbols         = [1 3 4 5 2 6 7 8 1;...
                   8 7 2 6 1 3 5 4 3]';
instrsides      = randi(2,[1, numel(nsamples)]);
probesides      = randi(2,[1, numel(nsamples)]);
colorvalues     = fx_get_linearcolorvalues(nsamples, symmetries, targetcolors, perr, colgenit);

symbolsides = num2cell(nan(1, numel(nsamples)));
for is = 1:numel(nsamples)
    nrepeated = 3;
    symbside = zeros(1,nsamples(is)); % init as many samples:
    while abs(diff(hist(symbside,1:2))) > 1 % while not equal number of 1s and 2s
        symbside(1:nrepeated) = randi(2,1,nrepeated); % fill the first three
        for i = (nrepeated+1):nsamples(is) % loop on following ones:
            ipos = randi(2,1);             % get one random
            if sum(symbside((i-nrepeated):(i-1))==ipos)>=nrepeated % if last 3 were already == thisside
                symbside(i) = 3-ipos; % take other side
            else; symbside(i) = ipos; % else take this side
            end
        end
    end
    symbolsides{is} = symbside;
end

% convert to structure with fieldnames matching display script:
cl(:,1)  = num2cell( repelem(0, numel(nsamples)) );  %
cl(:,2)  = num2cell( 1:numel(nsamples) );            % genid
cl(:,3)  = num2cell( repelem(0, numel(nsamples)) );  % mirroring id
cl(:,4)  = num2cell( conditions );                   % condition 1 = DRAW, 2 = GUESS, 3 = FIND
cl(:,5)  = num2cell( nsamples );                     % nb of samples in the sequence
cl(:,6)  = num2cell( symmetries );                   % symmetry  1 = good/good, 2 = bad/bad, 3=good/bad
cl(:,7)  = num2cell( targetcolors);                  % colour (targetcolour as generated) 1 = blue, 2 = orange
cl(:,8)  = num2cell( probesides);                    % side of target symbol on final probe 1 = left, 2 = right
cl(:,9)  = num2cell( symbols, 2);                    % pair of symbols to be used for each sequence
cl(:,10) = num2cell( instrsides );                   % side of the target symbol for the sequence instructions screen 1 = left, 2 = right
cl(:,11) = symbolsides;                              % side of the target symbol in each trial/sample
cl(:,12) = colorvalues;                              % linear colour values for both symbols (1st is target, 2nd is other), transformed to rgb in display script
training_sqc = cell2struct(cl, {'subj', 'genid', 'mirroring', 'condition', 'nsamples', 'symmetry', 'targetcolor', 'probeside', 'symbolpair', 'instrside', 'symbolsides', 'colorslin'}, 2)';

% order:
[training_sqc.blk] = deal(1);
training_sqc = fx_orderblocks(training_sqc);

%%% saving:
stampedname = sprintf('../training_sqc_%s.mat', datestr(now,'ddmmyy_HHMM'));
save(stampedname, 'training_sqc');
save('../training_sqc.mat', 'training_sqc');

fprintf('Done making training_sqc.mat at %s\n', datestr(now,'HH:MM'));


