function [sqc] = fx_makeblocks(sqc)

nblk = 4;

% make blocks:
maxtry = 30;
niutry = 0;

while true
    fprintf('...making blocks...');
    niutry = niutry + 1;
    if niutry > maxtry
        sqc = [];
        break
    end
    
    [sqc.blk]    = deal([]);
    currentpool  = sqc;
    temp         = sqc;
    temp(:)      = [];
    
    for iblk = 1:nblk
        countit = 0;
        maxit   = 1e3;
        
        while true
            countit = countit+1;
            hibnd   = 66;
            lobnd   = 33;
            
            % draw sequences of the right conditionXsymmetry combination:
            temppool   = currentpool;
            thisblk    = sqc;
            thisblk(:) = [];
            for icond = 1:3
                for isym = 1:3
                    if isym == 1 || isym == 2; ndraw = 2; elseif isym == 3; ndraw = 4; end
                    drawingpool     = Shuffle( find([temppool.condition] == icond & [temppool.symmetry] == isym)  );
                    idrawnseq       = drawingpool(1:ndraw);
                    thisdraw        = temppool(idrawnseq);
                    thisblk         = [thisblk thisdraw];
                end
            end
            
            % check nb of targetcolours is balanced in each condition:
            colok  = 1;
            if sum([thisblk.targetcolor] == 1) ~= (numel([thisblk.targetcolor]) *50 / 100)
                colok = 0;
            end
            for icond = 1:3
                hipct = sum([thisblk.condition] == icond) * hibnd / 100; % repeated to adapt to the different Ns in cond 1-2 ≠ 3
                lopct = sum([thisblk.condition] == icond) * lobnd / 100;
                if sum([thisblk([thisblk.condition] == icond).targetcolor] == 1) < lopct || ...
                        sum([thisblk([thisblk.condition] == icond).targetcolor] == 1) > hipct
                    colok = 0;
                end
            end
            
            % check the side of the response is balanced in each targetcolour and in each condition:
            sideok = 1;
            for icol = 1:2
                hipct = sum([thisblk.targetcolor] == icol) * hibnd / 100;
                lopct = sum([thisblk.targetcolor] == icol) * lobnd / 100;
                if sum([thisblk([thisblk.targetcolor] == icol).probeside] == 1) < lopct || ...
                        sum([thisblk([thisblk.targetcolor] == icol).probeside] == 1) > hipct
                    sideok = 0;
                end
            end
            for icond = 1:3
                hipct = sum([thisblk.condition] == icond) * hibnd / 100;
                lopct = sum([thisblk.condition] == icond) * lobnd / 100;
                if sum([thisblk([thisblk.condition] == icond).probeside] == 1) < lopct || ...
                        sum([thisblk([thisblk.condition] == icond).probeside] == 1) > hipct
                    sideok = 0;
                end
            end
            
            % check we don't repeat too many times the same symbol in each block:
            symbok = 1;
            noksymrepeats   = 8;
            nokpairsrepeats = 4;
            
            if max(histcounts([thisblk.symbolpair])) > noksymrepeats % check if max nb of occurrences of same *symbol* is over threshold
                symbok = 0;
            end
            
            pairs   = reshape([thisblk.symbolpair], [2, size(thisblk,2) ] )';   % get pairs from drawnseqs
            [~, ~, pairs_id] = unique(pairs, 'rows');                           % identical pairs are put in same bin/given an 'id'
            if max(histcounts(pairs_id, unique(pairs_id))) > nokpairsrepeats    % count occurrences of each id = repetitions of the symb pair
                                                                                % arg2= use unique(pairs_id) as binedges... not sure that's nec. since integers?
                symbok = 0;
            end
            
            % assess constraints:
            if colok && sideok && symbok
                %fprintf('...done block %d', iblk);
                [thisblk.blk] = deal(iblk);
                temp          = [temp thisblk];
                for irm = [thisblk.genid]
                    currentpool([currentpool.genid] == irm) = [];
                end
                break % out of the blockwise while = this block checks constraints, move to next
                
            elseif (iblk < nblk && countit > maxit)
                currentpool = sqc;
                temp        = [];
                iblk        = 1; % go back to block1
                fprintf('...timeout block %d \n', iblk)
                break
                
            elseif (iblk == nblk && ~(colok && sideok && symbok) && countit > maxit) % if at 4th block, too many of same symbol, restart at block 1
                currentpool = sqc;
                temp        = [];
                iblk        = 1; % go back to block1
                fprintf('...!!! BREAKING AT LAST BLOCK !!!\n')
                break
            end % if checking conditions for this block: either break or redo this block
            
        end % while this blk not good enough, retry
        
    end % for each block
    
    if isempty(currentpool)
        fprintf('...done making blocks\n')
        sqc = temp;
        break % out of the while making blocks loop
    end
    
end % while not all blocks are done

end % def