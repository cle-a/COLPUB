%%% runs the replication blocs (training or expe)
% INPUT: sqc     = an sqc structure, output of fx_gen_expe_sqc
%        subj    = the subj nb
%        session = 'training' or 'expe'
%        testing = 1 or 0 = whether debugging or not (whether stops spilling keys, hide cursor, display times...)
%
% OUTPUTS: an expout structure with fields header, sqc, video, sampling, final
%
% NB: native KB keys are not inhibited: don't press a | p keys!
% iblock    = block idx in nb of blocks
% iseq      = sequence idx in nb of sequences in this block == blkseq
% isqc      = sequence idx out of total nb of sequences in the experiment struct (sqc)
% ismp      = sample idx in nb of samples in this sequence
% isamples  = sample idx out of total nb of samples in the experiment

function expout = COLMID_fx_run_expe(sqc, subj, session, testing)

try
    
    %% Add IO toolbox, init subj & set running parameters:
    
    addpath ./Toolboxes/IO % Valentin's toolbox in Experiment's path
    
    subjdir = sprintf('Data/S%02d',subj);
    diary(sprintf('Data/S%02d/logS%02d_%s_%s.txt', subj, subj, session, datestr(now,'mmdd_HHMM')));
    
    % create output structures:
    header.subj = subj;
    header.start = datestr(now,'yyyymmdd-HHMM');
    header.aborted = 1; % changed to 0 if completes
    sampling = struct();
    final = struct();
    ME = struct();
    
    video.testingON = testing;
    video.synchflip = true;
    video.mouseON   = false;
    video.session   = session;
    
    %%% Experimental visual parameters:
    if ispc % in 2nd floor booths/downstairs
        video.screenwdth_cm = 66;
        video.screenwdth_px = 1920;
        video.screendistance_mm = 550;
        video.ppd = round(tand(1) * video.screendistance_mm / 10/ video.screenwdth_cm * video.screenwdth_px);
        %    dist_cm - screen-eye distance in centimeters
        %    wdth_cm - screen width in centimeters
        %    wdth_px - screen width in pixels (native resolution)
    elseif ismac % testing non mac, doesnt care.
        video.ppd = 40; % number of screen pixels per degree of visual angle
    end
    video.bag_sz            = 7*video.ppd;
    video.shape_sz          = 5*video.ppd;  % video.ppd pixels per degree of visual angle = 40
    video.shape_offset      = 6*video.ppd;  % shape offset
    video.respbutton_offset = 12*video.ppd; % response button offset
    video.rep_sz            = 5*video.ppd;
    video.fb_sz             = 6*video.ppd;  % selection square size
    video.cup_sz            = 6*video.ppd;
    black                   = [0,0,0];
    white                   = [1,1,1];
    video.lbgd              = 0.3950;
    orange                  = [1,0.5,0];
    blue                    = [0,0.5,1];
    rgbcolors               = [blue; orange];
    
    %%% which screens?
    screens = Screen('Screens');
    video.screen = max(screens); %screen index on PC: 0=extended, 1=native, 2=ext
    
    %%% set synch properties:
    if video.synchflip && ispc
        % set screen synchronization properties -- workaround Valentin for PC
        % see 'help SyncTrouble',
        %     'help BeampositionQueries' or
        %     'help ConserveVRAMSettings' for more information
        Screen('Preference','VisualDebuglevel',3); % verbosity
        Screen('Preference','SyncTestSettings',[],[],0.2,10); % soften synchronization test requirements
        Screen('Preference','ConserveVRAM',bitor(4096,Screen('Preference','ConserveVRAM'))); % enforce beamposition workaround for missing VBL interval
        fprintf('Synching flips with softer requirements...\n');
    elseif ~video.synchflip || ismac
        % skip synchronization tests altogether
        % //!\\ force skipping tests, PTB wont work = timing inaccurate
        Screen('Preference','SkipSyncTests',2); % assumes 60Hz etc..
        Screen('Preference','VisualDebuglevel',0);
        Screen('Preference','SuppressAllWarnings',1);
        fprintf('||| SYNCHFLIP OFF or running on OSX => TIMINGS WILL BE INACCURATE! |||\n')
    end
    
    %%% open main window and set PTB technical display parameters
    PsychImaging('PrepareConfiguration');
    PsychImaging('AddTask','General','UseFastOffscreenWindows');
    PsychImaging('AddTask','General','NormalizedHighresColorRange');
    video.res = Screen('Resolution',video.screen);
    [video.window, video.windowRect] = PsychImaging('OpenWindow',video.screen,0.395);
    fprintf('passed OpenWindow!\n')
    [video.screenXpixels, video.screenYpixels] = Screen('WindowSize', video.window);
    [video.xCenter, video.yCenter] = RectCenter(video.windowRect);
    Screen('BlendFunction', video.window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');%smooth blending/antialiasing
    video.blend = '''GL_SRC_ALPHA'', ''GL_ONE_MINUS_SRC_ALPHA''';
    video.colorrange = 1;
    Screen('ColorRange',video.window,video.colorrange);
    % 1 = to pass color values in OpenGL's native floating point color range of 0.0 to
    % 1.0: This has two advantages: First, your color values are independent of
    % display device depth, i.e. no need to rewrite your code when running it on
    % higher resolution hardware. Second, PTB can skip any color range remapping
    % operations - this can speed up drawing significantly in some cases.   ...???
    video.textsize = 50;
    Screen('TextSize', video.window, video.textsize);
    video.ifi = Screen('GetFlipInterval', video.window);
    video.priority = Priority(MaxPriority(video.window));
    vbl = Screen('Flip', video.window); % first flip
    nextfliptime = vbl+fx_roundfp(0.01,0,video.ifi); % wait 10ms
    
    video.offscreen = Screen('OpenOffscreenWindow',-1);
    Screen('TextSize', video.offscreen, video.textsize);

    
    %% Make textures:
    
    % bag textures:
    for ibag = 1:2 % 1 = inside 2 = outline
        bagimg      = double(imread(sprintf('./img/bag%d.png', ibag)))/255;
        bagimg      = imresize(bagimg, video.bag_sz/size(bagimg, 1));
        bagtx(ibag) = Screen('MakeTexture', video.window, cat(3, ones(size(bagimg)), bagimg), [], [], 2);
    end
    
    % shape textures:
    shapetx = zeros(2,8); %dim1: 1=inside / 2 =outline  // dim2: shape nb
    for i = 1:2
        for sh = 1:8
            shapeimg      = double(imread(sprintf('./img/shape%d_%d.png', sh, i)))/255;
            shapeimg      = imresize(shapeimg,video.shape_sz/size(shapeimg,1));
            shapetx(i,sh) = Screen('MakeTexture', video.window, cat(3,ones(size(shapeimg)), shapeimg), [],[], 2);
        end
    end
    
    % define position rectangles of textures: positions for the sampling probes:
    video.dotx     = [video.xCenter]; video.doty = [video.screenYpixels*0.55];
    bagpos(1,:)    = CenterRectOnPoint(Screen('Rect', bagtx(2)), video.xCenter-video.shape_offset, video.yCenter);
    bagpos(2,:)    = CenterRectOnPoint(Screen('Rect', bagtx(2)), video.xCenter+video.shape_offset, video.yCenter);
    bagheight      = (bagpos(1,3)-bagpos(1,1)); % centering shapes on bags
    bagwidth       = (bagpos(1,4)-bagpos(1,2));
    shapepos(1,:)  = CenterRectOnPoint(Screen('Rect',shapetx(1, 1)), video.xCenter - video.shape_offset, video.yCenter + bagheight* 0.2);
    shapepos(2,:)  = CenterRectOnPoint(Screen('Rect',shapetx(1, 1)), video.xCenter + video.shape_offset, video.yCenter + bagheight * 0.2);
    
    % define positions for the final probe
    respbagpos(1,:) = CenterRectOnPoint(Screen('Rect', bagtx(2)), video.xCenter- video.respbutton_offset, video.screenYpixels*0.65);
    respbagpos(2,:) = CenterRectOnPoint(Screen('Rect', bagtx(2)), video.xCenter+ video.respbutton_offset, video.screenYpixels*0.65);
    respbagpos(3,:) = CenterRectOnPoint(Screen('Rect', bagtx(2)), video.xCenter, video.screenYpixels*0.35);
    
    respshapepos(1,:) = CenterRectOnPoint(Screen('Rect',shapetx(1, 1)), video.xCenter- video.respbutton_offset, video.screenYpixels*0.65 + bagheight* 0.2);
    respshapepos(2,:) = CenterRectOnPoint(Screen('Rect',shapetx(1, 1)), video.xCenter+ video.respbutton_offset, video.screenYpixels*0.65 + bagheight* 0.2);
    respshapepos(3,:) = CenterRectOnPoint(Screen('Rect',shapetx(1, 1)), video.xCenter, video.screenYpixels*0.35 + bagheight* 0.2);
    
    buttonbuffer        = 0.1*bagwidth;
    selsize             = [0 0 bagwidth+buttonbuffer bagwidth+buttonbuffer];
    selection_rect(1,:) = CenterRectOnPoint(selsize, video.xCenter-video.respbutton_offset,video.screenYpixels*0.65);
    selection_rect(2,:) = CenterRectOnPoint(selsize, video.xCenter+video.respbutton_offset,video.screenYpixels*0.65);
    
    % score feedback: cup textures:
    cupi         = imread('./img/cupimg.png');
    cupi         = imresize(cupi,video.shape_sz/size(cupi,1));
    cuptext      = Screen('MakeTexture', video.window, cat(3,ones(size(cupi)), cupi));
    cuppos(1,:)  = CenterRectOnPoint(Screen('Rect',cuptext),video.xCenter-(video.shape_sz),video.yCenter);
    cuppos(2,:)  = CenterRectOnPoint(Screen('Rect',cuptext),video.xCenter, video.yCenter);
    cuppos(3,:)  = CenterRectOnPoint(Screen('Rect',cuptext),video.xCenter+(video.shape_sz),video.yCenter);
    
    buffer        = 0.3*size(cupi,2);
    cupframe      = [0 0 3*size(cupi,2)+buffer size(cupi,1)+buffer];
    cupframe_rect = CenterRectOnPoint(cupframe, video.xCenter,video.yCenter);
    
    %% Init keys:
    clear PsychHID; % Force new enumeration of devices.
    clear KbCheck;
    KbName('UnifyKeyNames'); %across OSs
    GetKeyboardIndices();
    
    escapeKey   = KbName('ESCAPE'); %experimenter keys (native Kb)
    continueKey = KbName('y');
    
    spaceKey    = KbName('space'); %participant keys (numpad)
    if ispc; leftKey = KbName('a');
    elseif ismac; leftKey = KbName('q');
    end
    rightKey    = KbName('p');
    
    respKeys     = [leftKey, rightKey];% left=1 right=2
    expeKeys     = [escapeKey, continueKey, leftKey, rightKey, spaceKey];
    allKeys      = [];
    k = RestrictKeysForKbCheck(allKeys);
    
    %% Start display:
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% test kb, hide cursor, stop spilling keys
    if ~video.testingON
        HideCursor;
        FlushEvents;
        ListenChar(2);
    end %setting testing parameters
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% welcome screen:
    if strcmp(session, 'training') == 1
        line1 = sprintf('D\0351but de l''entra\0356nement :\n\n\n');
        line2 = sprintf('    - vous avez 8 s\0351quences d''entra\0356nement (~ 4-5 min).\n');
        line3 = sprintf('    - les s\0351quences sont en vitesse r\0351elle de l''exp\0351rience.\n');
    elseif strcmp(session, 'expe') == 1
        line1 = sprintf('D\0351but de l''exp\0351rience :\n\n\n');
        line2 = sprintf('    - il y aura 4 blocs de 24 s\0351quences (~ 12min).\n');
        line3 = sprintf('    - des pauses sont pr\0351vues entre les blocs.');
    end
    
    DrawFormattedText(video.window, [line1 line2 line3], video.screenYpixels * 0.25, video.screenYpixels * 0.25, black);
    spaceline = 'Appuyez sur [espace] pour commencer.';
    DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.90, black);
    vbl = Screen('Flip', video.window, nextfliptime);
    nextfliptime = vbl+fx_roundfp(0.01,0,video.ifi); % wait 10ms
    if ~any(subj == [101 102 103])
    	WaitKeyPress(spaceKey);
    end
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% loop over blocks
    fprintf('Starting looping over blocks \n\n');
    blocks = unique([sqc.blk]);
    nblocks = numel(blocks);
    
    isamples = 0;
    for iblock = 1:nblocks
        
        thisblock = sqc([sqc.blk] == iblock);
        nseq = numel(thisblock);
        choicesgrades = NaN(1, nseq);
        
        k = RestrictKeysForKbCheck(expeKeys);
        
        % Begin block screen:
        fprintf('...******************************\n');
        fprintf('>>>START BLOCK %.f/%.f----- %s<<<\n', iblock, nblocks, datestr(now, 'HH:MM:SS'));
        line = sprintf('DEBUT DU BLOC %.f/%.f !\n', iblock, nblocks);
        DrawFormattedText(video.window, line, 'center', video.screenYpixels * 0.30, black);
        spaceline = 'Appuyez sur [espace] pour commencer le bloc.';
        DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.90, black);
        vbl = Screen('Flip', video.window, nextfliptime);
        nextfliptime = vbl+fx_roundfp(1,0,video.ifi); % wait 1 sec before begining block.
        if ~any(subj == [101 102 103])
            WaitKeyPress(spaceKey);
        end
        startblk = GetSecs;
        
        %%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% loop over sequences :
        fprintf('...Starting looping over sequences\n');
        for iseq = 1:nseq
            
            % Get idx of current sequence in full structure (sqc)
            isqc = find([sqc.blk] == iblock & [sqc.blkseq] == iseq);
            if mod(isqc,5) == 0; fprintf('...Seq # %d/%d...\n', iseq, nseq); end
            
            % Draw instructions for the sequence
            targetsymbol = sqc(isqc).symbolpair(1);
            othersymbol  = sqc(isqc).symbolpair(2);
            condition    = sqc(isqc).condition;
            targetcolor  = sqc(isqc).targetcolor;
            sidea  = sqc(isqc).instrside; % get side of target at the instructions
            sideb  = 3 - sidea;
            if      targetcolor == 1; colorstr = 'bleues'; % get target color string
            elseif  targetcolor == 2; colorstr = 'oranges';
            end
            if condition == 1 % get instructions string
                line0       = sprintf('piochez des formes %s !\nf', colorstr);
                [~,~,txb]   = DrawFormattedText(video.offscreen, line0, 'center', video.screenYpixels * 0.2, 1);
                nx          = video.xCenter - (txb(3)-txb(1))/2;
                ny          = txb(4);
                
                line1       = sprintf('Voici les sacs pour cette s\0351quence,');
                line2       = sprintf('piochez des formes ');
                line3       = sprintf('%s', colorstr);
                line4       = sprintf(' !');
                
                DrawFormattedText(video.window, line1, 'center', video.screenYpixels * 0.2, black);
                [nx, ny]    = DrawFormattedText(video.window, line2, nx, ny, black);
                [nx, ny]    = DrawFormattedText(video.window, line3, nx, ny, rgbcolors(targetcolor,:));
                DrawFormattedText(video.window, line4, nx, ny, black);
            
            elseif condition == 2
                line = sprintf('Voici les sacs pour cette s\0351quence,\n\0340 la fin de la s\0351quence vous devrez indiquer :\nun sac de formes de la couleur affich\0351e !');
                DrawFormattedText(video.window, line, 'center', video.screenYpixels * 0.2, 0);
            
            elseif condition == 3
                line0       = sprintf('un sac de formes %s !\nf\nf', colorstr);
                [~,~,txb]   = DrawFormattedText(video.offscreen, line0, 'center', video.screenYpixels * 0.15, 1);
                nx          = video.xCenter - (txb(3)-txb(1))/2;
                ny          = txb(4);
                
                line1       = sprintf('Voici les sacs pour cette s\0351quence,\n\0340 la fin de la s\0351quence vous devrez indiquer :');
                line2       = sprintf('un sac de formes ');
                line3       = sprintf('%s', colorstr);
                line4       = sprintf(' !');
                
                DrawFormattedText(video.window, line1, 'center', video.screenYpixels * 0.15, 0);
                [nx, ny]    = DrawFormattedText(video.window, line2, nx, ny, 0);
                [nx, ny]    = DrawFormattedText(video.window, line3, nx, ny, rgbcolors(targetcolor,:));
                DrawFormattedText(video.window, line4, nx, ny, 0);
            end
            
            % Draw sequence bags:
            Screen('DrawDots', video.window, [video.dotx video.doty], 15 , black, [0 0], 2);
            Screen('DrawTexture', video.window, bagtx(2), [], bagpos(sidea,:), 0, [], [], [0,0,0]);
            Screen('DrawTexture', video.window, shapetx(2, targetsymbol), [], shapepos(sidea,:), 0, [], [], [0,0,0]);
            Screen('DrawTexture', video.window, shapetx(1, targetsymbol), [], shapepos(sidea,:), 0, [], [], video.lbgd);
            Screen('DrawTexture', video.window, bagtx(2), [], bagpos(sideb,:), 0, [], [], [0,0,0]);
            Screen('DrawTexture', video.window, shapetx(2, othersymbol), [], shapepos(sideb,:), 0, [], [], [0,0,0]);
            Screen('DrawTexture', video.window, shapetx(1, othersymbol), [], shapepos(sideb,:), 0, [], [], video.lbgd);
            
            % Draw progress at the bottom:
            spaceline = sprintf('Appuyez sur [espace] pour commencer la s\0351quence.');
            progressline = sprintf('S\0351quence %.f/%.f de ce bloc, continuez !\n', iseq, nseq);
            nextline = [progressline spaceline];
            DrawFormattedText(video.window, nextline, 'center', video.screenYpixels * 0.90, black);
            
            % Flip, wait 1'' for spacebar press
            Screen('DrawingFinished', video.window);
            vbl = Screen('Flip', video.window, nextfliptime);
            nextfliptime = vbl+fx_roundfp(1,0,video.ifi); % wait 1sec showing the instructions
            k = RestrictKeysForKbCheck(spaceKey);
            
            if ~any(subj == [101 102 103])
                WaitKeyPress(spaceKey);
            end
            
            % Flip fixation point for 1sec before starting
            Screen('DrawDots', video.window, [video.dotx video.doty], 15 , black, [0 0], 2);
            vbl = Screen('Flip', video.window, nextfliptime);
            nextfliptime = vbl+fx_roundfp(1,0,video.ifi); %WaitSecs(1) before next flip
            
            %%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%% loop over samples
            nsamples     = sqc(isqc).nsamples;
            choices      = nan(1,nsamples);
            choicesrt    = nan(1,nsamples);
            choicessides = nan(1,nsamples);
            
            for ismp = 1:nsamples
                
                k = RestrictKeysForKbCheck(expeKeys); % reset by clearall or RestrictKeysForKbCheck([])
                
                % Draw sampling alternatives: draw "target" on "symbolside"
                targetside  = sqc(isqc).symbolsides(ismp);
                otherside   = 3 - sqc(isqc).symbolsides(ismp);
                Screen('DrawDots', video.window, [video.dotx video.doty], 15 , black, [0 0], 2);
                
                Screen('DrawTexture', video.window, bagtx(2), [], bagpos(targetside,:), 0, [], [], [0,0,0]);
                Screen('DrawTexture', video.window, shapetx(2, targetsymbol), [], shapepos(targetside,:), 0, [], [], [0,0,0]);
                Screen('DrawTexture', video.window, shapetx(1, targetsymbol), [], shapepos(targetside,:), 0, [], [], video.lbgd);
                
                Screen('DrawTexture', video.window, bagtx(2), [], bagpos(otherside,:), 0, [], [], [0,0,0]);
                Screen('DrawTexture', video.window, shapetx(2, othersymbol), [], shapepos(otherside,:), 0, [], [], [0,0,0]);
                Screen('DrawTexture', video.window, shapetx(1, othersymbol), [], shapepos(otherside,:), 0, [], [], video.lbgd);
                Screen('DrawingFinished', video.window);
                
                % Before next flip, check for abort key:
                if CheckKeyPress(escapeKey)
                    Priority(0); FlushEvents; ListenChar(0); ShowCursor;
                    WaitSecs(1);
                end
                vbl = Screen('Flip', video.window, nextfliptime);
                nextfliptime = vbl; %don't wait, flip asap
                
                % Get sampling choice:
                while true
                    
                    if subj == 101 % 101 chooses always the right side
                        rt = rand(1);
                        choice = 2; break
                    elseif subj == 102 % 102 chooses always the correct side
                        rt = rand(1);
                        choice = targetside; break
                    elseif subj == 103  % 103 chooses randomly
                        rt = rand(1);
                        choice = randi(2,[1,1]); break
                    end
                    
                    [keyIsDown,secs,keylist] = KbCheck();
                    if keyIsDown && sum(keylist) == 1 && (keylist(leftKey) == 1 || keylist(rightKey) == 1)
                        rt = secs - vbl;
                        choice = find(respKeys == (find(keylist==1))); % side
                        break
                    end
                    
                end
                
                % draw color outcome:
                if choice == targetside
                    samplecolor = fx_get_rgb(sqc(isqc).colorslin(1,ismp));
                    Screen('DrawDots', video.window, [video.dotx video.doty], 15 , black, [0 0], 2);
                    Screen('DrawTexture', video.window, shapetx(2, targetsymbol), [], shapepos(targetside,:), 0, [], [], [0,0,0]);
                    Screen('DrawTexture', video.window, shapetx(1, targetsymbol), [], shapepos(targetside,:), 0, [], [], samplecolor);
                    choices(ismp)      = 1;
                    choicessides(ismp) = targetside;
                    choicesrt(ismp)    = rt;
                elseif choice == otherside
                    samplecolor = fx_get_rgb(sqc(isqc).colorslin(2,ismp));
                    Screen('DrawDots', video.window, [video.dotx video.doty], 15 , black, [0 0], 2);
                    Screen('DrawTexture', video.window, shapetx(2, othersymbol), [], shapepos(otherside,:), 0, [], [], [0,0,0]);
                    Screen('DrawTexture', video.window, shapetx(1, othersymbol), [], shapepos(otherside,:), 0, [], [], samplecolor);
                    choices(ismp)      = 2;
                    choicessides(ismp) = otherside;
                    choicesrt(ismp)    = rt;
                end
                
                % flip outcome for 0.5''
                vbl = Screen('Flip', video.window, nextfliptime);
                nextfliptime = vbl+fx_roundfp(0.5,0,video.ifi);
                
                % draw fxpoint for 0.5''
                Screen('DrawDots', video.window, [video.dotx video.doty], 15 , black, [0 0], 2);
                vbl = Screen('Flip', video.window, nextfliptime);
                nextfliptime = vbl+fx_roundfp(0.5,0,video.ifi);
                
                % Store results:
                isamples                          = isamples +1;
                sampling(isamples).subj           = subj;
                sampling(isamples).condition      = condition;
                sampling(isamples).mirroring      = sqc(isqc).mirroring;
                sampling(isamples).blk            = sqc(isqc).blk;
                sampling(isamples).blkseq         = iseq;
                sampling(isamples).isqc           = isqc;     % 1:128
                sampling(isamples).samplenb       = ismp;
                sampling(isamples).nsamples       = sqc(isqc).nsamples;
                sampling(isamples).symbolpair     = sqc(isqc).symbolpair;
                sampling(isamples).targetsym      = targetsymbol;
                
                sampling(isamples).symmetry       = sqc(isqc).symmetry;
                sampling(isamples).targetcolor    = targetcolor;
                
                sampling(isamples).targetside     = targetside; % keeps the side information
                sampling(isamples).samplingchoice = choice; % choice is 1: target 2: other
                sampling(isamples).samplingcolor  = samplecolor;
                sampling(isamples).samplingrt     = rt;
                
            end%FOR each sample
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % Add blank screen 0.5''
            Screen('FillRect', video.window, video.lbgd);
            vbl = Screen('Flip', video.window, nextfliptime);
            nextfliptime = vbl+fx_roundfp(0.5,0,video.ifi);
            
            % End of each sequence: draw final probe while no answer yet:
            k = RestrictKeysForKbCheck(expeKeys);
            
            % Final probe text; depending on condition,
            if condition == 1 % draw: don't show final probe
                respprobe  = NaN;
                rtprobe    = NaN;
                finaltargetside = NaN;
                                
            elseif condition == 2 || condition == 3 % guess or find = show final probe:

                % retreive side of the target and actual colors of the shapes:
                % targetcolor is the instructions' color, actual colors is corrected for good/good or bad/bad situations
                % in COLMID we're not asking for the color of the bag, but for a bag of the relevant color
                % BADBAD: both bags are the INCORRECT color
                % GOODGOOD: both bags are the CORRECT color
                % finaltargetside = 1/2 = Left/Right = side of the bag in the target color as generated in the instructions
                % actualcolors    = actual colors of the right and the left bags, correcting for BADBAD and GOODGOOD conditions (where they're both the same color)
                finaltargetside  = sqc(isqc).probeside;
                finalotherside   = 3-finaltargetside;
                if sqc(isqc).symmetry == 1;         bothactualcolors(finaltargetside,:) = targetcolor;     bothactualcolors(finalotherside,:) = targetcolor;
                elseif sqc(isqc).symmetry == 2;     bothactualcolors(finaltargetside,:) = 3 - targetcolor; bothactualcolors(finalotherside,:) = 3 - targetcolor;
                elseif sqc(isqc).symmetry == 3;     bothactualcolors(finaltargetside,:) = targetcolor;     bothactualcolors(finalotherside,:) = 3 - targetcolor;
                end
                
                % GUESS or FIND probe :
                if condition == 2 || condition == 3        
                    line0       = sprintf('Indiquez un sac de formes %s :', colorstr);
                    [nx,~,txb] = DrawFormattedText(video.offscreen, line0, 'center', video.screenYpixels * 0.15, 1);
                    nx          = nx - (txb(3)-txb(1));
                    ny          = txb(4);
                    
                    line1       = sprintf('Indiquez un sac de formes ');
                    line2       = sprintf('%s', colorstr);
                    line3       = sprintf(' :');
                    
                    [nx, ny]    = DrawFormattedText(video.window, line1, nx, ny, black);
                    [nx, ny]    = DrawFormattedText(video.window, line2, nx, ny, rgbcolors(targetcolor,:));
                    DrawFormattedText(video.window, line3, nx, ny, black);
                    
                    Screen('DrawTexture', video.window, bagtx(2), [], respbagpos(3,:), 0, [], [], [0,0,0]);
                    Screen('DrawTexture', video.window, bagtx(1), [], respbagpos(3,:), 0, [], [], rgbcolors(targetcolor,:));
                    
                    Screen('DrawTexture', video.window, bagtx(2), [], respbagpos(finaltargetside,:), 0, [], [], [0,0,0]); % display targetbag on its correct side
                    Screen('DrawTexture', video.window, shapetx(2, targetsymbol), [], respshapepos(finaltargetside,:), 0, [], [], [0,0,0]);
                    Screen('DrawTexture', video.window, shapetx(1, targetsymbol), [], respshapepos(finaltargetside,:), 0, [], [], video.lbgd);
                    
                    Screen('DrawTexture', video.window, bagtx(2), [], respbagpos(finalotherside,:), 0, [], [], [0,0,0]); % display otherbag on the other side
                    Screen('DrawTexture', video.window, shapetx(2, othersymbol), [], respshapepos(finalotherside,:), 0, [], [], [0,0,0]);
                    Screen('DrawTexture', video.window, shapetx(1, othersymbol), [], respshapepos(finalotherside,:), 0, [], [], video.lbgd);
                    
                    Screen('DrawingFinished', video.window);
                    
                end
                
                % flip probe for as long as no answer:
                vbl = Screen('Flip', video.window, nextfliptime);
                nextfliptime = vbl; % flip ASAP after response
                
                % wait for final response:
                while true
                    
                    if subj == 101      % 101 chooses always the right side
                        rtprobe  = rand(1);
                        respprobe = 2; break
                    elseif subj == 102   % 102 chooses always the target side
                        rtprobe = rand(1);
                        if sqc(isqc).symmetry == 2; respprobe = 3-finaltargetside; break
                        else;                       respprobe = finaltargetside; break
                        end
                    elseif subj == 103   % 103 chooses randomly
                        rtprobe = rand(1);
                        respprobe = randi(2,[1,1]); break
                    end
                    
                    [keyIsDown,secs,keylist] = KbCheck();
                    if keyIsDown && (keylist(leftKey) == 1 ||  keylist(rightKey) == 1) && sum(keylist) == 1
                        respprobe = find(respKeys == (find(keylist==1)));
                        rtprobe   = secs - vbl;
                        break
                    end
                end
                
                % GUESS: draw the given response (probe + selection square):
                if condition == 2 || condition == 3
                    line0       = sprintf('Indiquez un sac de formes %s :', colorstr);
                    [nx,~,txb] = DrawFormattedText(video.offscreen, line0, 'center', video.screenYpixels * 0.15, 1);
                    nx          = nx - (txb(3)-txb(1));
                    ny          = txb(4);
                    
                    line1       = sprintf('Indiquez un sac de formes ');
                    line2       = sprintf('%s', colorstr);
                    line3       = sprintf(' :');
                    
                    [nx, ny]    = DrawFormattedText(video.window, line1, nx, ny, black);
                    [nx, ny]    = DrawFormattedText(video.window, line2, nx, ny, rgbcolors(targetcolor,:));
                    DrawFormattedText(video.window, line3, nx, ny, black);
                    
                    Screen('DrawTexture', video.window, bagtx(2), [], respbagpos(3,:), 0, [], [], [0,0,0]);
                    Screen('DrawTexture', video.window, bagtx(1), [], respbagpos(3,:), 0, [], [], rgbcolors(targetcolor,:));
                    
                    Screen('DrawTexture', video.window, bagtx(2), [], respbagpos(finaltargetside,:), 0, [], [], [0,0,0]);
                    Screen('DrawTexture', video.window, shapetx(2, targetsymbol), [], respshapepos(finaltargetside,:), 0, [], [], [0,0,0]);
                    Screen('DrawTexture', video.window, shapetx(1, targetsymbol), [], respshapepos(finaltargetside,:), 0, [], [], video.lbgd);
                    
                    Screen('DrawTexture', video.window, bagtx(2), [], respbagpos(finalotherside,:), 0, [], [], [0,0,0]);
                    Screen('DrawTexture', video.window, shapetx(2, othersymbol), [], respshapepos(finalotherside,:), 0, [], [], [0,0,0]);
                    Screen('DrawTexture', video.window, shapetx(1, othersymbol), [], respshapepos(finalotherside,:), 0, [], [], video.lbgd);

                    Screen('FrameRect', video.window, black, selection_rect(respprobe,:), 6);
                    Screen('DrawingFinished', video.window);
                end
                
                % flip response, show for 0.4'':
                vbl = Screen('Flip', video.window, nextfliptime);
                nextfliptime = vbl+fx_roundfp(0.4,0,video.ifi); % show sel square for 0.4
                
                % draw color feedback on final probe:
                % /•/•/ NB: finaltargetside is the side where final(sq).targetcolor was presented
                % so the side of "theoretical/condition" target color
                % so it's the color of the targetsymbol *if* it was generated in the "good" color, for "bad", needs flipping:
                % in bad-bad open sequences the underlying color was actually the opposite of the final(sq).targetcolor field
                % so the symbol was actually of the color on the "finalothercolorside"
                % is flipped when parsing the data. \•\•\
                
                % GUESS or FIND show feedback on final response:
                if condition == 2 || condition == 3
                    line0       = sprintf('Indiquez un sac de formes %s :', colorstr);
                    [nx,~,txb] = DrawFormattedText(video.offscreen, line0, 'center', video.screenYpixels * 0.15, 1);
                    nx          = nx - (txb(3)-txb(1));
                    ny          = txb(4);
                    
                    line1       = sprintf('Indiquez un sac de formes ');
                    line2       = sprintf('%s', colorstr);
                    line3       = sprintf(' :');
                    
                    [nx, ny]    = DrawFormattedText(video.window, line1, nx, ny, black);
                    [nx, ny]    = DrawFormattedText(video.window, line2, nx, ny, rgbcolors(targetcolor,:));
                    DrawFormattedText(video.window, line3, nx, ny, black);
                    Screen('DrawTexture', video.window, bagtx(2), [], respbagpos(3,:), 0, [], [], [0,0,0]);
                    Screen('DrawTexture', video.window, bagtx(1), [], respbagpos(3,:), 0, [], [], rgbcolors(targetcolor,:));
                    
                    Screen('DrawTexture', video.window, bagtx(2), [], respbagpos(finaltargetside,:), 0, [], [], [0,0,0]); % draw all bags,
                    Screen('DrawTexture', video.window, bagtx(2), [], respbagpos(finalotherside,:), 0, [], [], [0,0,0]);  
                    Screen('DrawTexture', video.window, bagtx(1), [], respbagpos(respprobe,:), 0, [], [], rgbcolors(bothactualcolors(respprobe),:) ); % color the chosen one
                    % take the actual color (bothactualcolors) of the bag on the side of the response (respprobe)
                    
                    Screen('DrawTexture', video.window, shapetx(2, targetsymbol), [], respshapepos(finaltargetside,:), 0, [], [], [0,0,0]); % overlay shapes
                    Screen('DrawTexture', video.window, shapetx(1, targetsymbol), [], respshapepos(finaltargetside,:), 0, [], [], video.lbgd);
                    Screen('DrawTexture', video.window, shapetx(2, othersymbol), [], respshapepos(finalotherside,:), 0, [], [], [0,0,0]);
                    Screen('DrawTexture', video.window, shapetx(1, othersymbol), [], respshapepos(finalotherside,:), 0, [], [], video.lbgd);
                    
                    Screen('FrameRect', video.window, black, selection_rect(respprobe,:), 6);
                    Screen('DrawingFinished', video.window);
                end
                
                % flip color feedback on final probe for 0.5''
                vbl = Screen('Flip', video.window, nextfliptime);
                nextfliptime = vbl+fx_roundfp(0.5,0,video.ifi); % show feedback for 0.5
                
            end %CASE condition open, show final probe
            
            % flip blank screen for 0.3 before next sequence:
            Screen('FillRect', video.window, video.lbgd);
            vbl = Screen('Flip', video.window, nextfliptime);
            nextfliptime = vbl+fx_roundfp(0.3,0,video.ifi);
            
            % grade responses:
            switch condition
                case 1
                    switch sqc(isqc).symmetry
                        case 1; choicesgrades(iseq) = 1;
                        case 2; choicesgrades(iseq) = 0;
                        case 3; choicesgrades(iseq) = mean(choices == 1);
                    end
                case 2
                    switch sqc(isqc).symmetry
                        case 1; choicesgrades(iseq) = 1;
                        case 2; choicesgrades(iseq) = 0;
                        case 3; choicesgrades(iseq) = respprobe == finaltargetside;
                    end
                case 3
                    switch sqc(isqc).symmetry
                        case 1; choicesgrades(iseq) = 1;
                        case 2; choicesgrades(iseq) = 0;
                        case 3; choicesgrades(iseq) = respprobe == finaltargetside;
                    end
            end

            % Store final response: cuisine pour remettre info dans tous les samples:
            for i = (isamples - (nsamples-1)) : isamples
                sampling(i).finaltargetside      = finaltargetside;
                sampling(i).finalresponseside    = respprobe;
                sampling(i).finalrt              = rtprobe;
            end
            final(isqc).subj                 = subj;
            final(isqc).condition            = condition;
            final(isqc).mirroring            = sqc(isqc).mirroring;
            final(isqc).blk                  = sqc(isqc).blk;
            final(isqc).blkseq               = sqc(isqc).blkseq;
            final(isqc).isqc                 = isqc;
            final(isqc).nsamples             = sqc(isqc).nsamples;
            final(isqc).symbolpair           = sqc(isqc).symbolpair;
            final(isqc).targetsymbol         = targetsymbol;
            final(isqc).symmetry             = sqc(isqc).symmetry;
            final(isqc).targetcolor          = sqc(isqc).targetcolor;
            final(isqc).finaltargetside      = sqc(isqc).probeside;
            final(isqc).finalresponseside    = respprobe;
            final(isqc).finalresponse        = respprobe == finaltargetside; % NB: this is wrong for bad bad --> parsedata flips it.
            final(isqc).finalrt              = rtprobe;
            
            final(isqc).choices              = choices; % choices are 1: target 2: other
            final(isqc).choicesrt            = choicesrt;
            final(isqc).choicessides         = choicessides; % sides are 1: left 2: right
            
        end%FOR each sequence of samples in this block
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        save(sprintf('%s/S%02d_%s_b%02d.mat', subjdir, subj, session, iblock), 'header', 'sampling', 'final', 'sqc', 'video', 'ME');
        
        %%%
        %%% End of each block, insert pause & flip score
        endblk        = GetSecs;
        blkkeypresses = [sampling([sampling.blk] == iblock).samplingchoice]; % get samples from this block & check key presses:
        keypresses    = sum(blkkeypresses==1) / numel(blkkeypresses) *100;
        fprintf('>>>DONE WITH BLOCK %d!\n', iblock);
        fprintf('...Block %d lasted %.f:%02.f min\n', iblock, (endblk - startblk)/60, mod((endblk - startblk),60))
        fprintf('...Mean prop of optimal responses is %2.f%%\n', nanmean(choicesgrades)*100);
        fprintf('...Mean prop of optimal responses is %2.f%%\n', nanmean(choicesgrades)*100);
        fprintf('...Participant pressed [a] %.f %%\n', keypresses);
        
        %%%
        %%% show block score feedback
        line1 = sprintf('C''est la fin de ce bloc, bravo !\nSCORE :');
        DrawFormattedText(video.window, line1, 'center', video.screenYpixels * 0.25, black);
        if nanmean(choicesgrades) < 0.5
            pscr = 0;
            Screen('FrameRect', video.window, black, cupframe_rect, 4);
            fprintf('...Score 0\n');
        elseif nanmean(choicesgrades) >= 0.5 && nanmean(choicesgrades) < 0.6
            pscr = 1;
            Screen('FrameRect', video.window, black, cupframe_rect, 4);
            Screen('DrawTexture', video.window, cuptext, [], cuppos(1,:));
            fprintf('...Score *\n');
        elseif nanmean(choicesgrades) >= 0.60 && nanmean(choicesgrades) < 0.65
            pscr = 2;
            Screen('FrameRect', video.window, black, cupframe_rect, 4);
            Screen('DrawTexture', video.window, cuptext, [], cuppos(1,:));
            Screen('DrawTexture', video.window, cuptext, [], cuppos(2,:));
            fprintf('...Score **\n');
        elseif nanmean(choicesgrades) >= 0.65
            pscr = 3;
            Screen('FrameRect', video.window, black, cupframe_rect, 4);
            Screen('DrawTexture', video.window, cuptext, [], cuppos(1,:));
            Screen('DrawTexture', video.window, cuptext, [], cuppos(2,:));
            Screen('DrawTexture', video.window, cuptext, [], cuppos(3,:));
            fprintf('...Score ***\n');
        else; nanmean(choicesgrades)
        end
        vbl = Screen('Flip', video.window, nextfliptime);
        nextfliptime = vbl+fx_roundfp(1,0,video.ifi); % schow score for 1''
        
        %%%
        %%%% show break until pressed space - force break every 2 blocks:
        switch pscr
            case 0
                Screen('FrameRect', video.window, black, cupframe_rect, 4);
            case 1
                Screen('FrameRect', video.window, black, cupframe_rect, 4);
                Screen('DrawTexture', video.window, cuptext, [], cuppos(1,:));
            case 2
                Screen('FrameRect', video.window, black, cupframe_rect, 4);
                Screen('DrawTexture', video.window, cuptext, [], cuppos(1,:));
                Screen('DrawTexture', video.window, cuptext, [], cuppos(2,:));
            case 3
                Screen('FrameRect', video.window, black, cupframe_rect, 4);
                Screen('DrawTexture', video.window, cuptext, [], cuppos(1,:));
                Screen('DrawTexture', video.window, cuptext, [], cuppos(2,:));
                Screen('DrawTexture', video.window, cuptext, [], cuppos(3,:));
        end
        
        if strcmp(session, 'expe') == 1
            if iblock < nblocks && mod(iblock,2) == 1 % free break
                line1 = sprintf('C''est la fin de ce bloc, bravo !\nSCORE :');
                DrawFormattedText(video.window, line1, 'center', video.screenYpixels * 0.25, black);
                line2 = sprintf('PRENEZ UNE PAUSE !\n Quand vous \0352tes pr\0352t.e \0340 continuer,\nappuyez sur [espace].\n');
                DrawFormattedText(video.window, line2, 'center', video.screenYpixels * 0.75, black);
                Screen('DrawingFinished', video.window);
                vbl = Screen('Flip', video.window, nextfliptime);
                fprintf('>>>Participant is on BREAK!\n')
                k = RestrictKeysForKbCheck(spaceKey);
                if ~any(subj == [101 102 103])
                    WaitKeyPress(spaceKey); % flip break and wait for continue key [space]
                end 
                nextfliptime = vbl + fx_roundfp(1,0,video.ifi); % block for 1'';
            elseif iblock < nblocks && mod(iblock,2) == 0 % forced break
                line1 = sprintf('C''est la fin de ce bloc, bravo !\nSCORE :');
                DrawFormattedText(video.window, line1, 'center', video.screenYpixels * 0.25, black);
                line2 = sprintf('PRENEZ UNE PAUSE !\n Quand vous \0352tes pr\0352t.e \0340 continuer,\nallez chercher l''exp\0351rimentateur.\n');
                DrawFormattedText(video.window, line2, 'center', video.screenYpixels * 0.75, black);
                Screen('DrawingFinished', video.window);
                vbl = Screen('Flip', video.window, nextfliptime);
                nextfliptime = vbl + fx_roundfp(1,0,video.ifi); % block for 1'';
                k = RestrictKeysForKbCheck(continueKey);
                fprintf('>>>Participant is on BREAK!\n...*****Press [y] to continue.\n...******************************\n')
                if ~any(subj == [101 102 103])
                    WaitKeyPress(continueKey); % flip break and wait for debreak key: Y
                end
            elseif iblock == nblocks
                line1 = ('Score :');
                DrawFormattedText(video.window, line1, 'center', video.screenYpixels * 0.25, black);
                line2 = sprintf('Appuyez sur [espace] pour terminer.');
                DrawFormattedText(video.window, line2, 'center', video.screenYpixels * 0.75, black);
                vbl = Screen('Flip', video.window, nextfliptime);
                k = RestrictKeysForKbCheck(spaceKey);
                if ~any(subj == [101 102 103])
                    WaitKeyPress(spaceKey); % flip break and wait for continue key [space]
                end 
                nextfliptime = vbl;
            end
        elseif strcmp(session, 'training') == 1
            line1 = sprintf('C''est la fin de l''entra\0356nement, bravo !\nSCORE :');
            DrawFormattedText(video.window, line1, 'center', video.screenYpixels * 0.25, black);
            line2 = sprintf('Appuyez sur [espace] pour terminer.\n');
            DrawFormattedText(video.window, line2, 'center', video.screenYpixels * 0.75, black);
            vbl = Screen('Flip', video.window, nextfliptime);
            k = RestrictKeysForKbCheck(spaceKey);
            if ~any(subj == [101 102 103])
            	WaitKeyPress(spaceKey); % flip break and wait for continue key [space]
            end 
            nextfliptime = vbl;
        end
        
        endpause = GetSecs;
        fprintf('...Break %d lasted %.f:%02.f min\n\n', iblock, (endpause - endblk)/60, mod((endpause - endblk),60))
        
        
    end%FOR EACH BLOCK IN SQC STRUCTURE
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %% DRAW THANK YOU SCREEN
    if strcmp(session, 'training') == 1
        line = sprintf('Fin de l''entra\0356nement !\nAvez-vous des questions ?\n\n\n\nVous allez pouvoir commencer l''exp\0351rience !\n');
        endline = sprintf('Appuyez sur [espace] pour quitter l''entra\0356nement.');
    elseif strcmp(session, 'expe') == 1
        line = sprintf('Merci pour votre participation, l''exp\0351rience est termin\0351e !\n\n\n');
        endline = '-- FIN --';
    end
    DrawFormattedText(video.window, [line], 'center', video.screenYpixels * 0.25, black);
    DrawFormattedText(video.window, [endline], 'center', video.screenYpixels * 0.75, black);
    Screen('DrawingFinished', video.window);
    vbl = Screen('Flip', video.window, nextfliptime);
    k = RestrictKeysForKbCheck(spaceKey);
    if ~any(subj == [101 102 103])
    	WaitKeyPress(spaceKey); % flip break and wait for continue key [space]
    end 
    sca;
    
    % save PTB data
    header.end = datestr(now,'yyyymmdd-HHMM');
    header.aborted = 0;
    expout.header = header;
    expout.sampling = sampling;
    expout.final = final;
    expout.sqc = sqc;
    expout.video = video;
    save(sprintf('%s/S%02d_%s.mat', subjdir, subj, session), 'header', 'sampling', 'final', 'sqc', 'video', 'ME');
    
    % close PTB
    Priority(0); FlushEvents; ListenChar(0); ShowCursor; Screen('CloseAll');
    Screen('Preference', 'SkipSyncTests', 0);%restore synch testing
    Screen('Preference','VisualDebuglevel',4);%restore warning verbosity
    Screen('Preference','SuppressAllWarnings',0);%=1 == warning verbosity=0
    k = RestrictKeysForKbCheck([]);
    
    fprintf('\n...FX_RUN_EXPE OVER FOR %s...\n', session);
    diary off;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% catching results so far if crashes
catch ME
    
    % close PTB
    Priority(0); FlushEvents; ListenChar(0); ShowCursor; Screen('CloseAll');
    Screen('Preference', 'SkipSyncTests', 0);%restore synch testing
    Screen('Preference','VisualDebuglevel',4);%restore warning verbosity
    Screen('Preference','SuppressAllWarnings',0);%=1 == warning verbosity=0
    k = RestrictKeysForKbCheck([]);
    
    % clean save behaviour:
    header.end = datestr(now,'yyyymmdd-HHMM');
    header.aborted = 1;
    expout.header = header;
    expout.sampling = sampling;
    expout.final = final;
    expout.sqc = sqc;
    expout.video = video;
    expout.ME = ME;
    save(sprintf('%s/S%02d_%s_b%02d_part.mat', subjdir, subj, session, iblock)) % if crash, save all variables in workspace
    
    rethrow(ME);
    
end% TRY/CATCH

end% function def fx_run_expe.m


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


