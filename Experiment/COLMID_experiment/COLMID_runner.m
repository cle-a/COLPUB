%%% running the whole COLMID experiment from runner:
%%%
%%% Asks whether to run iin debug mode (whether to spill keys and show cursor etc)
%%% Asks for participant's number
%%%
%%% SEQUENCES MUST BE GENERATED BEFOREHAND
%%% COLMID_fx_gen_training_sqc.m = for training sequences, was run once for all participants
%%% COLMID_fx_gen_sqc.m          = to make the sqc structures for all participants
%%%
%%% instructions were displayed on ppt for this version of the experiment

close all
clear all java
close all hidden
clc
addpath ./Toolboxes/IO % Valentin's toolbox in Experiment's path

testing = input('>>>  Run in debug mode? yes(1) or no(0): ');
if ~(testing == 1 || testing == 0)
    error('Debug mode is either 1 or 0.');
end

subj = input('>>>  Input participant nb: ');
if ~isscalar(subj) || mod(subj,1) ~= 0
    error('Invalid subject number!');
end

subjdir = sprintf('../Data/S%02d',subj);

diary(sprintf('../Data/S%02d/S%02d_log_%s.txt', subj, subj, datestr(now,'mmdd_HHMM')));

fprintf('Saving into %s\n', subjdir);

% run training:
fprintf('...Launching training now \n');
load('training_sqc.mat')
trainingoutput = COLMID_fx_run_expe(training_sqc, subj, 'training', testing);
save(sprintf('%s/S%02d_recovery_training.mat', subjdir, subj), 'trainingoutput');
fprintf('...Finished training, went from %s to %s ... \n', trainingoutput.header.start, trainingoutput.header.end);
fprintf('        waiting for [t] press... ... \n');
WaitKeyPress(KbName('t'));

% run experiment
fprintf('...Loading existing runs from ''%s/S%02d_sqc.mat''...\n', subjdir, subj)
load(sprintf('%s/S%02d_sqc.mat', subjdir, subj),'sqc');
fprintf('*****\n*****\n...Launching experiment now... \n');
expeoutput = COLMID_fx_run_expe(sqc, subj, 'expe', testing);
save(sprintf('%s/S%02d_recovery_expe.mat', subjdir, subj), 'expeoutput');
fprintf('FINISHED EXPERIMENT, went from %s to %s ... \n', expeoutput.header.start, expeoutput.header.end);
diary off;